IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLClientCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLClientCheck
GO

/********************************
 설    명 - 고객등록:체크
 작 성 일 - 2020년 05월 11일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLClientCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq          INT        = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    ---- 사용할 변수를 선언한다.
    DECLARE @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT          ,
            @UserType     INT          ,
            @EmpSeq       INT 

    SELECT @UserType = UserType,
           @EmpSeq   = EmpSeq  
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq
           

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock1)
    BEGIN


        -------------------------------------------      
        -- 고객 삭제 시, 해당 고객이 입력된 서비스 접수 및 처리 건, 고객상담정보가 있으면 삭제 불가
        /*
            참고화면
            서비스접수 - [서비스접수]
            서비스처리 - [서비스처리]
            고객상담정보 - [고객상담정보]
        */
        -------------------------------------------  

        -- 서비스접수내역이 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
                               @Status      OUTPUT     ,  
                               @Results     OUTPUT     ,  
                               1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq            ,  
                               24355, N'서비스접수정보', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스접수정보%' 
                               308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS (SELECT TOP 1 1 
                         FROM _TSLASServiceReceipt WITH(NOLOCK)
                        WHERE CompanySeq = @CompanySeq
                          AND ClientSeq = A.ClientSeq
                      )

        -- 서비스처리내역이 존재할 경우 삭제 불가- 추후 테이블 생성되면 추가 
   --     EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
   --                            @Status      OUTPUT     ,  
   --                            @Results     OUTPUT     ,  
   --                            1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
   --                            @LanguageSeq            ,  
   --                            24355, N'서비스접수정보', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스접수정보%' 
   --                            308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

   --     UPDATE #BIZ_OUT_DataBlock1 
   --        SET Result      = @Results    , 
   --            MessageType = @MessageType,  
   --            Status      = @Status  
   --       FROM #BIZ_OUT_DataBlock1 AS A
		 --WHERE A.WorkingTag = 'D'
		 --  AND A.Status     = 0  
   --        AND EXISTS (SELECT TOP 1 1 
   --                      FROM _TSLASServiceReceipt WITH(NOLOCK)
   --                     WHERE CompanySeq = @CompanySeq
   --                       AND ServiceCenterSeq = A.ServiceCenterSeq
   --                   )

        -- 고객상담정보가 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                               @Status      OUTPUT,  
                               @Results     OUTPUT,  
                               1355               , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq       ,  
                               72882, N'상담정보' , -- SELECT * FROM _TCADictionary WHERE Word like '%상담정보%' 
                               308, N'삭제'         -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS (SELECT TOP 1 1 
                         FROM _TSLASClientConsultInfo WITH(NOLOCK)
                        WHERE CompanySeq = @CompanySeq
                          AND ClientSeq = A.ClientSeq
                      )


		-------------------------------------------      
		-- 외부사용자 또는 서비스센터의 담당자로 지정된 내부사용자의 경우 고객 등록시 관할지역을 필수로 입력해야함. 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT  ,  
                               @Status      OUTPUT  ,  
                               @Results     OUTPUT  ,  
                               1008                 , -- @1을(를) 입력하지 않았습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1008)  
                               @LanguageSeq         ,  
                               72892, N'관할지역'     -- SELECT * FROM _TCADictionary WHERE Word like '%관할지역%'  

	    UPDATE #BIZ_OUT_DataBlock1 
		   SET Result      = @Results    , 
			   MessageType = @MessageType,  
			   Status      = @Status  
		  FROM #BIZ_OUT_DataBlock1 AS A
	     WHERE A.WorkingTag IN ('A', 'U') 
		   AND A.Status     = 0 
           AND ISNULL(A.UMAreaSeq, 0 ) = 0
           AND (@UserType = 1002 OR EXISTS ( SELECT TOP 1 1 
                                               FROM _TSLServiceCenter AS A WITH(NOLOCK)
                                              WHERE A.CompanySeq = @CompanySeq
                                                AND A.EmpSeq     = @EmpSeq    
                                           )
               )


		-------------------------------------------      
		-- 고객명 + 전화번호+ 관할지역 중복체크 
		-- 신규고객으로 체크되지 않은 경우, 동일한 고객명 + 전화번호 +관할지역으로 등록된 고객이 존재하면 등록할 수 없음 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT  ,  
                               @Status      OUTPUT  ,  
                               @Results     OUTPUT  ,  
                               1356                 , -- 동일한 @1에 동일한 @2과 @3가 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1356)  
                               @LanguageSeq         ,  
                               2421, N'지역'        , -- SELECT * FROM _TCADictionary WHERE Word like '%지역%'   
                               2819, N'고객명'      , -- SELECT * FROM _TCADictionary WHERE Word like '%고객명%'   
							   1396, N'전화번호'      -- SELECT * FROM _TCADictionary WHERE Word like '%전화번호%' 
							               
        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT  S.ClientName, S.Phone, S.UMAreaSeq
                        FROM (
                               SELECT A.ClientName, A.Phone, A.UMAreaSeq
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A', 'U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.ClientName, dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq) AS Phone, A.UMAreaSeq
                                 FROM _TSLASClient AS A WITH(NOLOCK)
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1
                                                    WHERE WorkingTag IN ('A', 'U')
                                                      AND Status	 = 0
                                                      AND ClientSeq  = A.ClientSeq
                                                 )
					         ) AS S
				       GROUP BY S.ClientName, S.Phone, S.UMAreaSeq
				      HAVING COUNT(1) > 1
                    ) AS B
                 ON B.ClientName = A.ClientName
				AND B.Phone      = A.Phone
                AND B.UMAreaSeq  = A.UMAreaSeq
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0  
		   AND ISNULL(A.NewClient, 0)  <> '1'

		-------------------------------------------      
		-- 이메일 유효성 검사 
		-- 1. 이메일 아이디부분, 도메인 중 하나만 입력된 경우 
		-- 2. 이메일 형식이 올바르지 않은 경우
		--  1) 이메일 아이디부분, 도메인에 @(at)이 입력된 경우 
		--  2) ex. aaa@bb.cc 형식이 아닌 경우 
		-------------------------------------------  

	    -- 1. 이메일 아이디부분, 도메인 중 하나만 입력된 경우 
		EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               2011                , -- @정확한 @1을(를) 입력하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 2011)  
                               @LanguageSeq        ,  
                               16432               , -- SELECT * FROM _TCADictionary WHERE Word like '%이메일주소%'  
							   N'이메일주소'      

	    UPDATE #BIZ_OUT_DataBlock1 
		   SET Result      = @Results    , 
			   MessageType = @MessageType,  
			   Status      = @Status  
		  FROM #BIZ_OUT_DataBlock1 AS A
	     WHERE A.WorkingTag IN ('A', 'U') 
		   AND A.Status     = 0 
		   AND ((A.Email <> '' AND A.Domain = '') 
			    OR (A.Email = '' AND A.Domain <> ''))


		-- 2. 이메일 형식이 올바르지 않은 경우
		--  1) 이메일 아이디부분, 도메인에 @(at)이 입력된 경우 

		UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag IN ('A', 'U') 
		   AND A.Status     = 0 
		   AND (A.Email <> '' AND A.Domain <> '') 
		   AND (CHARINDEX('@', A.Email) <> 0 
		        OR CHARINDEX('@', A.Domain) <> 0)

		 
		-- 2. 이메일 형식이 올바르지 않은 경우
		--  2) ex. aaa@bb.cc 형식이 아닌 경우 
        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results, 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
	     WHERE A.WorkingTag IN ('A', 'U') 
	       AND A.Status     = 0 
	       AND (A.Email <> '' AND A.Domain <> '') 
           AND CONVERT(NVARCHAR(400), A.Email + '@' + A.Domain) NOT LIKE '%_@__%.__%'	
				

	   --위 체크로직을 통과한 경우 이메일의 아이디부분과 도메인을 붙여서 UPDATE
        UPDATE #BIZ_OUT_DataBlock1 
	       SET Email = CONVERT(NVARCHAR(400), Email + '@' + Domain)
	      FROM #BIZ_OUT_DataBlock1 AS A
	     WHERE A.WorkingTag IN ('A', 'U') 
	       AND A.Status     = 0 
           AND (A.Email <> '' AND A.Domain <> '')


		-------------------------------------------      
		-- INSERT 번호부여(맨 마지막 처리) 
		-------------------------------------------         

        IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)
		BEGIN  
            
			-- 키값생성코드부분 시작  
			EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLASClient', 'ClientSeq', 1  
  
			-- Temp Table 에 생성된 키값 UPDATE  
			UPDATE #BIZ_OUT_DataBlock1  
			   SET ClientSeq = @Seq + 1
			  FROM #BIZ_OUT_DataBlock1 
			 WHERE WorkingTag = 'A' 
			   AND Status     = 0  

  
		END  

    END

RETURN
