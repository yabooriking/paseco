IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLClientQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLClientQuery
GO

/********************************
 설    명 - 고객등록:조회
 작 성 일 - 2020년 05월 11일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLClientQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    DECLARE @Phone      NVARCHAR(400),
	        @ClientName NVARCHAR(100),
            @UserType   INT          ,
            @EmpSeq     INT          ,
            @CustSeq    INT          ,
            @IsDefault  NCHAR(1) = '0' -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터의 관할지역에 해당하는 고객만 조회 가능 

    SELECT @Phone      = ISNULL(Phone     , ''),
	       @ClientName = ISNULL(ClientName, '') 
	  FROM #BIZ_IN_DataBlock1

    SELECT @EmpSeq   = EmpSeq    , 
           @CustSeq  = CustSeq   , 
           @UserType = UserType
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq

    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              ) OR @UserType = 1002 
    BEGIN

        SET @IsDefault = '1'


    END

    SELECT A.ClientName                                                                          , 
		   B.MinorName AS UMTendencyName                                                         ,
		   A.Email                                                                               ,
		   A.Address                                                                             ,
		   A.ClientSeq                                                                           ,
           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		   ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	  FROM _TSLASClient                      AS A WITH(NOLOCK)
	       LEFT OUTER JOIN _TDAUMinor        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
			                                                  AND B.MinorSeq         = A.UMTendency
           LEFT OUTER JOIN _TSLASArea        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq
                                                              AND C.UMAreaSeq        = A.UMAreaSeq
           LEFT OUTER JOIN _TSLServiceCenter AS D WITH(NOLOCK) ON D.CompanySeq       = C.CompanySeq
                                                              AND D.ServiceCenterSeq = C.ServiceCenterSeq
	 WHERE A.CompanySeq = @CompanySeq 
	   AND ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') LIKE @Phone + '%'
	   AND A.ClientName LIKE @ClientName + '%'
       AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND D.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND D.CustSeq = @CustSeq))

    --IF @UserType = 1001 
    --BEGIN

    --    SELECT A.ClientName                                                                          , 
		  --     B.MinorName AS UMTendencyName                                                         ,
		  --     A.Email                                                                               ,
		  --     A.Address                                                                             ,
		  --     A.ClientSeq                                                                           ,
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	   --   FROM _TSLASClient              AS A WITH(NOLOCK)
	   --        LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
			 --                                              AND B.MinorSeq   = A.UMTendency
	   --  WHERE A.CompanySeq = @CompanySeq 
	   --    AND ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') LIKE @Phone + '%'
	   --    AND A.ClientName LIKE @ClientName + '%'


    --END 

    --ELSE
    --BEGIN

    --    SELECT A.ClientName                                                                          , 
		  --     B.MinorName AS UMTendencyName                                                         ,
		  --     A.Email                                                                               ,
		  --     A.Address                                                                             ,
		  --     A.ClientSeq                                                                           ,
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	   --   FROM _TSLASClient               AS A WITH(NOLOCK)
	   --        LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
			 --                                              AND B.MinorSeq         = A.UMTendency
    --            JOIN _TSLASArea            AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq
    --                                                        AND C.UMAreaSeq        = A.UMAreaSeq
    --            JOIN _TSLServiceCenter     AS D WITH(NOLOCK) ON D.CompanySeq       = C.CompanySeq
    --                                                        AND D.ServiceCenterSeq = C.ServiceCenterSeq
    --            JOIN _TCAUser              AS E WITH(NOLOCK) ON E.CompanySeq       = D.CompanySeq
    --                                                        AND E.CustSeq          = D.CustSeq
                   
	   --  WHERE A.CompanySeq = @CompanySeq 
    --       AND  E.UserSeq    = @UserSeq
	   --    AND ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') LIKE @Phone + '%'
	   --    AND A.ClientName LIKE @ClientName + '%'


    --END

RETURN