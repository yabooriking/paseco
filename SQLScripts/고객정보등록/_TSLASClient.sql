CREATE TABLE _TSLASClient (
    CompanySeq       INT          ,
    ClientSeq        INT          ,
    ClientName       NVARCHAR(100),
    Phone            NVARCHAR(400),
    TelNo            NVARCHAR(400),
    Email            NVARCHAR(400),
    Address          NVARCHAR(280),
    UMTendency       INT          ,
    UMAreaSeq        INT          ,
    Remark           NVARCHAR(400),
    PgmSeq           INT          ,
    LastUserSeq      INT          ,
    LastDateTime     DATETIME     )

CREATE CLUSTERED INDEX IDX_TSLASClient ON _TSLASClient (CompanySeq, ClientSeq)

IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 521063)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLASClient', 1, 521063, '1', 1, GETDATE()END