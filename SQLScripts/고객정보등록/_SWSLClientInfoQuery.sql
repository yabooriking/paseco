IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLClientInfoQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLClientInfoQuery
GO

/********************************
 설    명 - 고객등록:상세조회
 작 성 일 - 2020년 05월 11일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLClientInfoQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    DECLARE @ClientSeq      INT          ,
            @UMSetDomain    NVARCHAR(200),
            @UMSetDomainSeq INT

    -- 사용자정의코드(이메일) 의 소분류값 - '직접입력'
    SELECT @UMSetDomain    = MinorName,
           @UMSetDomainSeq = MinorSeq
      FROM _TDAUMinor AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.MinorSeq   = 100115001

    SELECT @ClientSeq = ISNULL(ClientSeq, 0)
	  FROM #BIZ_IN_DataBlock1

	SELECT A.ClientName                              ,
		   CASE WHEN C.MinorSeq IS NULL                                                          
		        THEN @UMSetDomain                                                                
				ELSE C.MinorName END AS UMSetDomain  ,
		   CASE WHEN C.MinorSeq IS NULL                                                          
		        THEN @UMSetDomainSeq                                                             
				ELSE C.MinorSeq END AS UMSetDomainSeq,
		   A.Address                                 ,
		   ISNULL(B.MinorName, '') AS UMTendencyName ,
		   A.UMTendency                              ,
		   A.Remark                                  ,
           D.MinorName AS UMArea                     , 
           A.UMAreaSeq                               ,
           A.UMAreaSeq                               ,
		   A.ClientSeq                               ,
           SUBSTRING(A.Email, 0, CHARINDEX('@', A.Email) ) AS Email                              , -- 이메일 아이디 
		   SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email)) AS Domain               , -- 이메일 도메인
           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		   ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	  FROM _TSLASClient               AS A WITH(NOLOCK)
	       LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
		                                               AND B.MinorSeq   = A.UMTendency
		   LEFT OUTER JOIN _TDAUMinor AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq 
													   AND C.MajorSeq   = 100115
													   AND C.MinorName  = SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email))
	       LEFT OUTER JOIN _TDAUMinor AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq 
		                                               AND D.MinorSeq   = A.UMAreaSeq
	 WHERE A.CompanySeq = @CompanySeq 
	   AND A.ClientSeq  = @ClientSeq

RETURN
 