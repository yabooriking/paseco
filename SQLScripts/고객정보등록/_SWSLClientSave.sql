IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLClientSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLClientSave
GO

/********************************
 설    명 - 고객등록:저장
 작 성 일 - 2020년 05월 12일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLClientSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog @CompanySeq             ,      
                     @UserSeq                ,      
                     '_TSLASClient'          ,     
                     '#BIZ_OUT_DataBlock1'   , -- 템프테이블명      
                     'ClientSeq'             , -- 키가 여러개일 경우는 , 로 연결한다.      
                     @PgmSeq
 
    -- DELETE                      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
  
        DELETE _TSLASClient    
          FROM _TSLASClient             AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ClientSeq = A.ClientSeq  
         WHERE A.CompanySeq = @CompanySeq  
           AND B.WorkingTag = 'D' 
		   AND B.Status     = 0  
  
        IF @@ERROR <> 0 RETURN      

		   
    END  
  
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  

       UPDATE _TSLASClient    
          SET ClientName   = ISNULL(B.ClientName, '')                                         ,
              Email        = ISNULL(B.Email, '')                                              ,
              Address      = ISNULL(B.Address, 0)	                                          ,
              UMTendency   = ISNULL(B.UMTendency, 0)                                          ,
              UMAreaSeq    = ISNULL(B.UMAreaSeq, 0)                                           ,
              Remark       = ISNULL(B.Remark, '')  	                                          ,
			  PgmSeq       = @PgmSeq                                                          , 
              LastUserSeq  = @UserSeq                                                         ,
              LastDateTime = GETDATE()                                                        ,
              Phone		   = dbo._FWCOMEncrypt(B.Phone, N'_TSLASClient', 'Phone', @CompanySeq), 
			  TelNo		   = dbo._FWCOMEncrypt(B.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq)
         FROM _TSLASClient             AS A  
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.ClientSeq = A.ClientSeq  
        WHERE A.CompanySeq = @CompanySeq   
          AND B.WorkingTag = 'U' 
		  AND B.Status     = 0  
  
        IF @@ERROR <> 0 RETURN  

       
    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLASClient(CompanySeq  , ClientSeq   , ClientName  , Email       , Address     , 
                                 UMTendency  , UMAreaSeq   , Remark      , PgmSeq      , LastUserSeq , 
                                 LastDateTime,	         
								 Phone       , 
                                 TelNo)   
		SELECT  @CompanySeq, ClientSeq , ClientName, Email      , Address   ,
                 UMTendency, UMAreaSeq ,Remark    , @PgmSeq   , @UserSeq   , 
                 GETDATE() ,
                 dbo._FWCOMEncrypt(Phone, N'_TSLASClient', 'Phone', @CompanySeq), 
                 dbo._FWCOMEncrypt(TelNo, N'_TSLASClient', 'TelNo', @CompanySeq)        															    
          FROM #BIZ_OUT_DataBlock1   
         WHERE WorkingTag = 'A' 
		   AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN  


    END

	-- Check SP에서 합쳤던 이메일주소에서 아이디부분빼고 제거 
	UPDATE #BIZ_OUT_DataBlock1 
	   SET Email = SUBSTRING(A.Email, 0, CHARINDEX('@', A.Email))
	  FROM #BIZ_OUT_DataBlock1 AS A 
	 WHERE A.WorkingTag IN ('A', 'U')
	   AND A.Status     = 0


RETURN