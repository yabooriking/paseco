IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqListQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqListQuery
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청조회-조회   
 작성일 - 2020.04.27
 작성자 - 유태우
*************************************************************************************************/  

CREATE PROCEDURE _SWLGASTransReqListQuery      
    @ServiceSeq     INT          = 0 , 
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0     
AS           
    DECLARE   @BizUnit            INT         ,  
              @ReqDateFr          NCHAR(8)    ,  
              @ReqDateTo          NCHAR(8)    ,
              @CompleteWishDateFr NCHAR(8)    ,  
              @CompleteWishDateTo NCHAR(8)    ,
              @InWHSeq            INT         ,
              @SMProgressType     INT         ,
              @ReqNo              NVARCHAR(30),
              @InOutReqDetailKind INT

              
    SELECT @BizUnit            = ISNULL(BizUnit, 0)            ,
           @ReqDateFr          = ISNULL(ReqDateFr, '')         ,
           @ReqDateTo          = ISNULL(ReqDateTo, '')         ,
           @InWHSeq            = ISNULL(InWHSeq, 0)            ,
           @CompleteWishDateFr = ISNULL(CompleteWishDateFr, ''),
           @CompleteWishDateTo = ISNULL(CompleteWishDateTo, ''),
           @InOutReqDetailKind = ISNULL(InOutReqDetailKind, 0) ,
           @SMProgressType     = ISNULL(SMProgressType, 0)     ,
           @ReqNo              = ISNULL(ReqNo, '')
      FROM #BIZ_IN_DataBlock1
       
    CREATE TABLE #Temp_ASTransReqProg(
        IDX_NO INT IDENTITY        , 
        ReqSeq INT                 , 
        CompleteCHECK INT          , 
        SMProgressType INT NULL    , 
        IsNotQC NCHAR(1)           ,
        Confirm       INT          ,  
        ConfirmEmp    NVARCHAR(100),  
        ConfirmDate   NCHAR(8)     ,  
        ConfirmReason NVARCHAR(500),  
        IsStop        NCHAR(1)     ,  
        StopEmp       NVARCHAR(100),  
        StopDate      NCHAR(8)     ,  
        StopReason    NVARCHAR(500))
    
    INSERT INTO #Temp_ASTransReqProg(ReqSeq, CompleteCHECK)      
    SELECT A.ReqSeq, -1      
      FROM _TLGASTransReq AS A WITH(NOLOCK)
     WHERE A.CompanySeq         = @CompanySeq
       AND (@ReqDateFr          = '' OR  A.ReqDate         >= @ReqDateFr)  
       AND (@ReqDateTo          = '' OR  A.ReqDate         <= @ReqDateTo)
       AND (@InWHSeq            = 0  OR  A.InWHSeq         = @InWHSeq)
       AND (@CompleteWishDateFr = '' OR A.CompleteWishDate >= @CompleteWishDateFr)  
       AND (@CompleteWishDateTo = '' OR A.CompleteWishDate <= @CompleteWishDateTo)
       AND (@ReqNo              = '' OR A.ReqNo            LIKE @ReqNo + '%')
       
    EXEC _SWCOMConfirmStopUpdate @CompanySeq, '#Temp_ASTransReqProg', 'ReqSeq', '', '', @PgmSeq 
    
    EXEC _SWCOMProgStatus @CompanySeq, '_TLGASTransReqItem', 1036004, '#Temp_ASTransReqProg', 'ReqSeq', '', '', '', '', '', '', '', 'CompleteCHECK', 1, 'Qty', 'STDQty', '', '', 'ReqSeq', 'ReqSerl', '', '_TLGASTransReq', @PgmSeq  
  
    /** 품목의 진행상태에 따른 마스터 진행상태 보정로직 (START) 20190902 성지원  **/
    CREATE TABLE #Temp_InOutReqItem(
        IDX_NO     INT IDENTITY         , 
        ReqSeq            INT           , 
        ReqSerl           INT           , 
        CompleteCHECK     INT           , 
        SMProgressType   INT            ,
        Confirm           INT           ,  
        ConfirmEmp        NVARCHAR(100) ,  
        ConfirmDate       NCHAR(8)      ,  
        ConfirmReason     NVARCHAR(500) ,  
        IsStop            NCHAR(1)      ,  
        StopEmp           NVARCHAR(100) ,  
        StopDate          NCHAR(8)      ,  
        StopReason        NVARCHAR(500))

    INSERT INTO #Temp_InOutReqItem (ReqSeq, ReqSerl)
    SELECT A.ReqSeq, B.ReqSerl
      FROM #Temp_ASTransReqProg    AS A WITH(NOLOCK)
           JOIN _TLGASTransReqItem AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                    AND B.ReqSeq     = A.ReqSeq

    EXEC _SWCOMConfirmStopUpdate @CompanySeq, '#Temp_InOutReqItem', 'ReqSeq', 'ReqSerl', '', @PgmSeq

    -- 품목 진행상태 수집
    EXEC _SWCOMProgStatus @CompanySeq, '_TLGASTransReqItem', 1036004, '#Temp_InOutReqItem', 'ReqSeq', 'ReqSerl', '', '', '', '', '', '', 'CompleteCHECK', 1, 'Qty', 'STDQty', '', '', 'ReqSeq', 'ReqSerl', '', '_TLGASTransReq', @PgmSeq      

    -- 품목 진행상태가 모두 같거나, 품목이 1건만 존재하는 출하의뢰 건 수집
    CREATE TABLE #Temp_InOutReqUnique (ReqSeq INT)

    INSERT INTO #Temp_InOutReqUnique (ReqSeq)
    SELECT A.ReqSeq
      FROM _TLGASTransReq            AS A WITH(NOLOCK) 
           JOIN _TLGASTransReqItem   AS B WITH(NOLOCK) ON A.CompanySeq = B.CompanySeq
                                                      AND A.ReqSeq     = B.ReqSeq
           JOIN #Temp_ASTransReqProg AS C WITH(NOLOCK) ON C.ReqSeq     = A.ReqSeq
     WHERE A.CompanySeq = @CompanySeq
     GROUP BY A.ReqSeq 
    HAVING COUNT(1) = 1
     UNION ALL
    SELECT ReqSeq
      FROM #Temp_InOutReqItem
     GROUP BY ReqSeq
    HAVING COUNT(1) = 1

    -- 품목의 진행상태가 중단 / 진행중단 / 완료 인 품목 삭제
    DELETE #Temp_InOutReqItem WHERE ISNULL(IsStop, '') = '1' OR CompleteCHECK = 40

    -- 더이상 진행될 건이 없는 출하의뢰 건의 경우 완료로 간주
    UPDATE #Temp_ASTransReqProg
       SET CompleteCHECK = 40 -- 완료
     WHERE CompleteCHECK = 20 -- 진행
       AND ReqSeq NOT IN (SELECT ReqSeq FROM #Temp_InOutReqItem)
       AND ReqSeq NOT IN (SELECT ReqSeq FROM #Temp_InOutReqUnique)

    /** 품목의 진행상태에 따른 마스터 진행상태 보정로직 (END)  **/

    UPDATE #Temp_ASTransReqProg               
       SET SMProgressType = (SELECT CASE WHEN (A.IsStop = '1' AND B.MinorSeq = 1037006) OR --중단&진행 이거나
                                              (A.IsStop = '1' AND B.MinorSeq = 1037008) THEN 1037007 --중단&완료 일 때 진행중단
                                         WHEN B.MinorSeq = 1037009 THEN 1037009 -- 완료  
                                         WHEN A.IsStop = '1' THEN 1037005 -- 중단    
                                         ELSE B.MinorSeq END)    
      FROM #Temp_ASTransReqProg       AS A     
           LEFT OUTER JOIN _TDASMinor AS B WITH(NOLOCK) ON B.MajorSeq      = 1037    
                                                       AND B.CompanySeq    = @CompanySeq 
                                                       AND A.CompleteCHECK = B.Minorvalue    

    SELECT DISTINCT 
           A.ReqSeq                       AS ReqSeq                      ,  
           ISNULL(B.BizUnitName, '')      AS BizUnitName                 ,  
           ISNULL(A.BizUnit, 0)           AS BizUnit                     ,
           ISNULL(A.ReqNo, '')            AS ReqNo                       ,   
           ISNULL(E.WHName, '')           AS InWHName                    ,  
           ISNULL(A.InWHSeq, 0)           AS InWHSeq                     ,
           ISNULL(H.EmpName, '')          AS EmpName                     ,  
           ISNULL(A.EmpSeq, 0)            AS EmpSeq                      ,       
           ISNULL(A.ReqDate, '')          AS ReqDate                     ,
           ISNULL(A.CompleteWishDate, '') AS CompleteWishDate            ,
           ISNULL(A.Remark, '')           AS Remark                      ,
           ISNULL(A.InOutReqDetailKind,0) AS InOutReqDetailKind          ,
           ISNULL(J.MinorName,'')         AS InOutReqDetailKindName      ,  
           ISNULL(A.InOutReqType, 0)      AS InOutReqType                ,  
           CASE ISNULL(A.InOutReqType, 0) WHEN 30 THEN '기타출고요청'  
                                          WHEN 40 THEN '기타입고요청'  
                                          WHEN 31 THEN '자재기타출고요청'  
                                          WHEN 41 THEN '자재기타입고요청'  
                                          WHEN 50 THEN '위탁출하요청'  
                                          WHEN 80 THEN '이동요청' 
                                          WHEN 81 THEN '적송요청' 
                                          WHEN 82 THEN '자재이동요청' 
                                          WHEN 83 THEN '자재적송요청'  
                                          WHEN 100 THEN '수탁품출고요청'  
                                          WHEN 110 THEN '수탁품입고요청' 
                                          ELSE '' END AS InOutReqTypeName,
           ISNULL((SELECT MinorName FROM _TDASMinor WHERE CompanySeq = A.CompanySeq AND MinorSeq = X.SMProgressType), 0) AS SMProgressTypeName,  
           ISNULL(X.Confirm,0)              AS Confirm                    ,  
           ISNULL(X.ConfirmEmp,'')          AS ConfirmEmp                 ,  
           ISNULL(X.ConfirmDate,'')         AS ConfirmDate                ,  
           ISNULL(X.ConfirmReason,'')       AS ConfirmReason              ,  
           ISNULL(X.IsStop,'')              AS IsStop                     ,  
           ISNULL(X.StopEmp,'')             AS StopEmp                    ,  
           ISNULL(X.StopDate,'')            AS StopDate                   ,  
           ISNULL(X.StopReason,'')          AS StopReason                 ,
           A.LastDateTime     AS LastDateTime                                    ,
           (SELECT UserName FROM _TCAUser WHERE CompanySeq = @CompanySeq AND UserSeq = A.LastUserSeq) AS LastUserName,
           ISNULL(A.IsAuto,'') AS IsAuto 
      FROM #Temp_ASTransReqProg      AS X
           JOIN _TLGASTransReq       AS A WITH (NOLOCK) ON X.ReqSeq        = A.ReqSeq
           JOIN _TLGASTransReqItem   AS C WITH (NOLOCK) ON A.ReqSeq        = C.ReqSeq
                                                       AND (C.ReqSerl IS NOT NULL AND C.ReqSerl = (SELECT MIN(ReqSerl) FROM _TLGASTransReqItem WHERE CompanySeq = @CompanySeq  AND ReqSeq = A.ReqSeq))  
                                                       AND A.CompanySeq = C.CompanySeq
           LEFT OUTER JOIN _TDABizUnit AS B WITH (NOLOCK) ON B.CompanySeq  = A.CompanySeq 
                                                         AND B.BizUnit     = A.BizUnit
           LEFT OUTER JOIN _TDAWH      AS E WITH (NOLOCK) ON E.CompanySeq  = A.CompanySeq 
                                                         AND E.WHSeq       = A.InWHSeq
           LEFT OUTER JOIN _TDAEmp     AS H WITH (NOLOCK) ON H.CompanySeq  = A.CompanySeq 
                                                         AND H.EmpSeq      = A.EmpSeq
           LEFT OUTER JOIN _TDAUMinor  AS J WITH (NOLOCK) ON J.CompanySeq  = @CompanySeq 
                                                         AND J.MinorSeq    = A.InOutReqDetailKind
                                                       --AND J.MajorSeq    = 8012
     WHERE (@InOutReqDetailKind = 0 OR A.InOutReqDetailKind = @InOutReqDetailKind)
       AND (@SMProgressType  = 0                OR 
           (X.SMProgressType = @SMProgressType) OR 
           (@SMProgressType  = 1037010         AND X.SMProgressType IN ( SELECT MinorSeq 
                                                                           FROM _TDASMinorValue WITH(NOLOCK) 
                                                                          WHERE CompanySeq = @CompanySeq 
                                                                            AND MajorSeq   = 1037 
                                                                            AND Serl       = 1001 
                                                                            AND ValueText  = '1' ))) --20150608 이범확 추가
       AND A.CompanySeq         = @CompanySeq
     ORDER BY ISNULL(A.BizUnit, 0), ISNULL(A.ReqDate,''), ISNULL(A.ReqNo,'')
 
RETURN      