﻿--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72905) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72905, N'착불', null, N'착불', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72905, N'착불', null, N'착불', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72906) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
end 


--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 65701) 
begin 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 65701, N'고객상담정보', null, N'고객상담정보', 0, getdate(), 0 
end 

