﻿--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72826) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72826, N'코드분류', null, N'코드분류', 0, getdate(), 0 
end 


--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 65694) 
begin 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 65694, N'서비스코드등록', null, N'서비스코드등록', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 65695) 
begin 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 65695, N'AS자재반납입력', null, N'AS자재반납입력', 0, getdate(), 0 
end 

