﻿--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionaryCommon WHERE WordSeq = 65693) 
BEGIN
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 65693, N'서비스관할지역등록', NULL, N'서비스관할지역등록', 0,GETDATE(), 0 
END 


--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72811) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72811, N'회사전화번호', NULL, N'회사전화번호', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72812) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72812, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72813) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72813, N'보조전화번호', NULL, N'보조전화번호', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72814) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72814, N'성향', NULL, N'성향', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72815) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72815, N'성향', NULL, N'성향', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72816) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72816, N'신규고객', NULL, N'신규고객', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72817) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72817, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72818) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72818, N'관할서비스센터', NULL, N'관할서비스센터', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72819) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72819, N'출장거리미입력', NULL, N'출장거리미입력', 0,GETDATE(), 0 
END 
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary WHERE WordSeq = 72820) 
BEGIN
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 72820, N'출장거리', NULL, N'출장거리', 0,GETDATE(), 0 
END 

