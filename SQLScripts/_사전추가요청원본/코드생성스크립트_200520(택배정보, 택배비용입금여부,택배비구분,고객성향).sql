﻿--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72906) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72906, N'택배정보', null, N'택배정보', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72910) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72910, N'택배비용입금여부', null, N'택배비용입금여부', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72911) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72911, N'택배비구분', null, N'택배비구분', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72912) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72912, N'고객성향', null, N'고객성향', 0, getdate(), 0 
end 

