﻿--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 65702) 
begin 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 65702, N'서비스BOM등록', null, N'서비스BOM등록', 0, getdate(), 0 
end 


--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72914) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72914, N'품목자산분류(제품)', null, N'품목자산분류(제품)', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58501) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58501, N'品目资材分类(资材)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 58501, N'품목자산분류(자재)', null, N'품목자산분류(자재)', 0, getdate(), 0 
end 

