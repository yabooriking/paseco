﻿--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionaryCommon WHERE WordSeq = 65689) 
BEGIN 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 65689, N'서비스센터인원등록', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 65689, N'Service Center Personnel Reg.', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 65689, N'서비스센터인원등록', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 65689, N'服务中心人员登记', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 65689, N'服務中心人員登記', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 65689, N'서비스센터인원등록', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 65689, N'서비스센터인원등록', null, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 65689, N'서비스센터인원등록', null, N'서비스센터인원등록', 0, GETDATE(), 0 
END 

-- SHID 칼럼(Seq)은 DataFieldName명을 Caption에 쓰므로 아래 스크립트는 패치 안함.
--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionary where WordSeq = 72806) 
BEGIN 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
INSERT INTO _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72806, N'서비스센터인원내부코드', NULL, N'서비스센터인원내부코드', 0, GETDATE(), 0 
END 

