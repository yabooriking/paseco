﻿--------------------------------------------------------------------------------------------------------------
--업무사전 (시트/컨트롤/단어/라벨) _TCADictionary
--------------------------------------------------------------------------------------------------------------
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72916) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72916, N'주문수집', null, N'주문수집', 0, getdate(), 0 
end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 72917) 
begin 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 7, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 8, 72917, N'주문취소', null, N'주문취소', 0, getdate(), 0 
end 

