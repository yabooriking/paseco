IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceReceiptInfoQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceReceiptInfoQuery
GO

/********************************
 설    명 - 서비스접수조회:조회
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceReceiptInfoQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    DECLARE @ServiceCenterSeq     INT           ,
            @ServiceReceiptDateFr NCHAR(8)      ,
            @ServiceReceiptDateTo NCHAR(8)      ,
            @VisitDateFr          NCHAR(8)      ,
            @VisitDateTo          NCHAR(8)      ,
            @SMCompleteStatus     INT           ,
            @UMServiceKind        INT           ,
            @UMReceiptKind        INT           ,
            @ReceiveUserSeq       INT           ,
            @ServiceReceiptNo     NVARCHAR(20)  ,
            @ClientSeq            INT           ,
            @Phone                NVARCHAR(100) ,
            @ItemSeq              INT           ,
            @ItemClassSSeq        INT           ,
            @SMPayStatus          INT           ,
            @SMPayKind            INT           ,
            @UMAreaSeq            INT           ,
            -- 이상 조회조건                                  
            @UserType             INT           ,
            @EmpSeq               INT           ,
            @CustSeq              INT           ,
            @IsDefault            NCHAR(1) = '0'  -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터의 관할지역에 해당하는 고객의 서비스접수건만 조회 가능 

    SELECT @ServiceCenterSeq     = ISNULL(ServiceCenterSeq, 0)     ,
           @ServiceReceiptDateFr = ISNULL(ServiceReceiptDateFr, ''),
           @ServiceReceiptDateTo = ISNULL(ServiceReceiptDateTo, ''),
           @VisitDateFr          = ISNULL(VisitDateFr, '')         ,
           @VisitDateTo          = ISNULL(VisitDateTo, '')         ,
           @SMCompleteStatus     = ISNULL(SMCompleteStatus, 0)     ,
           @UMServiceKind        = ISNULL(UMServiceKind, 0)        ,
           @UMReceiptKind        = ISNULL(UMReceiptKind, 0)        ,
           @ReceiveUserSeq       = ISNULL(ReceiveUserSeq, 0)       ,
           @ServiceReceiptNo     = ISNULL(ServiceReceiptNo, '')    , 
           @ClientSeq            = ISNULL(ClientSeq, 0)            ,
           @Phone                = ISNULL(Phone, '')               ,
           @ItemSeq              = ISNULL(ItemSeq, 0)              ,
           @ItemClassSSeq        = ISNULL(ItemClassSSeq, 0)        ,
           @SMPayStatus          = ISNULL(SMPayStatus, 0)          ,
           @SMPayKind            = ISNULL(SMPayKind, 0)            ,
           @UMAreaSeq            = ISNULL(UMAreaSeq, 0)            
      FROM #BIZ_IN_DataBlock1 

    SELECT @UserType = ISNULL(UserType, 0),
           @EmpSeq   = ISNULL(EmpSeq, 0)  ,
           @CustSeq  = ISNULL(CustSeq, 0)  
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq 

    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              )
    BEGIN

        SET @IsDefault = '1'


    END

    SELECT '' AS SMCompleteStatusName       ,
           A.ServiceReceiptNo               ,
           A.ServiceReceiptDate             ,
           B.MinorName AS UMServiceKindName ,
           C.MinorName AS UMReceiptKindName ,
           D.ItemName                       ,
           E.MinorName AS ItemClassSName    ,
           F.ASCode AS TroubleName          ,
           A.Memo                           ,
           A.Remark                         ,
           G.UserName AS ReceiveUserName    ,
           A.BuyDate                        ,
           H.MinorName AS SMWarrantyKindName,
           I.ClientName                     ,
           I.Email                          ,
           I.Address                        ,
           J.MinorName AS UMArea            ,
           K.ServiceCenterName              ,
           L.ASEmpName                      ,
           A.VisitDate                      ,
           M.MinorName AS UMVisitTimeName   ,
           A.ReceiptClientName              ,
           N.MinorName AS SMPayKindName     ,
           O.MinorName AS SMDelvPayKindName ,
           A.ReceiptDate                    , 
           A.ServiceReceiptSeq              ,
           ISNULL(dbo._FWCOMDecrypt(I.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		   ISNULL(dbo._FWCOMDecrypt(I.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo          
      FROM _TSLASServiceReceipt                   AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDAUMinor             AS B WITH(NOLOCK)  ON B.CompanySeq       = A.CompanySeq
                                                                    AND B.MinorSeq         = A.UMServiceKind
           LEFT OUTER JOIN _TDAUMinor             AS C WITH(NOLOCK)  ON C.CompanySeq       = A.CompanySeq
                                                                    AND C.MinorSeq         = A.UMReceiptKind
           LEFT OUTER JOIN _TDAUMinorValue        AS C1 WITH(NOLOCK) ON C1.CompanySeq      = A.CompanySeq
                                                                    AND C1.MinorSeq        = A.UMReceiptKind
                                                                    AND C1.Serl            = 10000001
           LEFT OUTER JOIN _TDAItem               AS D WITH(NOLOCK)  ON D.CompanySeq       = A.CompanySeq 
                                                                    AND D.ItemSeq          = A.ItemSeq 
           LEFT OUTER JOIN _TDAUMinor             AS E WITH(NOLOCK)  ON E.CompanySeq       = A.CompanySeq
                                                                    AND E.MinorSeq         = A.ItemClassSSeq
           LEFT OUTER JOIN _TSLASCode             AS F WITH(NOLOCK)  ON F.CompanySeq       = A.CompanySeq
                                                                    AND F.ASCodeSeq        = A.TroubleSeq
           LEFT OUTER JOIN _TCAUser               AS G WITH(NOLOCK)  ON G.CompanySeq       = A.CompanySeq
                                                                    AND G.UserSeq          = A.ReceiveUserSeq
           LEFT OUTER JOIN _TDASMinor             AS H WITH(NOLOCK)  ON H.CompanySeq       = A.CompanySeq
                                                                    AND H.MinorSeq         = A.SMWarrantyKind
           LEFT OUTER JOIN _TSLASClient           AS I WITH(NOLOCK)  ON I.CompanySeq       = A.CompanySeq
                                                                    AND I.ClientSeq        = A.ClientSeq
           LEFT OUTER JOIN _TDAUMinor             AS J WITH(NOLOCK)  ON J.CompanySeq       = I.CompanySeq
                                                                    AND J.MinorSeq         = I.UMAreaSeq  
           LEFT OUTER JOIN _TSLServiceCenter      AS K WITH(NOLOCK)  ON K.CompanySeq       = A.CompanySeq
                                                                    AND K.ServiceCenterSeq = A.ServiceCenterSeq
           LEFT OUTER JOIN _TSLServiceCenterEmpIn AS L WITH(NOLOCK)  ON L.CompanySeq       = A.CompanySeq
                                                                    AND L.ASEmpSeq         = A.ASEmpSeq
           LEFT OUTER JOIN _TDAUMinor             AS M WITH(NOLOCK)  ON M.CompanySeq       = A.CompanySeq
                                                                    AND M.MinorSeq         = A.UMVisitTime
           LEFT OUTER JOIN _TDASMinor             AS N WITH(NOLOCK)  ON N.CompanySeq       = A.CompanySeq
                                                                    AND N.MinorSeq         = A.SMPayKind
           LEFT OUTER JOIN _TDASMinor             AS O WITH(NOLOCK)  ON O.CompanySeq       = A.CompanySeq
                                                                    AND O.MinorSeq         = A.SMDelvPayKind
     WHERE A.CompanySeq = @CompanySeq
       AND (@ServiceCenterSeq     = 0  OR A.ServiceCenterSeq   =     @ServiceCenterSeq)
       AND (@ServiceReceiptDateFr = '' OR A.ServiceReceiptDate >=    @ServiceReceiptDateFr)  
       AND (@ServiceReceiptDateTo = '' OR A.ServiceReceiptDate <=    @ServiceReceiptDateTo)
       AND (@VisitDateFr          = '' OR A.VisitDate          >=    @VisitDateFr)  
       AND (@VisitDateTo          = '' OR A.VisitDate          <=    @VisitDateTo)
       --AND 추후 처리상태 조회조건 추가                             
       AND (@UMServiceKind        = 0  OR A.UMServiceKind      =     @UMServiceKind)
       AND (@UMReceiptKind        = 0  OR A.UMReceiptKind      =     @UMReceiptKind)
       AND (@ReceiveUserSeq       = 0  OR A.ReceiveUserSeq     =     @ReceiveUserSeq)
       AND (@ServiceReceiptNo     = '' OR A.ServiceReceiptNo   =     @ServiceReceiptNo)
       AND (@ClientSeq            = '' OR A.ClientSeq          =     @ClientSeq)
       AND (@ItemSeq              = '' OR A.ItemSeq            =     @ItemSeq)
       AND (@ItemClassSSeq        = 0  OR A.ItemClassSSeq      =     @ItemClassSSeq)
       AND (@SMPayKind            = 0  OR A.SMPayKind          =     @SMPayKind)
       AND (@UMAreaSeq            = 0  OR I.UMAreaSeq          =     @UMAreaSeq)
       AND (@Phone                = '' OR ISNULL(dbo._FWCOMDecrypt(I.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') LIKE  @Phone + '%')
       -- 접수유형의 택배 추가정보정의에 체크된 건에 한해서만 택배비용입금여부 검색조건이 적용됨.
       AND (@SMPayStatus          = 0  OR ((@SMPayStatus = 1070001 AND A.ReceiptDate <> '') OR (@SMPayStatus = 1070002 AND A.ReceiptDate = '') AND C1.ValueText = '1')) 
       -- 이상 조회조건  
       AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND K.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND K.CustSeq = @CustSeq))
RETURN
