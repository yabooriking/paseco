IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallStockQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallStockQuery
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 재고조회
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallStockQuery
    @ServiceSeq    INT          = 0 ,
    @WorkingTag    NVARCHAR(10) = '',
    @CompanySeq    INT          = 1 ,
    @LanguageSeq   INT          = 1 ,
    @UserSeq       INT          = 0 ,
    @PgmSeq        INT          = 0 ,
    @IsTransaction BIT          = 0   

AS    

    DECLARE @BizUnit INT, 
            @STDDate NCHAR(8)

    CREATE TABLE #MallWH (
        MallName NVARCHAR(100), 
        WHSeq    INT          , 
        BizUnit  INT          ,
        ItemSeq  INT          
    )
    
    CREATE TABLE #GetInOutItem (ItemSeq INT)

    CREATE TABLE #GetInOutStock (
        BizUnit     INT           ,    
        WHSeq       INT           ,    
        MainWHSeq   INT           ,    
        ItemSeq     INT           ,    
        UnitSeq     INT           ,    
        PrevQty     DECIMAL(19, 5),      
        InQty       DECIMAL(19, 5),      
        OutQty      DECIMAL(19, 5),      
        StockQty    DECIMAL(19, 5),      
        STDPrevQty  DECIMAL(19, 5),      
        STDInQty    DECIMAL(19, 5),      
        STDOutQty   DECIMAL(19, 5),      
        STDStockQty DECIMAL(19, 5),      
        StockAmt    DECIMAL(19, 5)
     )    

    CREATE TABLE #GetInOutStockSUM (ItemSeq INT, StockQty DECIMAL(19, 5))

    INSERT INTO #MallWH (MallName, WHSeq, BizUnit, ItemSeq)
    SELECT A.MallName, ISNULL(B.ERPWHSeq, 0), ISNULL(C.BizUnit, 0), A.ItemSeq
      FROM #BIZ_OUT_DataBlock2              AS A
           LEFT OUTER JOIN _TSLShoppingMall AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                             AND B.MallName   = A.MallName
           LEFT OUTER JOIN _TDAWH           AS C WITH(NOLOCK) ON C.CompanySeq = B.CompanySeq
                                                             AND C.WHSeq      = B.ERPWHSeq

    INSERT INTO #GetInOutItem (ItemSeq)
    SELECT DISTINCT ItemSeq
      FROM #BIZ_OUT_DataBlock2
     WHERE ItemSeq <> 0

    SELECT @STDDate = CONVERT(NCHAR(8), GETDATE(), 112)

    EXEC _SWLGGetInOutStock @CompanySeq   = @CompanySeq,
                            @BizUnit      = 0          ,
                            @DateFr       = @STDDate   ,
                            @DateTo       = @STDDate   ,
                            @WHSeq        = 0          ,
                            @SMWHKind     = 0          ,
                            @IsSubDisplay = '1'        ,
                            @IsOnlyStock  = '1'        ,
                            @IsStockAmt   = '0'        ,
                            @StockAmtType = 0          ,
                            @UnitType     = 1          ,
                            @ConvUnitSeq  = 0          ,
                            @QryType      = 'S'        ,
                            @IsTrustCust  = ''
    
    INSERT INTO #GetInOutStockSUM (ItemSeq, StockQty)
    SELECT ItemSeq, SUM(StockQty)
      FROM #GetInOutStock
     GROUP BY ItemSeq

    UPDATE #BIZ_OUT_DataBlock2
       SET StockQty = C.StockQty
      FROM #BIZ_OUT_DataBlock2    AS A
           JOIN #MallWH           AS B ON B.MallName = A.MallName
                                      AND B.ItemSeq  = A.ItemSeq
           JOIN #GetInOutStockSUM AS C ON C.ItemSeq  = B.ItemSeq
     WHERE B.WHSeq    = 0
       AND B.BizUnit  = 0
       AND B.ItemSeq <> 0

    UPDATE #BIZ_OUT_DataBlock2
       SET StockQty = C.StockQty
      FROM #BIZ_OUT_DataBlock2 AS A
           JOIN #MallWH        AS B ON B.MallName = A.MallName
                                   AND B.ItemSeq  = A.ItemSeq
           JOIN #GetInOutStock AS C ON C.ItemSeq  = B.ItemSeq
                                   AND C.BizUnit  = B.BizUnit
                                   AND C.WHSeq    = B.WHSeq
     WHERE B.WHSeq   <> 0
       AND B.BizUnit <> 0
       AND B.ItemSeq <> 0
       
    UPDATE #BIZ_OUT_DataBlock2 SET StockQty = 0 WHERE ItemSeq = 0
    
RETURN