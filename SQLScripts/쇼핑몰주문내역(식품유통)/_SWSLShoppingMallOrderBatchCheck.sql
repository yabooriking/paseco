IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderBatchCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderBatchCheck
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 수주생성체크
 작 성 일 - 2020년 05월 20일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROCEDURE _SWFPSLShoppingMallOrderBatchCheck   
    @ServiceSeq    INT          = 0 ,  
    @WorkingTag    NVARCHAR(10) = '',  
    @CompanySeq    INT          = 1 ,  
    @LanguageSeq   INT          = 1 ,  
    @UserSeq       INT          = 0 ,  
    @PgmSeq        INT          = 0 ,  
    @IsTransaction BIT          = 0    

AS

    DECLARE @MessageType      INT          ,
            @Status           INT          ,
            @Results          NVARCHAR(250),
            @BizUnit          INT          ,
            @IsCustSalesMan   NCHAR(1)     ,
            @IsCfmUsed        NCHAR(1)     , -- 수주 확정 사용여부 
            @IsCloseUsed      NCHAR(1)       -- 업무 마감 사용여부
     
    SELECT TOP 1 @BizUnit = BIzUnit 
      FROM #BIZ_OUT_DataBlock2 

    -- 환경설정 : 영업담당 사용여부 
    EXEC dbo._SWCOMEnv @CompanySeq, 8001, @UserSeq, @@PROCID, @IsCustSalesMan OUTPUT -- 영업담당 사용여부

    -- 수주확정 사용여부 
    SELECT @IsCfmUsed = IsNotUsed 
      FROM _TCOMConfirmDef WITH(NOLOCK)
     WHERE CompanySeq = @CompanySeq 
       AND ConfirmSeq = 6303 

    -- 영업업무월마감 사용여부 
    SELECT @IsCloseUsed = IsUseClosing 
      FROM _TCOMClosingDefine WITH(NOLOCK)
     WHERE CompanySeq   = @CompanySeq 
       AND ClosingSeq   = 62

    IF @IsCloseUsed = '1'
    BEGIN
    
    -- 영업업무월마감 여부체크 
    EXEC dbo._SWCOMMessage @MessageType OUTPUT, 
                           @Status      OUTPUT, 
                           @Results     OUTPUT, 
                           2                  , -- 업무 마감이 되었습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 2)
                           @LanguageSeq                      

    UPDATE #BIZ_OUT_DataBlock2
       SET Result      = @Results    ,
           MessageType = @MessageType,
           Status      = @Status
      FROM #BIZ_OUT_DataBlock2 AS A
           JOIN _TSLOrder      AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                AND B.OrderSeq   = A.OrderSeq
           JOIN _TCOMClosingYM AS C WITH(NOLOCK) ON C.CompanySeq = B.CompanySeq
                                                AND C.UnitSeq    = B.BizUnit
                                                AND C.ClosingYM  = LEFT(B.OrderDate, 6)
     WHERE A.Status     = 0
       AND C.DtlUnitSeq = 1
       AND C.ClosingSeq = 62
       AND C.IsClose    = '1'    


    END

    IF @WorkingTag = 'P' -- 수주처리
    BEGIN
        

        -- 존재유무확인
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               1365               , -- @1이(가) 존재하지 않습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1365)
                               @LanguageSeq       ,
                               23963, N'주문'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2 AS A
         WHERE A.Status = 0
           AND NOT EXISTS (SELECT 1
                             FROM _TSLFPShoppingMallOrder WITH(NOLOCK)
                            WHERE CompanySeq   = @CompanySeq
                              AND OrderNo      = A.OrderNo     
                              AND MallOrderNo  = A.MallOrderNo 
                              AND MallOrderSeq = A.MallOrderSeq
                          )
                              
        -- 진행여부확인
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               5                  , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                               @LanguageSeq       ,
                               23642, N'수주'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 23642
                               23963, N'주문'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                             AND B.OrderNo      = A.OrderNo     
                                                             AND B.MallOrderNo  = A.MallOrderNo 
                                                             AND B.MallOrderSeq = A.MallOrderSeq
         WHERE A.Status = 0
           AND B.OrderSeq <> 0

        -- 품목 필수 입력        
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               1286               , -- 필수항목인 @1에 @2이(가) 입력되지 않았습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1286)
                               @LanguageSeq       ,
                               2090, N'품명'      , -- SELECT * FROM _TCADictionary WHERE WordSeq = 2090
                               287 , N'정보'        -- SELECT * FROM _TCADictionary WHERE WordSeq = 287

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                             AND B.OrderNo      = A.OrderNo     
                                                             AND B.MallOrderNo  = A.MallOrderNo 
                                                             AND B.MallOrderSeq = A.MallOrderSeq
         WHERE A.Status  = 0
           AND B.ItemSeq = 0

        -- 쇼핑몰명 존재여부

        EXEC dbo._SWCOMMessage @MessageType OUTPUT        ,
                               @Status      OUTPUT        ,
                               @Results     OUTPUT        ,
                               1293                       , -- @1의 @2을(를) 확인하십시오. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1293)
                               @LanguageSeq               ,
                               10000141, N'쇼핑몰연동설정', -- SELECT * FROM _TCADictionary WHERE WordSeq = 10000141
                               10000098, N'쇼핑몰명'        -- SELECT * FROM _TCADictionary WHERE WordSeq = 10000098 

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                             AND B.OrderNo      = A.OrderNo     
                                                             AND B.MallOrderNo  = A.MallOrderNo 
                                                             AND B.MallOrderSeq = A.MallOrderSeq
         WHERE A.Status = 0
           AND NOT EXISTS (SELECT 1
                             FROM _TSLFPShoppingMall WITH(NOLOCK)
                            WHERE CompanySeq = @CompanySeq
                              AND MallName   = B.MallName
                          )

        -- 사용자 사원등록여부

        IF ISNULL((SELECT EmpSeq FROM _TCAUser WITH(NOLOCK) WHERE CompanySeq = @CompanySeq AND UserSeq = @UserSeq), 0) = 0
        BEGIN
        
            EXEC dbo._SWCOMMessage @MessageType OUTPUT ,
                                   @Status      OUTPUT ,
                                   @Results     OUTPUT ,
                                   1293                ,  -- @1의 @2을(를) 확인하십시오. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1293)
                                   @LanguageSeq        ,
                                   25779, N'사용자등록', -- SELECT * FROM _TCADictionary WHERE WordSeq = 25779
                                   14606, N'사원정보'    -- SELECT * FROM _TCADictionary WHERE WordSeq = 14606

            UPDATE #BIZ_OUT_DataBlock2
               SET Result      = @Results    ,
                   MessageType = @MessageType,
                   Status      = @Status
              FROM #BIZ_OUT_DataBlock2
             WHERE Status = 0


        END
        
        -- 쇼핑몰 연동항목 수주구분
        IF ISNULL((SELECT UMOrderKind FROM _TSLFPShoppingMallEnv WITH(NOLOCK) WHERE CompanySeq = @CompanySeq), 0) = 0
        BEGIN

            EXEC dbo._SWCOMMessage @MessageType OUTPUT ,
                                   @Status      OUTPUT ,
                                   @Results     OUTPUT ,
                                   1293                , -- @1의 @2을(를) 확인하십시오. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1293)
                                   @LanguageSeq        ,
                                   68075, N'기본설정값', -- SELECT * FROM _TCADictionary WHERE WordSeq = 68075
                                   630, N'수주구분'      -- SELECT * FROM _TCADictionary WHERE WordSeq = 630

            UPDATE #BIZ_OUT_DataBlock2
               SET Result      = @Results    ,
                   MessageType = @MessageType,
                   Status      = @Status
              FROM #BIZ_OUT_DataBlock2
             WHERE Status = 0


        END

        -- 쇼핑몰 연동항목 위탁처리구분
        IF ISNULL((SELECT InOutDetailKind FROM _TSLFPShoppingMallEnv WITH(NOLOCK) WHERE CompanySeq = @CompanySeq), 0) = 0
        BEGIN

            EXEC dbo._SWCOMMessage @MessageType OUTPUT   ,
                                   @Status      OUTPUT   ,
                                   @Results     OUTPUT   ,
                                   1293                  , -- @1의 @2을(를) 확인하십시오. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1293)
                                   @LanguageSeq          ,
                                   68075, N'기본설정값'  , -- SELECT * FROM _TCADictionary WHERE WordSeq = 68075
                                   16372, N'위탁처리구분'  -- SELECT * FROM _TCADictionary WHERE WordSeq = 16372

            UPDATE #BIZ_OUT_DataBlock2
               SET Result      = @Results    ,
                   MessageType = @MessageType,
                   Status      = @Status
              FROM #BIZ_OUT_DataBlock2
             WHERE Status = 0


        END

        -- 쇼핑몰 창고 사업단위 확인

        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               3054               , -- @1과(와) @2의 @3가(이) 다릅니다. @4을(를) 확인하세요. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 3054)
                               @LanguageSeq       ,                               
                               584, N'입고창고'   , -- SELECT * FROM _TCADictionary WHERE WordSeq = 62558
                               626, N'출고창고'   , -- SELECT * FROM _TCADictionary WHERE WordSeq = 30437
                               49926, N'사업단위' , -- SELECT * FROM _TCADictionary WHERE WordSeq = 49926
                               6575, N'설정내용'    -- SELECT * FROM _TCADictionary WHERE WordSeq = 6575 

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                             AND B.OrderNo      = A.OrderNo     
                                                             AND B.MallOrderNo  = A.MallOrderNo 
                                                             AND B.MallOrderSeq = A.MallOrderSeq
               JOIN _TSLFPShoppingMall      AS C WITH(NOLOCK) ON C.CompanySeq   = B.CompanySeq
                                                             AND C.MallName     = B.MallName
               JOIN _TDAWH                  AS D WITH(NOLOCK) ON D.CompanySeq   = C.CompanySeq
                                                             AND D.WHSeq        = C.ERPWHSeq
               JOIN _TDAWH                  AS E WITH(NOLOCK) ON E.CompanySeq   = C.CompanySeq
                                                             AND E.WHSeq        = C.WHSeq              
         WHERE A.Status  = 0
           AND D.BizUnit <> E.BizUnit

        -- 쇼핑몰의 ERP거래처가 판매거래처가 아닌 경우 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT , 
                               @Status      OUTPUT , 
                               @Results     OUTPUT , 
                               1261                , -- @1이(가) 아닙니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1261)
                               @LanguageSeq        ,
                               18593, N'판매거래처'  -- SELECT * FROM _TCADictionary WHERE WordSeq = 18593

            UPDATE #BIZ_OUT_DataBlock1                               
               SET Result      = @Results    ,    
                   MessageType = @MessageType,    
                   Status      = @Status    
              FROM #BIZ_OUT_DataBlock2     AS A
                   JOIN _TSLFPShoppingMall AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq 
                                                            AND B.MallId     = A.MallUserId 
                                                            AND B.MallName   = A.MallName 
         WHERE A.Status = 0
           AND NOT EXISTS (SELECT 1
                             FROM _TDACustKind              AS X WITH(NOLOCK)
                                  JOIN _TDACustKindCodeHelp AS Y WITH(NOLOCK) ON Y.CompanySeq = X.CompanySeq
                                                                             AND Y.UMCustKind = X.UMCustKind
                            WHERE X.CompanySeq = @CompanySeq
                              AND X.CustSeq    = B.CustSeq
                              AND Y.CustCodeHelpSeq = 1
                           ) -- 판매거래처

        IF @IsCustSalesMan = '1' 
        BEGIN
            
            CREATE TABLE #TSLCustSalesEmp (
                CustSeq INT     ,
                SDate   NCHAR(8),
                EDate   NCHAR(8),
                EmpSeq  INT     ,
                DeptSeq INT     
            )

            INSERT INTO #TSLCustSalesEmp (CustSeq, SDate, EDate, EmpSeq, DeptSeq)
            SELECT CustSeq, SDate, EDate, EmpSeq, DeptSeq
              FROM _TSLCustSalesEmp WITH(NOLOCK)
             WHERE CompanySeq = @CompanySeq
             UNION
            SELECT CustSeq, SDate, EDate, EmpSeq, DeptSeq
              FROM _TSLCustSalesEmpHist WITH(NOLOCK)
             WHERE CompanySeq = @CompanySeq
            
            EXEC dbo._SWCOMMessage @MessageType OUTPUT       , 
                                   @Status      OUTPUT       , 
                                   @Results     OUTPUT       , 
                                   1199                      , -- @1이(가) @2 되지 않았습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1199)
                                   @LanguageSeq              ,
                                   11706, N'거래처영업담당자', -- SELECT * FROM _TCADictionary WHERE WordSeq = 11706
                                   24260, N'설정'              -- SELECT * FROM _TCADictionary WHERE WordSeq = 24260

            UPDATE #BIZ_OUT_DataBlock2                               
               SET Result      = @Results    ,    
                   MessageType = @MessageType,    
                   Status      = @Status    
              FROM #BIZ_OUT_DataBlock2     AS A
                   JOIN _TSLFPShoppingMall AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq 
                                                            AND B.MallId     = A.MallUserId 
                                                            AND B.MallName   = A.MallName 
             WHERE NOT EXISTS (SELECT 1
                                 FROM #TSLCustSalesEmp
                                WHERE B.CustSeq = CustSeq
                                  AND A.OrderDate BETWEEN SDate AND EDate
                              )
         

        END

        IF @IsCfmUsed = '1' 
        BEGIN

            -- 진행여부확인
            EXEC dbo._SWCOMMessage @MessageType OUTPUT   ,
                                   @Status      OUTPUT   ,
                                   @Results     OUTPUT   ,
                                   5                     , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                                   @LanguageSeq          ,
                                   51176, N'수주확정'    , -- SELECT * FROM _TCADictionary WHERE WordSeq = 51176
                                   23963, N'주문'          -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

            UPDATE #BIZ_OUT_DataBlock2
               SET Result      = @Results    ,
                   MessageType = @MessageType,
                   Status      = @Status
              FROM #BIZ_OUT_DataBlock2          AS A
                   JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                                 AND B.OrderNo      = A.OrderNo     
                                                                 AND B.MallOrderNo  = A.MallOrderNo 
                                                                 AND B.MallOrderSeq = A.MallOrderSeq
                   JOIN _TSLOrder               AS C WITH(NOLOCK) ON C.CompanySeq   = B.CompanySeq 
                                                                 AND C.OrderSeq     = B.OrderSeq 
                   JOIN _TCOMConfirm            AS D WITH(NOLOCK) ON D.CompanySeq   = C.CompanySeq 
                                                                 AND D.ConfirmSeq   = 6303 
                                                                 AND D.CfmSeq       = C.OrderSeq 
             WHERE A.Status   = 0
               AND B.OrderSeq <> 0
               AND D.CfmCode  = 1


        END 

    END 

    ELSE IF @WorkingTag = 'C' -- 수주확정취소
    BEGIN
        

        -- 진행여부확인
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                               @Status      OUTPUT,  
                               @Results     OUTPUT,  
                               1121               , -- @1되지 않은 것은 @2할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1121)  
                               @LanguageSeq       ,   
                               23642, N'수주'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 23642
                               18311, N'취소'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 2529

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                             AND B.OrderNo      = A.OrderNo     
                                                             AND B.MallOrderNo  = A.MallOrderNo 
                                                             AND B.MallOrderSeq = A.MallOrderSeq
         WHERE A.Status   = 0
           AND B.OrderSeq = 0

        -- 위탁출고여부확인 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                                @Status      OUTPUT,  
                                @Results     OUTPUT,  
                                5                  , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                                @LanguageSeq       ,   
                                25266, N'위탁출고' , -- SELECT * FROM _TCADictionary WHERE WordSeq = 25266
                                18311, N'취소'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 2529

        UPDATE #BIZ_OUT_DataBlock2
            SET Result      = @Results    ,
                MessageType = @MessageType,
                Status      = @Status
            FROM #BIZ_OUT_DataBlock2         AS A
                JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                              AND B.OrderNo      = A.OrderNo     
                                                              AND B.MallOrderNo  = A.MallOrderNo 
                                                              AND B.MallOrderSeq = A.MallOrderSeq
                JOIN _TSLOrder               AS C WITH(NOLOCK) ON C.CompanySeq   = B.CompanySeq 
                                                              AND C.OrderSeq     = B.OrderSeq 
            WHERE A.Status = 0
              AND B.OrderSeq <> 0
              AND EXISTS (SELECT TOP 1 1 
                            FROM _TCOMSourceDailyTraceR WITH(NOLOCK)
                           WHERE CompanySeq   = C.CompanySeq 
                             AND FromSeq      = C.OrderSeq 
                             AND FromTableSeq = 19
                             AND ToTableSeq   = 14
                         )

        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               5                  , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                               @LanguageSeq       ,
                               25733, N'정산'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 25733
                               23963, N'주문'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2              AS A
               JOIN _TSLFPShoppingMallOrder     AS B WITH(NOLOCK) ON B.CompanySeq    = @CompanySeq
                                                                 AND B.OrderNo       = A.OrderNo     
                                                                 AND B.MallOrderNo   = A.MallOrderNo 
                                                                 AND B.MallOrderSeq  = A.MallOrderSeq
               JOIN _TSLShoppingMallSalesUpload AS C WITH(NOLOCK) ON C.CompanySeq    = B.CompanySeq
                                                                 AND C.MallName      = B.MallName
                                                                 AND C.MallOrderNo   = B.MallOrderNo
                                                                 AND C.MallOrderSeq  = B.MallOrderSeq
                                                                 AND C.MallOrderSerl = B.MallOrderSerl
         WHERE A.Status      = 0
           AND C.InvoiceSeq <> 0

        IF @IsCfmUsed = '1' 
        BEGIN

            -- 확정여부 확인 
            EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                                   @Status      OUTPUT,  
                                   @Results     OUTPUT,  
                                   1121               , -- @1되지 않은 것은 @2할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1121)  
                                   @LanguageSeq       ,   
                                   51176, N'수주확정' , -- SELECT * FROM _TCADictionary WHERE WordSeq = 51176
                                   18311, N'취소'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 2529

            UPDATE #BIZ_OUT_DataBlock2
               SET Result      = @Results    ,
                   MessageType = @MessageType,
                   Status      = @Status
              FROM #BIZ_OUT_DataBlock2          AS A
                   JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq   = @CompanySeq
                                                                 AND B.OrderNo      = A.OrderNo     
                                                                 AND B.MallOrderNo  = A.MallOrderNo 
                                                                 AND B.MallOrderSeq = A.MallOrderSeq
                   JOIN _TSLOrder               AS C WITH(NOLOCK) ON C.CompanySeq   = B.CompanySeq 
                                                                 AND C.OrderSeq     = B.OrderSeq 
                   JOIN _TCOMConfirm            AS D WITH(NOLOCK) ON D.CompanySeq   = C.CompanySeq 
                                                                 AND D.ConfirmSeq   = 6303 
                                                                 AND D.CfmSeq       = C.OrderSeq 
             WHERE A.Status   = 0               
               AND B.OrderSeq <> 0
               AND D.CfmCode  <> 1

        END
          


    END

RETURN
