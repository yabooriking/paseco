IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderCheck
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 체크
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallOrderCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS  
  
    DECLARE @Count       INT          , --채번시사용
            @Seq         INT          , --채번시사용
            @MessageType INT          , --중복,필수값체크시사용
            @Status      INT          , --중복,필수값체크시사용
            @Results     NVARCHAR(250), --중복,필수값체크시사용
            @MaxSeq      INT          , --채번시사용
            @Date        NCHAR(8)     , --외부키딸때사용
            @MaxNo       NVARCHAR(200)

    ---------------------------
    -- 필수입력체크
    ---------------------------
    DECLARE @WordSeq INT
    
    SELECT @WordSeq = CASE WHEN ISNULL(ItemSeq,0) = 0 THEN 30951
                                                      ELSE 0 END
      FROM #BIZ_OUT_DataBlock2 
     WHERE ISNULL(ItemSeq,0) = 0 

    -- 필수입력Message 받아오기
    EXEC dbo._SWCOMMessage 
            @MessageType OUTPUT,
            @Status      OUTPUT,
            @Results     OUTPUT,
            1004               , -- @1을(를) 먼저 선택해 주십시오 (SELECT * FROM _TCAMessageLanguage WHERE Message like '%선택%')
            @LanguageSeq       , 
            @WordSeq,''          -- SELECT * FROM _TCADictionary WHERE Word like '%ERp%품목%'

     UPDATE #BIZ_OUT_DataBlock2
        SET Result      = @Results    ,
            MessageType = @MessageType,
            Status      = @Status
       FROM #BIZ_OUT_DataBlock2 AS A
      WHERE A.WorkingTag IN ('A','U')
        AND A.Status     = 0
        AND @WordSeq     <> 0

        -- 진행여부확인
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                               @Status      OUTPUT,
                               @Results     OUTPUT,
                               5                  , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                               @LanguageSeq       ,
                               23642, N'수주'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 36241
                               23963, N'주문'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

     UPDATE #BIZ_OUT_DataBlock2
        SET Result      = @Results    ,
            MessageType = @MessageType,
            Status      = @Status
       FROM #BIZ_OUT_DataBlock2          AS A
            JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq    = @CompanySeq
                                                          AND B.OrderNo       = A.OrderNo     
                                                          AND B.MallOrderNo   = A.MallOrderNo 
                                                          AND B.MallOrderSeq  = A.MallOrderSeq
                                                          AND B.MallOrderSerl = A.MallOrderSerl     
                                                          AND B.MallName      = A.MallName 
                                                          AND B.MallUserID    = A.MallUserID
      WHERE A.Status = 0
        AND ISNULL(B.OrderSeq ,0) <> 0

    -- 정산여부확인

    EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                           @Status      OUTPUT,
                           @Results     OUTPUT,
                           5                  , -- 이미 @1이(가) 완료된 @2입니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 5)
                           @LanguageSeq       ,
                           25733, N'정산'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 25733
                           23963, N'주문'       -- SELECT * FROM _TCADictionary WHERE WordSeq = 23963

    UPDATE #BIZ_OUT_DataBlock2
       SET Result      = @Results    ,
           MessageType = @MessageType,
           Status      = @Status
      FROM #BIZ_OUT_DataBlock2              AS A
           JOIN _TSLFPShoppingMallOrder     AS B WITH(NOLOCK) ON B.CompanySeq    = @CompanySeq
                                                             AND B.OrderNo       = A.OrderNo     
                                                             AND B.MallOrderNo   = A.MallOrderNo 
                                                             AND B.MallOrderSeq  = A.MallOrderSeq
                                                             AND B.MallOrderSerl = A.MallOrderSerl     
                                                             AND B.MallName      = A.MallName 
                                                             AND B.MallUserID    = A.MallUserID
           JOIN _TSLShoppingMallSalesUpload AS C WITH(NOLOCK) ON C.CompanySeq    = B.CompanySeq
                                                             AND C.MallName      = B.MallName
                                                             AND C.MallOrderNo   = B.MallOrderNo
                                                             AND C.MallOrderSeq  = B.MallOrderSeq
                                                             AND C.MallOrderSerl = B.MallOrderSerl
     WHERE A.Status = 0
       AND ISNULL(C.InvoiceSeq, 0) <> 0

RETURN
