CREATE TABLE _TSLFPShoppingMallEnv (
    CompanySeq		INT 	     , 
    CompanyID		NVARCHAR(200), 
    AuthKey		    NVARCHAR(400), 
    ShopItemColSeq	INT 	     , 
    ERPItemColSeq	INT 	     , 
    InOutDetailKind	INT 	     , 
    UMOrderKind		INT 	     , 
    LastUserSeq		INT 	     , 
    LastDateTime	DATETIME     
)
CREATE CLUSTERED INDEX IDX_TSLFPShoppingMallEnv ON _TSLFPShoppingMallEnv (CompanySeq, CompanyID)
IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 44320002)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLFPShoppingMallEnv', 1, 44320002, '1', 1, GETDATE()END