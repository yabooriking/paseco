CREATE TABLE _TSLFPShoppingMallOrder (
    CompanySeq	  INT 	             , 
    OrderNo		  NVARCHAR(50) 	     , 
    MallOrderNo	  NVARCHAR(50) 	     , 
    MallOrderSeq  NVARCHAR(50) 	     , 
    MallOrderSerl NVARCHAR(50) 	     , 
    MallName	  NVARCHAR(100)      , 
    MallUserID	  NVARCHAR(50) 	     , 
    OrderDate	  NCHAR(8) 	         , 
    OrderStatus	  NVARCHAR(50) 	 NULL, 
    CfmItem		  NVARCHAR(200)  NULL, 
    CfmOption	  NVARCHAR(200)  NULL, 
    ItemSeq		  INT 	         NULL, 
    CompanyItem	  NVARCHAR(50) 	 NULL, 
    MallItemID	  NVARCHAR(50) 	 NULL, 
    ItemID		  NVARCHAR(50) 	 NULL, 
    SingleItemID  NVARCHAR(50) 	 NULL, 
    ModelNo		  NVARCHAR(200)  NULL, 
    OptionName	  NVARCHAR(200)  NULL, 
    Qty		      DECIMAL(19,0)  NULL, 
    OrderAmt	  DECIMAL(19,0)  NULL, 
    PayAmt		  DECIMAL(19,0)  NULL, 
    OrdUserID	  NVARCHAR(50) 	 NULL, 
    OrdUserName	  NVARCHAR(50) 	 NULL, 
    OrdTelNo	  NVARCHAR(400)  NULL, 
    OrdPhone	  NVARCHAR(400)  NULL, 
    OrdEmail	  NVARCHAR(200)  NULL, 
    RecvTelNo	  NVARCHAR(400)  NULL, 
    RecvPhone	  NVARCHAR(400)  NULL, 
    RecvEmail	  NVARCHAR(200)  NULL, 
    RecvUserName  NVARCHAR(50) 	 NULL, 
    RecvZipCode	  NVARCHAR(50) 	 NULL, 
    RecvAddr	  NVARCHAR(200)  NULL, 
    TransCustName NVARCHAR(200)  NULL, 
    TransNo		  NVARCHAR(200)  NULL, 
    TranMSG		  NVARCHAR(1000) NULL, 
    OrderSeq	  INT 	         NULL, 
    OrderSerl	  INT 	         NULL, 
    LastUserSeq	  INT 	             , 
    LastDateTime  DATETIME 	 )

CREATE CLUSTERED INDEX IDX_TSLFPShoppingMallOrder ON _TSLFPShoppingMallOrder (CompanySeq, OrderNo, MallOrderNo, MallOrderSeq, MallOrderSerl, MallName, MallUserID)
-- 로그가 많이 남는다고 하여 별도의 로그테이블을 생성하여 관리함.. 