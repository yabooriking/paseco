IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderAPIQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderAPIQuery
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 주문내역수집API호출
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallOrderAPIQuery  
    @ServiceSeq     INT     = 0,
    @WorkingTag     NVARCHAR(10)= '',
    @CompanySeq     INT     = 1,
    @LanguageSeq    INT     = 1,
    @UserSeq        INT     = 0,
    @PgmSeq         INT     = 0,
    @IsTransaction  BIT     = 0   
AS    

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
    EXEC _SYMMETRICKeyOpen

    DECLARE @UserId NVARCHAR(100),
            @UserName NVARCHAR(100)

    SELECT @UserId   = ISNULL(UserId, '')  ,
           @UserName = ISNULL(UserName, '')
      FROM _TCAUser WITH(NOLOCK)
    WHERE CompanySeq = @CompanySeq
      AND UserSeq    = @UserSeq
                                               
    SELECT B.CompanyID                        ,
           A.Credential                       ,
           @UserId   AS UserId                ,
           @UserName AS UserName              ,
           A.CompanySeq                       ,
           1 AS ServicePartnerSeq             ,
           'LogisticService' AS  AccessService,
           'CustomerUser' AS Role             ,
           A.APIKey                           ,
           dbo._FWCOMDecrypt(B.AuthKey, '_TSLShoppingMallEnv', 'AuthKey', @CompanySeq) AS AuthKey
      FROM _TCACredentials AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TSLFPShoppingMallEnv AS B WITH(NOLOCK)ON A.CompanySeq = B.CompanySeq
     WHERE @CompanySeq = A.CompanySeq
      
    SELECT  StartDate ,
           EndDate    ,
           MallOrderNo,
           MallID     ,
           OrderStatus 
      FROM #BIZ_IN_DataBlock2
      
RETURN