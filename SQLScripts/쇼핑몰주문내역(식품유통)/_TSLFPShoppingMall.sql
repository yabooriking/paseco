CREATE TABLE _TSLFPShoppingMall (
    CompanySeq	 INT 	      , 
    MallSeq		 INT 	      , 
    MallName	 NVARCHAR(200), 
    MallID		 NVARCHAR(200), 
    CustSeq		 INT 	      , 
    WHSeq		 INT 	      , 
    ERPWHSeq	 INT 	      , 
    LastUserSeq	 INT 	      , 
    LastDateTime DATETIME 	  )

CREATE CLUSTERED INDEX IDX_TSLFPShoppingMall ON _TSLFPShoppingMall (CompanySeq, MallSeq)
IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 44320001)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLFPShoppingMall', 1, 44320001, '1', 1, GETDATE()END