IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderSave
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 저장
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallOrderSave
    @ServiceSeq     INT     = 0,
    @WorkingTag     NVARCHAR(10)= '',
    @CompanySeq     INT     = 1,
    @LanguageSeq    INT     = 1,
    @UserSeq        INT     = 0,
    @PgmSeq         INT     = 0,
    @IsTransaction  BIT     = 0   
AS    

    -- 로그테이블남기기(마지막파라메터는반드시한줄로보내기)
    -- 데이터 ROW 가 많을 예정이라 _TCOMTableLog 에 담을 타 모듈의 로그기록까지 조회 할 수 없는 상태가 될까봐 Log테이블 별도 구성
    INSERT INTO _TSLFPShoppingMallOrderLog
    (
        LogUserSeq      ,LogDateTime     ,LogType         ,LogPgmSeq       ,
        CompanySeq      ,MallOrderNo     ,MallOrderSeq    ,MallOrderSerl   ,OrderDate       ,
        OrderNo         ,MallName        ,MallUserID      ,OrderStatus     ,ItemSeq         ,
        CompanyItem     ,MallItemID      ,ItemID          ,SingleItemID    ,ModelNo         ,
        OptionName      ,Qty             ,OrderAmt        ,PayAmt          ,OrdUserID       ,
        OrdUserName     ,OrdTelNo        ,OrdPhone        ,OrdEmail        ,RecvTelNo       ,
        RecvPhone       ,RecvEmail       ,RecvUserName    ,RecvZipCode     ,RecvAddr        ,
        TransCustName   ,TransNo         ,TranMSG         ,OrderSeq        ,OrderSerl       ,
        LastUserSeq     ,LastDateTime
    )
    SELECT @UserSeq          , GETDATE()        , 'U'              , @PgmSeq, 
           A.CompanySeq      ,A.MallOrderNo     ,A.MallOrderSeq    ,A.MallOrderSerl   ,A.OrderDate       ,
           A.OrderNo         ,A.MallName        ,A.MallUserID      ,A.OrderStatus     ,A.ItemSeq         ,
           A.CompanyItem     ,A.MallItemID      ,A.ItemID          ,A.SingleItemID    ,A.ModelNo         ,
           A.OptionName      ,A.Qty             ,A.OrderAmt        ,A.PayAmt          ,A.OrdUserID       ,
           A.OrdUserName     ,A.OrdTelNo        ,A.OrdPhone        ,A.OrdEmail        ,A.RecvTelNo       ,
           A.RecvPhone       ,A.RecvEmail       ,A.RecvUserName    ,A.RecvZipCode     ,A.RecvAddr        ,
           A.TransCustName   ,A.TransNo         ,A.TranMSG         ,A.OrderSeq        ,A.OrderSerl       ,
           A.LastUserSeq     ,A.LastDateTime
      FROM  _TSLFPShoppingMallOrder  AS A WITH(NOLOCK)
            JOIN #BIZ_OUT_DataBlock2 AS B ON A.OrderNo       = B.OrderNo            
                                         AND A.MallOrderNo   = B.MallOrderNo        
                                         AND A.MallOrderSeq  = B.MallOrderSeq       
                                         AND A.MallOrderSerl = B.MallOrderSerl
     WHERE B.Status = 0
       AND B.WorkingTag = 'U'

    IF @@ERROR <> 0  RETURN
       
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'U' AND Status = 0)
    BEGIN

        UPDATE _TSLFPShoppingMallOrder
           SET ItemSeq      = B.ItemSeq     ,
               OrdUserID    = B.OrdUserID   ,
               OrdUserName  = B.OrdUserName ,
               OrdTelNo     = B.OrdTelNo    ,
               OrdPhone     = B.OrdPhone    ,
               OrdEmail     = B.OrdEmail    ,
               RecvUserName = B.RecvUserName,
               RecvZipCode  = B.RecvZipCode ,
               RecvAddr     = B.RecvAddr    ,
               RecvTelNo    = B.RecvTelNo   ,
               RecvPhone    = B.RecvPhone   ,
               RecvEmail    = B.RecvEmail   ,
               TransNo      = B.TransNo     ,
               TranMSG      = B.TranMSG     ,
               LastUserSeq  = @UserSeq      ,
               LastDateTime = GETDATE()
          FROM _TSLFPShoppingMallOrder  AS A WITH(NOLOCK)
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.OrderNo       = A.OrderNo            
                                            AND B.MallOrderNo   = A.MallOrderNo        
                                            AND B.MallOrderSeq  = A.MallOrderSeq       
                                            AND B.MallOrderSerl = A.MallOrderSerl
         WHERE A.CompanySeq = @CompanySeq
           AND B.WorkingTag = 'U'
           AND B.Status     = 0

        IF @@ERROR <> 0  RETURN

    END
    
RETURN