IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderBatchSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderBatchSave
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 수주생성저장
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROCEDURE _SWFPSLShoppingMallOrderBatchSave 
    @ServiceSeq    INT          = 0 ,  
    @WorkingTag    NVARCHAR(10) = '',  
    @CompanySeq    INT          = 1 ,  
    @LanguageSeq   INT          = 1 ,  
    @UserSeq       INT          = 0 ,  
    @PgmSeq        INT          = 0 ,  
    @IsTransaction BIT          = 0   
     
AS    

    DECLARE @MessageType        INT          ,
            @Status             INT          ,
            @Results            NVARCHAR(250),
            @Count              INT          ,
            @Date               NCHAR(8)     ,
            @Seq                INT          ,
            @Cnt            INT          ,
            @EmpSeq             INT          ,
            @DeptSeq            INT          ,
            @UMOrderKind        INT          ,
            @IsCfmUsed          NCHAR(1)     ,
            @IsCustSalesMan     NCHAR(1)     ,
            @CurrSeq            INT          ,
            @GoodQtyDecLength   INT          ,
            @GoodPriceDecLength INT          , 
            @CurAmtDecLength    INT          , 
            @DomAmtDecLength    INT          , 
            @No                 NVARCHAR(30) 

    -- 환경설정 수집
    EXEC dbo._SWCOMEnv @CompanySeq, 8   , @UserSeq, @@PROCID, @GoodQtyDecLength   OUTPUT -- 판매/제품 소수점자리수
    EXEC dbo._SWCOMEnv @CompanySeq, 9   , @UserSeq, @@PROCID, @GoodPriceDecLength OUTPUT -- 판매/제품 원화단가 소수점 자릿수
    EXEC dbo._SWCOMEnv @CompanySeq, 13  , @UserSeq, @@PROCID, @CurrSeq            OUTPUT -- 자국 통화        
    EXEC dbo._SWCOMEnv @CompanySeq, 14  , @UserSeq, @@PROCID, @CurAmtDecLength    OUTPUT -- 거래금액 소수점 자릿수
    EXEC dbo._SWCOMEnv @CompanySeq, 15  , @UserSeq, @@PROCID, @DomAmtDecLength    OUTPUT -- 자국통화금액 소수점 자릿수
    EXEC dbo._SWCOMEnv @CompanySeq, 8001, @UserSeq, @@PROCID, @IsCustSalesMan     OUTPUT -- 영업담당 사용여부
    
    SELECT TOP 1 @EmpSeq  = EmpSeq ,
                 @DeptSeq = DeptSeq 
      FROM #BIZ_OUT_DataBlock2 WITH(NOLOCK)
     WHERE Status = 0 

    -- [쇼핑몰 연동설정] 화면의 수주구분 
    SELECT @UMOrderKind = UMOrderKind 
      FROM _TSLFPShoppingMallEnv
     WHERE CompanySeq = @CompanySeq

    -- 수주확정 사용여부 
    SELECT @IsCfmUsed = CASE WHEN IsNotUsed = '1' THEN '0'
                                                  ELSE '1' END 
      FROM _TCOMConfirmDef WITH(NOLOCK)
     WHERE CompanySeq = @CompanySeq 
       AND ConfirmSeq = 6303 

    -- 거래처별 영업담당자 
    CREATE TABLE #TSLCustSalesEmp (
        CustSeq INT     ,
        SDate   NCHAR(8),
        EDate   NCHAR(8),
        EmpSeq  INT     ,
        DeptSeq INT     
    )

    -- 가져온 쇼핑몰 주문내역을 주문일, 쇼핑몰, 품목별 집계하여 수주 데이터 생성 
    CREATE TABLE #OrderData (
        DataSeq   INT          ,
        MallSeq   INT          ,
        OrderSeq  INT          ,
        OrderNo   NVARCHAR(20) ,
        OrderDate NCHAR(8)     ,
        ItemSeq   INT          , 
        Qty       DECIMAL(19,5),
        Amt       DECIMAL(19,5),
        Price     DECIMAL(19,5)     
    )

    CREATE TABLE #TSLOrder (
        DataSeq    INT           ,
        TmpDataSeq INT           , -- #BIZ_UT_DataBlock2 템프테이블의 DataSeq, GROUP BY 할 경우 가장 큰 값을 저장 
        OrderSeq   INT           ,
        OrderNo    NVARCHAR(20)  ,
        OrderDate  NCHAR(8)      ,
        MallSeq    INT           ,
        CustSeq    INT           , 
        ItemSeq    INT           , 
        Qty        DECIMAL(19,5) ,
        Amt        DECIMAL(19,5) ,
        Price      DECIMAL(19,5) ,
        Remark     NVARCHAR(1000)    
    ) 

    CREATE CLUSTERED INDEX IDX_TSLOrder ON #TSLOrder (OrderSeq)
        
    CREATE TABLE #TSLOrderItem (
        TmpDataSeq    INT           , -- #BIZ_OUT_DataBlock2 템프테이블의 DataSeq, GROUP BY 할 경우 가장 큰 값을 저장 
        OrderSeq      INT           ,
        OrderSerl     INT           ,
        ItemSeq       INT           ,
        UnitSeq       INT           ,
        Price         DECIMAL(19, 5),
        Qty           DECIMAL(19, 5),
        IsInclusedVAT NCHAR(1)      ,
        DomAmt        DECIMAL(19, 5),
        DomVAT        DECIMAL(19, 5),
        OrderDate     NCHAR(8)      ,
        VATRate       DECIMAL(19,5)
    )

    CREATE TABLE #TSLOrderCancel (
        WorkingTag    NCHAR(1),
        Status        INT     ,
        OrderSeq      INT     ,
        OrderSerl     INT     ,
        Qty           DECIMAL(19,5)
    )
            
    CREATE TABLE #SSLOrderSeq (SumSeq INT)
   
    -- 확정
    IF @WorkingTag = 'P'
    BEGIN

        IF ISNULL(@IsCustSalesMan, '') = '1'
        BEGIN
        
            INSERT INTO #TSLCustSalesEmp (CustSeq, SDate, EDate, EmpSeq, DeptSeq)
            SELECT CustSeq, SDate, EDate, EmpSeq, DeptSeq
              FROM _TSLCustSalesEmp WITH(NOLOCK)
             WHERE CompanySeq = @CompanySeq
             UNION
            SELECT CustSeq, SDate, EDate, EmpSeq, DeptSeq
              FROM _TSLCustSalesEmpHist WITH(NOLOCK)
             WHERE CompanySeq = @CompanySeq


        END

        -- GROUP BY 할 데이터를 템프테이블에 INSERT 
        INSERT INTO #OrderData (DataSeq, MallSeq, OrderDate, ItemSeq, Qty, Amt, Price)
        SELECT  A.DataSeq, 
                C.MallSeq  ,
                B.OrderDate, 
                B.ItemSeq  ,
                ISNULL(B.Qty,0)      ,
                ISNULL(B.OrderAmt, 0) ,
                ROUND(ISNULL(B.OrderAmt, 0) / ISNULL(B.Qty, 0), @GoodPriceDecLength) 
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B WITH(NOLOCK) ON B.CompanySeq    = @CompanySeq
                                                             AND B.OrderNo       = A.OrderNo     
                                                             AND B.MallOrderNo   = A.MallOrderNo 
                                                             AND B.MallOrderSeq  = A.MallOrderSeq
                                                             AND B.MallOrderSerl = A.MallOrderSerl
                                                             AND B.MallName      = A.MallName
                                                             AND B.MallUserId    = A.MallUserID
               JOIN _TSLFPShoppingMall      AS C WITH(NOLOCK) ON C.CompanySeq    = B.CompanySeq
                                                             AND C.MallName      = B.MallName
                                                             AND C.MallID        = B.MallUserID
               LEFT OUTER JOIN _TDAItem     AS D WITH(NOLOCK) ON D.CompanySeq    = B.CompanySeq 
                                                             AND D.ItemSeq       = B.ItemSeq 
         WHERE A.Status = 0

         -- 주문일(수주일), 쇼핑몰, 품목, 단가 별로 GROUP BY
         INSERT INTO #TSLOrder (DataSeq, TmpDataSeq, OrderNo, OrderSeq, OrderDate, MallSeq, CustSeq, ItemSeq, Qty, Amt, Price, Remark) 
         SELECT DENSE_RANK() OVER (ORDER BY A.OrderDate, A.MallSeq) ,
                MAX(A.DataSeq)       ,
                ''                   ,
                0                    ,
                A.OrderDate          ,
                A.MallSeq            ,
                B.CustSeq            ,
                A.ItemSeq            ,
                SUM(ISNULL(A.Qty, 0)),
                SUM(ISNULL(A.Amt, 0)),
                Price,
                ''
          FROM #OrderData               AS A
               JOIN _TSLFPShoppingMall  AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                         AND B.MallSeq    = A.MallSeq 
          GROUP BY A.OrderDate, A.MallSeq, B.CustSeq, A.ItemSeq, A.Price

        -- 수주 Master 테이블 OrderSeq, OrderNo 채번 
        SELECT @Count = COUNT(1) FROM #TSLOrder GROUP BY DataSeq

        IF @Count > 0
        BEGIN
                       
            EXEC @Seq = _SWCOMCreateSeq @CompanySeq, '_TSLOrder', 'OrderSeq', @Count

            UPDATE #TSLOrder SET OrderSeq = @Seq + DataSeq

            SELECT @Cnt = 1
        
            WHILE 1=1
            BEGIN

                SELECT @Date = OrderDate
                  FROM #TSLOrder
                 WHERE DataSeq = @Cnt

                IF @@ROWCOUNT = 0 BREAK

                EXEC _SWCOMCreateNo 'SL', '_TSLOrder', @CompanySeq, '', @Date, @No OUTPUT

                UPDATE #TSLOrder 
                   SET OrderNo = @No 
                 WHERE DataSeq = @Cnt

                SELECT @Cnt = @Cnt + 1

            END
            
        END
 
        -- 수주 품목 테이블 OrderSerl 채번 
        INSERT INTO #TSLOrderItem (OrderSeq     , TmpDataSeq   , ItemSeq      , UnitSeq      , Price        , 
                                   Qty          , IsInclusedVAT, OrderDate    , DomAmt       , DomVAT       ,
                                   OrderSerl    
                                  )
        SELECT A.OrderSeq     , MAX(A.TmpDataSeq)   , A.ItemSeq   , B.UnitSeq   , A.Price   , 
               SUM(ISNULL(A.Qty, 0))     , '1'      , A.OrderDate , 0           , 0         ,
               ROW_NUMBER() OVER (PARTITION BY A.OrderSeq ORDER BY A.ItemSeq, B.UnitSeq, A.Price) -- OrderSerl         
          FROM #TSLOrder                AS A
               LEFT OUTER JOIN _TDAItem AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq 
                                                         AND B.ItemSeq    = A.ItemSeq 
         GROUP BY A.OrderSeq, A.ItemSeq, B.UnitSeq, A.Price, /*B.IsInclusedVAT,*/ A.OrderDate

        UPDATE #TSLOrderItem
           SET VATRate = CASE ISNULL(B.SMVatKind, 0) WHEN 2003001 THEN ISNULL(C.VATRate, 10) ELSE 0 END
          FROM #TSLOrderItem               AS A
               JOIN _TDAItemSales          AS B ON B.CompanySeq = @CompanySeq
                                               AND B.ItemSeq    = A.ItemSeq  
               LEFT OUTER JOIN _TDAVATRate AS C ON C.CompanySeq = B.CompanySeq      
                                               AND C.SMVatType  = B.SMVatType
                                               AND C.SDate     <= A.OrderDate
                                               AND C.EDate     >= A.OrderDate                                                          

        -- 수주 Master INSERT 
        INSERT INTO _TSLOrder (CompanySeq      , OrderSeq        , BizUnit         , SMExpKind       , OrderNo         , -- 01
                               OrderDate       , UMOrderKind     , CustSeq         , BKCustSeq       , ContractDate    , -- 02
                               DVDate          , PONo            , CurrSeq         , ExRate          , IsOverCredit    , -- 03
                               IsMinAmt        , IsPreReceipt    , PaymentCondition, DVCondition     , CustDelvWay     , -- 04
                               IsStockSales    , IsImmediateDelv , IsDirectDelv    , Remark          , Memo            , -- 05
                               OrderRev        , LastUserSeq     , LastDateTime    , IsStop          , OrderRevEmpSeq  , -- 06
                               OrderRevDate    , StopEmpSeq      , StopDate        , SMConsignKind   , IsPJT           , -- 07
                               SMTransKind     , FileSeq         , MDummy1         , MDummy2         , MDummy3         , -- 08
                               MDummy4         , MDummy5         , MDummy6         , MDummy7         , MDummy8         , -- 09
                               MDummy9         , MDummy10        , UMDVConditionSeq, AGCustSeq       , PgmSeq          , -- 10
                               UMPriceTerms    , Shipper         , Consignee       , Notify          , PODate          , -- 11
                               DVPlace         ,                                                                         -- 12
                               DeptSeq         ,                                                                         -- 13
                               EmpSeq          )                                           -- 14
                                                                                           
        SELECT @CompanySeq , A.OrderSeq  , /*@BizUnit*/1    , 8009001     , A.OrderNo   ,  -- 01
               A.OrderDate , @UMOrderKind, A.CustSeq   , 0           , ''          ,       -- 02
               ''          , ''          , @CurrSeq    , 1           , '0'         ,       -- 03
               '0'         , '0'         , ''          , ''          , ''          ,       -- 04
               '0'         , '0'         , '0'         , ''    , ''          ,             -- 05
               0           , @UserSeq    , GETDATE()   , '0'         , 0           ,       -- 06
               ''          , 0           , ''          , 8060002     , '0'         ,       -- 07  20200525 김재호 : 위탁구분 기본값 = 위탁 
               0           , 0           , ''          , ''          , ''          ,       -- 08
               ''          , ''          , 0           , 0           , 0           ,       -- 09
               0           , 0           , 0           , 0           , @PgmSeq     ,       -- 10
               0           , ''          , ''          , ''          , ''          ,       -- 11
               ''          ,                                                               -- 12
               CASE ISNULL(@IsCustSalesMan, '') WHEN '1' THEN B.DeptSeq ELSE @DeptSeq END, -- 13
               CASE ISNULL(@IsCustSalesMan, '') WHEN '1' THEN B.EmpSeq  ELSE @EmpSeq  END  -- 14 
          FROM #TSLOrder                        AS A
               LEFT OUTER JOIN #TSLCustSalesEmp AS B WITH(NOLOCK) ON B.CustSeq   = A.CustSeq
                                                                 AND A.OrderDate BETWEEN B.SDate AND B.EDate
         GROUP BY A.OrderSeq, A.OrderNo, A.OrderDate, A.CustSeq, B.DeptSeq, B.EmpSeq
         
         IF @@ERROR <> 0 RETURN

         -- 수주품목 INSERT, 
         -- 부가세 사용여부(IsInclusedVAT = '1'로 하드 코딩)
        INSERT INTO _TSLOrderItem (CompanySeq          , OrderSeq            , OrderSerl           , OrderSubSerl        , ItemSeq             , -- 01
                                   UnitSeq             , ItemPrice           , CustPrice           , Qty                 , IsInclusedVAT       , -- 02
                                                                                                                           DVDate              , -- 03
                                   DVTime              , STDUnitSeq          , WHSeq               , DVPlaceSeq          , Remark              , -- 04
                                   ModelSeq            , IsStop              , StopEmpSeq          , StopDate            , OptionSeq           , -- 05
                                   UMEtcOutKind        , LastUserSeq         , LastDateTime        , IsGift              , CCtrSeq             , -- 06
                                   PJTSeq              , WBSSeq              , BKCustSeq           , RetOrderSeq         , RetOrderSerl        , -- 07
                                   LotNo               , ProgFromSeq         , ProgFromSerl        , ProgFromSubSerl     , ProgFromTableSeq    , -- 08
                                   SerialNo            , Price               , PgmSeq              , OriOrderSerl        , ProgFromQty         , -- 09
                                   ProgFromSTDQty      , ProgFromAmt         , ProgFromVAT         , ProgFromDOMAmt      , ProgFromDOMVAT      , -- 10
                                   ProgPrevFromTableSeq, Dummy1              , Dummy2              , Dummy3              , Dummy4              , -- 11
                                   Dummy5              , Dummy6              , Dummy7              , Dummy8              , Dummy9              , -- 12
                                   Dummy10             , SMPriceType         ,                                                                   -- 13
                                   VATRate             ,                                                                                         -- 14
                                   DomAmt              ,                                                                                         -- 15
                                   DomVAT              ,                                                                                         -- 16
                                   CurAmt              ,                                                                                         -- 17
                                   CurVAT              ,                                                                                         -- 18
                                   STDQty                                                                                                        -- 19

                                  )                                                                                         
        SELECT @CompanySeq    , A.OrderSeq     , A.OrderSerl    , 0              , A.ItemSeq      , -- 01
               A.UnitSeq      , 0              , 0              , A.Qty          , '1'            , -- 02
                                                                                   ''             , -- 03
               ''             , B.UnitSeq      , 0              , 0              , ''             , -- 04
               0              , '0'            , 0              , ''             , 0              , -- 05
               0              , @UserSeq       , GETDATE()      , '0'            , 0              , -- 06
               0              , 0              , 0              , 0              , 0              , -- 07
               ''             , 0              , 0              , 0              , 0              , -- 08
               ''             , A.Price        , @PgmSeq        , 0              , 0              , -- 09
               0              , 0              , 0              , 0              , 0              , -- 10
               0              , ''             , ''             , ''             , ''             , -- 11
               ''             , ''             , 0              , 0              , 0              , -- 12
               0              , 0              ,                                                    -- 13
               --CASE ISNULL(B.SMVatKind, 0) WHEN 2003001 THEN ISNULL(C.VATRate, 10) ELSE 0 END,    -- 14
               A.VATRate,                                                                           -- 14
               ISNULL(A.Qty, 0) * A.Price - ROUND(ROUND((ISNULL(A.Qty, 0) * A.Price * 10) / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength), -- 15
               ROUND(ROUND(ISNULL(A.Qty, 0) * A.Price * 10 / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength),                                -- 16
               ISNULL(A.Qty, 0) * A.Price - ROUND(ROUND((ISNULL(A.Qty, 0) * A.Price * 10) / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength), -- 17
               ROUND(ROUND(ISNULL(A.Qty, 0) * A.Price * 10 / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength),                                -- 18
               CASE WHEN A.UnitSeq = B.UnitSeq THEN A.Qty 
                                               ELSE ROUND((CASE ISNULL(C.ConvDen, 0) WHEN 0 THEN 0 
                                                                                            ELSE ISNULL(A.Qty, 0) * ISNULL(C.ConvNum, 0) / ISNULL(C.ConvDen, 0) END), @GoodQtyDecLength) END -- 17
          FROM #TSLOrderItem     AS A
               JOIN _TDAItem     AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                  AND B.ItemSeq    = A.ItemSeq
               JOIN _TDAItemUnit AS C WITH(NOLOCK) ON C.CompanySeq = @CompanySeq    
                                                  AND C.ItemSeq    = A.ItemSeq    
                                                  AND C.UnitSeq    = A.UnitSeq  

         IF @@ERROR <> 0 RETURN

        -- 수주 확정 기능을 사용할 경우 확정 테이블에 위에서 생성된 수주건들의 확정 데이터 INSERT 
        IF @IsCfmUsed ='1' 
        BEGIN

            INSERT INTO _TCOMConfirm (CompanySeq , ConfirmSeq , CfmSeq      , CfmSerl     , 
                                      CfmSubSerl , IsAuto     , CfmCode     , CfmDate     , 
                                      UMCfmReason , CfmReason , LastDateTime,
                                      CfmEmpSeq)
            SELECT @CompanySeq , 6303        , OrderSeq     , 0           , 
                   0           , 0           , 1            , OrderDate   , 
                   0           , ''          , GETDATE()    ,
                   CASE ISNULL(@IsCustSalesMan, '') WHEN '1' THEN B.EmpSeq  
                                                             ELSE @EmpSeq  END
              FROM #TSLOrder                        AS A
                   LEFT OUTER JOIN #TSLCustSalesEmp AS B WITH(NOLOCK) ON B.CustSeq = A.CustSeq
                                                                     AND A.OrderDate BETWEEN B.SDate AND B.EDate
             WHERE NOT EXISTS (SELECT TOP 1 1 
                                 FROM _TCOMConfirm WITH(NOLOCK)
                                WHERE ConfirmSeq = 6303
                                  AND CfmSeq     = A.OrderSeq 
                              )
            GROUP BY OrderSeq, OrderDate, B.EmpSeq

        INSERT INTO #SSLOrderSeq (SumSeq)
        SELECT OrderSeq
          FROM #TSLOrder
         GROUP BY OrderSeq 

        -- 위에서 생성된 수주건들을 집계에 포함 
        EXEC _SWSLOrderSum 'A', @CompanySeq


        END
      

        -- 템프테이블에 수주번호(ERP), 확정여부, OrderSeq, OrderSerl UPDATE 
        UPDATE #BIZ_OUT_DataBlock2
           SET ERPOrderNo  = D.OrderNo  ,
               OrderSeq    = D.OrderSeq ,
               OrderSerl   = E.OrderSerl, 
               IsOrder     = '1'       
          FROM #BIZ_OUT_DataBlock2     AS A
               JOIN #OrderData         AS B ON B.DataSeq    = A.DataSeq 
               JOIN _TSLFPShoppingMall AS C ON C.Companyseq = @CompanySeq 
                                           AND C.MallName   = A.MallName
                                           AND C.MallID     = A.MallUserID
               JOIN #TSLOrder          AS D ON D.OrderDate  = A.OrderDate 
                                           AND D.MallSeq    = C.Mallseq 
                                           AND D.CustSeq    = C.CustSeq 
                                           AND D.ItemSeq    = A.ItemSeq
                                           AND D.Price      = B.Price 
               JOIN #TSLOrderItem      AS E ON E.TmpDataSeq = D.TmpDataSeq 
         WHERE A.Status = 0      

        -- 수주가 생성(확정)된 주문내역의 OrderSeq, OrderSerl UPDATE 
        UPDATE _TSLFPShoppingMallOrder
           SET OrderSeq  = B.OrderSeq ,
               OrderSerl = B.OrderSerl
          FROM _TSLFPShoppingMallOrder  AS A
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.OrderNo       = A.OrderNo
                                            AND B.MallOrderNo   = A.MallOrderNo
                                            AND B.MallOrderSeq  = A.MallOrderSeq
                                            AND B.MallOrderSerl = A.MallOrderSerl
                                            AND B.MallName      = A.MallName
                                            AND B.MallUserID    = A.MallUserID 
          WHERE A.CompanySeq = @CompanySeq          

    END


    -- 취소 
    -- 동일한 품목으로 구성된 여러 건의 주문내역을 하나의 수주 건으로 주문확정하였을 때 이 중 일부를 취소했을 때 어떻게 해야할지 정의 필요 
    ELSE IF @WorkingTag = 'C'
    BEGIN

        -- 취소할 주문내역 정보만 따로 담기 
        INSERT INTO #TSLOrderCancel (WorkingTag, Status, OrderSeq, OrderSerl, Qty)
        SELECT 'D', 0, A.OrderSeq, A.OrderSerl, SUM(ISNULL(B.Qty, 0))
          FROM #BIZ_OUT_DataBlock2          AS A
               JOIN _TSLFPShoppingMallOrder AS B ON B.CompanySeq    = @CompanySeq
                                                AND B.OrderDate     = A.OrderDate    
                                                AND B.OrderNo       = A.OrderNo      
                                                AND B.MallOrderNo   = A.MallOrderNo  
                                                AND B.MallOrderSeq  = A.MallOrderSeq 
                                                AND B.MallOrderSerl = A.MallOrderSerl
                                                AND B.MallName      = A.MallName     
                                                AND B.MallUserID    = A.MallUserID   
         GROUP BY A.OrderSeq, A.OrderSerl

        EXEC _SCOMXMLLog @CompanySeq      ,
                         @UserSeq         ,     
                         '_TSLOrder'      ,
                         '#TSLOrderCancel',
                         'OrderSeq'       ,
                         @PgmSeq, ''
         
        EXEC _SCOMXMLLog @CompanySeq          ,
                         @UserSeq             ,     
                         '_TSLOrderItem'      ,
                         '#TSLOrderCancel'    ,
                         'OrderSeq, OrderSerl',
                         @PgmSeq, ''

        -- 수주 집계 제외, 확정취소된 수주건만 포함  
        INSERT INTO #SSLOrderSeq (SumSeq)
        SELECT OrderSeq 
          FROM #TSLOrderCancel
         GROUP BY OrderSeq 

        EXEC _SWSLOrderSum 'D', @CompanySeq    

        -- 여러건의 주문내역이 수주품목 하나로 합쳐졌을 경우 취소하는 주문내역의 수량과 수주품목의 수량을 비교하여 같으면 DELETE, 다르면 UPDATE한다. 
        DELETE _TSLOrderItem 
          FROM _TSLOrderItem        AS A
               JOIN #TSLOrderCancel AS B ON B.OrderSeq  = A.OrderSeq
                                        AND B.OrderSerl = A.OrderSerl               
         WHERE A.CompanySeq = @CompanySeq
           AND A.Qty        = B.Qty 

        UPDATE _TSLOrderItem
           SET Qty    = ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0),
               CurAmt = (ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price - ROUND(ROUND(((ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price * 10) / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength), 
               CurVAT = ROUND(ROUND((ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price * 10 / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength),                                
               DomAmt = (ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price - ROUND(ROUND(((ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price * 10) / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength), 
               DomVat = ROUND(ROUND((ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * A.Price * 10 / (100 + A.VATRate), @CurAmtDecLength), @DomAmtDecLength),                                
               STDQty = CASE WHEN A.UnitSeq = C.UnitSeq THEN ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0) 
                                               ELSE ROUND((CASE ISNULL(D.ConvDen, 0) WHEN 0 THEN 0 
                                                                                            ELSE (ISNULL(A.Qty, 0) - ISNULL(B.Qty, 0)) * ISNULL(D.ConvNum, 0) / ISNULL(D.ConvDen, 0) END), @GoodQtyDecLength) END

          FROM _TSLOrderItem        AS A
               JOIN #TSLOrderCancel AS B ON B.OrderSeq   = A.OrderSeq
                                        AND B.OrderSerl  = A.OrderSerl  
               JOIN _TDAItem        AS C ON C.CompanySeq = A.CompanySeq 
                                        AND C.ItemSeq    = A.ItemSeq      
               JOIN _TDAItemUnit    AS D ON C.CompanySeq = @CompanySeq    
                                        AND C.ItemSeq    = A.ItemSeq    
                                        AND C.UnitSeq    = A.UnitSeq                                          
                                         
         WHERE A.CompanySeq = @CompanySeq
           AND A.Qty        <> B.Qty 

         IF @@ERROR <> 0 RETURN        
                          
         -- 수주 Master 삭제 
         DELETE _TSLOrder
           FROM _TSLOrder            AS A
                JOIN #TSLOrderCancel AS B ON B.OrderSeq = A.OrderSeq
          WHERE A.CompanySeq = @CompanySeq          
            AND NOT EXISTS (
                             SELECT TOP 1 1 
                               FROM _TSLOrderItem WITH(NOLOCK)
                              WHERE CompanySeq = A.CompanySeq
                                AND OrderSeq   = A.OrderSeq 
                           )

        IF @@ERROR <> 0 RETURN

        -- 수주확정 사용 시 확정 데이터 삭제(취소) 
        IF @IsCfmUsed ='1' 
        BEGIN
            
            DELETE _TCOMConfirm
              FROM _TCOMConfirm         AS A 
                   JOIN #TSLOrderCancel AS B ON B.OrderSeq = A.CfmSeq 
             WHERE A.CompanySeq = @CompanySeq
               AND A.ConfirmSeq = 6303 
               AND NOT EXISTS (SELECT TOP 1 1
                                 FROM _TSLOrder WITH(NOLOCK)
                                WHERE CompanySeq = @CompanySeq
                                  AND OrderSeq   = B.OrderSeq 
                              )
         

        END

        -- 확정 취소 시 삭제된 수주건은 제외하고, 수주 집계 생성 
        DELETE #SSLOrderSeq
          FROM #SSLOrderSeq AS A 
         WHERE NOT EXISTS (SELECT TOP 1 1 
                             FROM _TSLOrder WITH(NOLOCK)
                            WHERE CompanySeq = @CompanySeq
                              AND OrderSeq   = A.SumSeq
                          )

        EXEC _SWSLOrderSum 'A', @CompanySeq
        
        -- 취소된 주문내역의 OrderSeq, OrderSerl 제거 
        UPDATE _TSLFPShoppingMallOrder 
           SET OrderSeq  = 0,
               OrderSerl = 0
          FROM _TSLFPShoppingMallOrder  AS A 
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.OrderNo       = A.OrderNo 
                                            AND B.MallOrderNo   = A.MallOrderNo
                                            AND B.MallOrderSeq  = A.MallOrderSeq
                                            AND B.MallOrderSerl = A.MallOrderSerl
                                            AND B.MallName      = A.MallName
                                            AND B.MallUserID    = A.MallUserID
         WHERE A.CompanySeq = @CompanySeq 


        IF @@ERROR <> 0 RETURN

        UPDATE #BIZ_OUT_DataBlock2 
           SET OrderSeq   = 0, 
               OrderSerl  = 0,
               ERPOrderNo = '', 
               IsOrder    = '0'
          FROM #BIZ_OUT_DataBlock2 AS A
         WHERE Status = 0 

    END
       
RETURN

