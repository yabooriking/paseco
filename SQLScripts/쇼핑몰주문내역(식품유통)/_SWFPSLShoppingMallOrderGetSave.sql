IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderGetSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderGetSave
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 주문내역수집
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallOrderGetSave
    @ServiceSeq     INT          = 0 , 
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS    

    -- WITH(NOLOCK) 효과  
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
    EXEC _SYMMETRICKeyOpen    
    
    CREATE TABLE #ShopOrderList
    (
        IDX                   NVARCHAR(50) , -- 주문번호(사방넷) 
        ORDER_ID              NVARCHAR(50) , -- 주문번호(쇼핑몰) 
        MALL_ID               NVARCHAR(50) , -- 쇼핑몰명 
        MALL_USER_ID          NVARCHAR(50) , -- 쇼핑몰ID 
        ORDER_STATUS          NVARCHAR(50) , -- 주문상태 
        USER_ID               NVARCHAR(50) , -- 주문자ID 
        USER_NAME             NVARCHAR(50) , -- 주문자명 
        USER_TEL              NVARCHAR(50) , -- 주문자전화번호 
        USER_CEL              NVARCHAR(50) , -- 주문자핸드폰번호 
        USER_EMAIL            NVARCHAR(200), -- 주문자이메일주소 
        RECEIVE_TEL           NVARCHAR(50) , -- 수취인전화번호 
        RECEIVE_CEL           NVARCHAR(50) , -- 수취인핸드폰번호 
        RECEIVE_EMAIL         NVARCHAR(200), -- 수취인이메일주소 
        DELV_MSG              NVARCHAR(500), -- 배송메세지 
        RECEIVE_NAME          NVARCHAR(50) , -- 수취인명 
        RECEIVE_ZIPCODE       NVARCHAR(50) , -- 수취인우편번호 
        RECEIVE_ADDR          NVARCHAR(200), -- 수취인주소 
        TOTAL_COST            NVARCHAR(50) , -- 주문금액 
        PAY_COST              NVARCHAR(50) , -- 결제금액 
        ORDER_DATE            NVARCHAR(50) , -- 주문일자 
        PARTNER_ID            NVARCHAR(200), -- 매입처명 
        DPARTNER_ID           NVARCHAR(200), -- 물류처명 
        MALL_PRODUCT_ID       NVARCHAR(50) , -- 상품코드(쇼핑몰) 
        PRODUCT_ID            NVARCHAR(50) , -- 품번코드(사방넷) 
        SKU_ID                NVARCHAR(50) , -- 단품코드(사방넷) 
        P_PRODUCT_NAME        NVARCHAR(200), -- 상품명(확정) 
        P_SKU_VALUE           NVARCHAR(200), -- 옵션(확정) 
        PRODUCT_NAME          NVARCHAR(200), -- 상품명(수집) 
        SALE_COST             NVARCHAR(50) , -- 판매가(수집) 
        MALL_WON_COST         NVARCHAR(50) , -- 공급단가 
        WON_COST              NVARCHAR(50) , -- 원가 
        SKU_VALUE             NVARCHAR(200), -- 옵션(수집) 
        SALE_CNT              NVARCHAR(50) , -- 수량 
        DELIVERY_METHOD_STR   NVARCHAR(50) , -- 배송구분 
        DELV_COST             NVARCHAR(50) , -- 배송비(수집) 
        COMPAYNY_GOODS_CD     NVARCHAR(50) , -- 자체상품코드 
        SKU_ALIAS             NVARCHAR(200), -- 옵션별칭 
        BOX_EA                NVARCHAR(50) , -- EA(상품) 
        JUNG_CHK_YN           NVARCHAR(50) , -- 정산대조확인여부 
        MALL_ORDER_SEQ        NVARCHAR(50) , -- 주문순번 
        MALL_ORDER_ID         NVARCHAR(50) , -- 부주문번호 
        ETC_FIELD3            NVARCHAR(50) , -- 수정된 수집옵션명 
        ORDER_GUBUN           NVARCHAR(50) , -- 주문구분 
        P_EA                  NVARCHAR(50) , -- EA(확정) 
        REG_DATE              NVARCHAR(50) , -- 수집일자 
        ORDER_ETC_1           NVARCHAR(200), -- 자사몰필드1 
        ORDER_ETC_2           NVARCHAR(200), -- 자사몰필드2 
        ORDER_ETC_3           NVARCHAR(200), -- 자사몰필드3 
        ORDER_ETC_4           NVARCHAR(200), -- 자사몰필드4 
        ORDER_ETC_5           NVARCHAR(200), -- 자사몰필드5 
        ORDER_ETC_6           NVARCHAR(200), -- 자사몰필드6 
        ORDER_ETC_7           NVARCHAR(200), -- 자사몰필드7 
        ORDER_ETC_8           NVARCHAR(200), -- 자사몰필드8 
        ORDER_ETC_9           NVARCHAR(200), -- 자사몰필드9 
        ORDER_ETC_10          NVARCHAR(200), -- 자사몰필드10 
        ORDER_ETC_11          NVARCHAR(200), -- 자사몰필드11 
        ORDER_ETC_12          NVARCHAR(200), -- 자사몰필드12 
        ORDER_ETC_13          NVARCHAR(200), -- 자사몰필드13 
        ORDER_ETC_14          NVARCHAR(200), -- 자사몰필드14 
        ord_field2            NVARCHAR(50) , -- 세트분리주문구분 
        copy_idx              NVARCHAR(50) , -- 원주문번호(사방넷) 
        GOODS_NM_PR           NVARCHAR(200), -- 출력상품명 
        GOODS_KEYWORD         NVARCHAR(200), -- 상품약어 
        ORD_CONFIRM_DATE      NVARCHAR(50) , -- 주문 확인일자 
        RTN_DT                NVARCHAR(50) , -- 반품 완료일자 
        CHNG_DT               NVARCHAR(50) , -- 교환 완료일자 
        DELIVERY_CONFIRM_DATE NVARCHAR(50) , -- 출고 완료일자 
        CANCEL_DT             NVARCHAR(50) , -- 취소 완료일자 
        CLASS_CD1             NVARCHAR(50) , -- 대분류코드 
        CLASS_CD2             NVARCHAR(50) , -- 중분류코드 
        CLASS_CD3             NVARCHAR(50) , -- 소분류코드 
        CLASS_CD4             NVARCHAR(50) , -- 세분류코드 
        BRAND_NM              NVARCHAR(50) , -- 브랜드명 
        DELIVERY_ID           NVARCHAR(50) , -- 택배사코드 
        INVOICE_NO            NVARCHAR(50) , -- 송장번호 
        HOPE_DELV_DATE        NVARCHAR(50) , -- 배송희망일자 
        FLD_DSP               NVARCHAR(50) , -- 주문엑셀용  
        INV_SEND_MSG          NVARCHAR(200), -- 운송장 전송 결과 메시지 
        MODEL_NO              NVARCHAR(200), -- 모델NO 
        SET_GUBUN             NVARCHAR(50) , -- 상품구분 
        ETC_MSG               NVARCHAR(200), -- 기타메세지 
        DELV_MSG1             NVARCHAR(200), -- 배송메세지 
        MUL_DELV_MSG          NVARCHAR(200), -- 물류메세지 
        BARCODE               NVARCHAR(50) , -- 바코드 
        INV_SEND_DM           NVARCHAR(50) , -- 송장전송일자 
        DELIVERY_METHOD_STR2  NVARCHAR(50) , -- 배송구분(배송비반영) 
        ItemSeq               INT
        )

    CREATE UNIQUE INDEX IDX_ShopOrder ON #ShopOrderList (IDX ASC,ORDER_ID ASC,MALL_ORDER_SEQ ASC,MALL_ORDER_ID ASC)

    INSERT INTO #ShopOrderList
    SELECT  ISNULL(IDX                 , ''), -- 주문번호(사방넷) 
           ISNULL(ORDER_ID             , ''), -- 주문번호(쇼핑몰) 
           ISNULL(MALL_ID              , ''), -- 쇼핑몰명 
           ISNULL(MALL_USER_ID         , ''), -- 쇼핑몰ID 
           ISNULL(ORDER_STATUS         , ''), -- 주문상태 
           ISNULL(USER_ID              , ''), -- 주문자ID 
           ISNULL(USER_NAME            , ''), -- 주문자명 
           ISNULL(USER_TEL             , ''), -- 주문자전화번호 
           ISNULL(USER_CEL             , ''), -- 주문자핸드폰번호 
           ISNULL(USER_EMAIL           , ''), -- 주문자이메일주소 
           ISNULL(RECEIVE_TEL          , ''), -- 수취인전화번호 
           ISNULL(RECEIVE_CEL          , ''), -- 수취인핸드폰번호 
           ISNULL(RECEIVE_EMAIL        , ''), -- 수취인이메일주소 
           ISNULL(DELV_MSG             , ''), -- 배송메세지 + 수집된 주문의 기타 메세지
           ISNULL(RECEIVE_NAME         , ''), -- 수취인명 
           ISNULL(RECEIVE_ZIPCODE      , ''), -- 수취인우편번호 
           ISNULL(RECEIVE_ADDR         , ''), -- 수취인주소 
           ISNULL(TOTAL_COST           , 0 ), -- 주문금액 
           ISNULL(PAY_COST             , 0 ), -- 결제금액 
           ISNULL(ORDER_DATE           , ''), -- 주문일자 
           ISNULL(PARTNER_ID           , ''), -- 매입처명 
           ISNULL(DPARTNER_ID          , ''), -- 물류처명 
           ISNULL(MALL_PRODUCT_ID      , ''), -- 상품코드(쇼핑몰) 
           ISNULL(PRODUCT_ID           , ''), -- 품번코드(사방넷) 
           ISNULL(SKU_ID               , ''), -- 단품코드(사방넷) 
           ISNULL(P_PRODUCT_NAME       , ''), -- 상품명(확정) 
           ISNULL(P_SKU_VALUE          , ''), -- 옵션(확정) 
           ISNULL(PRODUCT_NAME         , ''), -- 상품명(수집) 
           ISNULL(SALE_COST            , 0 ), -- 판매가(수집) 
           ISNULL(MALL_WON_COST        , 0 ), -- 공급단가 
           ISNULL(WON_COST             , 0 ), -- 원가 
           ISNULL(SKU_VALUE            , ''), -- 옵션(수집) 
           ISNULL(SALE_CNT             , 0 ), -- 수량 
           ISNULL(DELIVERY_METHOD_STR  , ''), -- 배송구분 
           ISNULL(DELV_COST            , 0 ), -- 배송비(수집) 
           ISNULL(COMPAYNY_GOODS_CD    , ''), -- 자체상품코드 
           ISNULL(SKU_ALIAS            , ''), -- 옵션별칭 
           ISNULL(BOX_EA               , 0 ), -- EA(상품) 
           ISNULL(JUNG_CHK_YN          , ''), -- 정산대조확인여부 
           ISNULL(MALL_ORDER_SEQ       , ''), -- 주문순번 
           ISNULL(MALL_ORDER_ID        , ''), -- 부주문번호 
           ISNULL(ETC_FIELD3           , ''), -- 수정된 수집옵션명 
           ISNULL(ORDER_GUBUN          , ''), -- 주문구분 
           ISNULL(P_EA                 , 0 ), -- EA(확정) 
           ISNULL(REG_DATE             , ''), -- 수집일자 
           ISNULL(ORDER_ETC_1          , ''), -- 자사몰필드1 
           ISNULL(ORDER_ETC_2          , ''), -- 자사몰필드2 
           ISNULL(ORDER_ETC_3          , ''), -- 자사몰필드3 
           ISNULL(ORDER_ETC_4          , ''), -- 자사몰필드4 
           ISNULL(ORDER_ETC_5          , ''), -- 자사몰필드5 
           ISNULL(ORDER_ETC_6          , ''), -- 자사몰필드6 
           ISNULL(ORDER_ETC_7          , ''), -- 자사몰필드7 
           ISNULL(ORDER_ETC_8          , ''), -- 자사몰필드8 
           ISNULL(ORDER_ETC_9          , ''), -- 자사몰필드9 
           ISNULL(ORDER_ETC_10         , ''), -- 자사몰필드10 
           ISNULL(ORDER_ETC_11         , ''), -- 자사몰필드11 
           ISNULL(ORDER_ETC_12         , ''), -- 자사몰필드12 
           ISNULL(ORDER_ETC_13         , ''), -- 자사몰필드13 
           ISNULL(ORDER_ETC_14         , ''), -- 자사몰필드14 
           ISNULL(ord_field2           , ''), -- 세트분리주문구분 
           ISNULL(copy_idx             , ''), -- 원주문번호(사방넷) 
           ISNULL(GOODS_NM_PR          , ''), -- 출력상품명 
           ISNULL(GOODS_KEYWORD        , ''), -- 상품약어 
           ISNULL(ORD_CONFIRM_DATE     , ''), -- 주문 확인일자 
           ISNULL(RTN_DT               , ''), -- 반품 완료일자 
           ISNULL(CHNG_DT              , ''), -- 교환 완료일자 
           ISNULL(DELIVERY_CONFIRM_DATE, ''), -- 출고 완료일자 
           ISNULL(CANCEL_DT            , ''), -- 취소 완료일자 
           ISNULL(CLASS_CD1            , ''), -- 대분류코드 
           ISNULL(CLASS_CD2            , ''), -- 중분류코드 
           ISNULL(CLASS_CD3            , ''), -- 소분류코드 
           ISNULL(CLASS_CD4            , ''), -- 세분류코드 
           ISNULL(BRAND_NM             , ''), -- 브랜드명 
           ISNULL(DELIVERY_ID          , ''), -- 택배사코드 
           ISNULL(INVOICE_NO           , ''), -- 송장번호 
           ISNULL(HOPE_DELV_DATE       , ''), -- 배송희망일자 
           ISNULL(FLD_DSP              , ''), -- 주문엑셀용  
           ISNULL(INV_SEND_MSG         , ''), -- 운송장 전송 결과 메시지 
           ISNULL(MODEL_NO             , ''), -- 모델NO 
           ISNULL(SET_GUBUN            , ''), -- 상품구분 
           ISNULL(ETC_MSG              , ''), -- 기타메세지 
           ISNULL(DELV_MSG1            , ''), -- 배송메세지 
           ISNULL(MUL_DELV_MSG         , ''), -- 물류메세지 
           ISNULL(BARCODE              , ''), -- 바코드 
           ISNULL(INV_SEND_DM          , ''), -- 송장전송일자 
           ISNULL(DELIVERY_METHOD_STR2 , ''), -- 배송구분(배송비반영) 
           0                               
      FROM _TSLShoppingMallOrderAPI 
      
     -- ERP 품목 내부코드 연결 컬럼에 따라서 매핑
     DECLARE @MallItemColumn NVARCHAR(200),@ERPItemColumn NVARCHAR(200), @SQL NVARCHAR(MAX)

     SELECT @MallItemColumn = B.MinorValue, 
            @ERPItemColumn  = C.MinorValue
       FROM _TSLFPShoppingMallEnv AS A 
            JOIN _TDASMinor       AS B ON A.ShopItemColSeq = B.MinorSeq AND A.CompanySeq = B.CompanySeq
            JOIN _TDASMinor       AS C ON A.ERPItemColSeq  = C.MinorSeq AND A.CompanySeq = C.CompanySeq
      WHERE @CompanySeq = A.CompanySeq

    SELECT @SQL = 'UPDATE #ShopOrderList
                      SET ItemSeq = B.ItemSeq
                     FROM #ShopOrderList AS A 
                          JOIN _TDAItem  AS B ON A.' + @MallItemColumn + ' = CAST(B.' + @ERPItemColumn + ' AS NVARCHAR)
                    WHERE B.CompanySeq = ' + CAST(@CompanySeq AS NVARCHAR) + 
                    ' AND B.' + @ERPItemColumn + CASE WHEN @ERPItemColumn LIKE '%Seq%' THEN   ' <> CAST(0 AS NVARCHAR) ' ELSE ' <> '''' ' END
    EXEC (@SQL)
    --truncate table _TSLShoppingMallOrderLog
    --truncate table _TSLShoppingMallOrder

    SELECT A.MallOrderNo,A.MallOrderSeq,A.MallOrderSerl, B.MallName, A.ReceiptAmt
      INTO #TSLShoppingMallSalesUPLOAD
      FROM _TSLShoppingMallSalesUPLOAD AS A 
           JOIN _TSLFPShoppingMall     AS B ON B.CompanySeq     = A.CompanySeq
                                           AND B.MallName       = A.MallName
           JOIN #ShopOrderList         AS C ON C.ORDER_ID       = A.MallOrderNo  
                                           AND C.MALL_ORDER_SEQ = A.MallOrderSeq 
                                           AND C.MALL_ORDER_ID  = A.MallOrderSerl
                                           AND C.MALL_ID        = A.MallName     
     WHERE ISNULL(A.InvoiceSeq ,0) > 0
       AND A.CompanySeq = @CompanySeq

    -- 데이터 ROW 가 많을 예정이라 _TCOMTableLog 에 담을 타 모듈의 로그기록까지 조회 할 수 없는 상태가 될까봐 Log테이블 별도 구성
    INSERT INTO _TSLFPShoppingMallOrderLog
    (
        LogUserSeq      , LogDateTime     , LogType         , LogPgmSeq       ,
        CompanySeq      , MallOrderNo     , MallOrderSeq    , MallOrderSerl   , OrderDate       ,
        OrderNo         , MallName        , MallUserID      , OrderStatus     , CfmItem         , CfmOption      , ItemSeq         ,
        CompanyItem     , MallItemID      , ItemID          , SingleItemID    , ModelNo         ,
        OptionName      , Qty             , OrderAmt        , PayAmt          , OrdUserID       ,
        OrdUserName     , OrdTelNo        , OrdPhone        , OrdEmail        , RecvTelNo       ,
        RecvPhone       , RecvEmail       , RecvUserName    , RecvZipCode     , RecvAddr        ,
        TransCustName   , TransNo         , TranMSG         , OrderSeq        , OrderSerl       ,
        LastUserSeq     , LastDateTime
    )
    SELECT @UserSeq          ,  GETDATE()        ,  'U'              ,  @PgmSeq, 
           B.CompanySeq      , B.MallOrderNo     , B.MallOrderSeq    , B.MallOrderSerl   , B.OrderDate       ,
           B.OrderNo         , B.MallName        , B.MallUserID      , B.OrderStatus     , B.CfmItem         , B.CfmOption       , B.ItemSeq         ,
           B.CompanyItem     , B.MallItemID      , B.ItemID          , B.SingleItemID    , B.ModelNo         ,
           B.OptionName      , B.Qty             , B.OrderAmt        , B.PayAmt          , B.OrdUserID       ,
           B.OrdUserName     , B.OrdTelNo        , B.OrdPhone        , B.OrdEmail        , B.RecvTelNo       ,
           B.RecvPhone       , B.RecvEmail       , B.RecvUserName    , B.RecvZipCode     , B.RecvAddr        ,
           B.TransCustName   , B.TransNo         , B.TranMSG         , B.OrderSeq        , B.OrderSerl       ,
           B.LastUserSeq     , B.LastDateTime
      FROM _TSLFPShoppingMallOrder AS B 
           JOIN #ShopOrderList   AS A ON B.CompanySeq   = @CompanySeq 
                                     AND B.OrderNo        = A.IDX
                                     AND B.MallOrderNo    = A.ORDER_ID 
                                     AND B.MallOrderSeq   = A.MALL_ORDER_SEQ
                                     AND B.MallOrderSerl  = A.MALL_ORDER_ID
                                     AND B.MallName       = A.MALL_ID 
                                     AND B.MallUserID     = A.MALL_USER_ID
           --LEFT OUTER JOIN #TSLShoppingMallSalesUPLOAD AS D ON B.MallOrderNo    = D.MallOrderNo
           --                                                AND B.MallOrderSeq   = D.MallOrderSeq
           --                                                AND B.MallOrderSerl  = D.MallOrderSerl
           --                                                AND B.MallName       = D.MallName
     WHERE (ISNULL(B.OrderSeq ,0) = 0)-- OR D.MallOrderSeq IS NULL)


    UPDATE _TSLFPShoppingMallOrder
       SET  --OrderDate       = LEFT(A.ORDER_DATE,8)      ,
            OrderStatus     = A.ORDER_STATUS     ,
            --CfmItem         = A.P_PRODUCT_NAME          ,
            --CfmOption       = A.P_SKU_VALUE             ,
            ItemSeq         = CASE WHEN B.ItemSeq = 0 THEN A.ItemSeq ELSE B.ItemSeq END ,
            CompanyItem     = A.COMPAYNY_GOODS_CD,
            --MallItemID      = A.MALL_PRODUCT_ID         ,
            --ItemID          = A.PRODUCT_ID              ,
            SingleItemID    = A.SKU_ID           ,
            ModelNo         = A.MODEL_NO         ,
            OptionName      = A.SKU_ALIAS        ,
            --Qty             = A.P_EA           ,
            OrderAmt        = A.TOTAL_COST       ,
            PayAmt          = A.PAY_COST         ,
            OrdUserID       = A.USER_ID          ,
            OrdUserName     = A.USER_NAME        ,
            OrdTelNo        = dbo._FWCOMEncrypt(A.USER_TEL, '_TSLFPShoppingMallOrder', 'OrdTelNo', @CompanySeq),
            OrdPhone        = dbo._FWCOMEncrypt(A.USER_CEL, '_TSLFPShoppingMallOrder', 'OrdPhone', @CompanySeq),
            OrdEmail        = A.USER_EMAIL       ,
            RecvTelNo       = dbo._FWCOMEncrypt(A.RECEIVE_TEL, '_TSLFPShoppingMallOrder', 'RecvTelNo', @CompanySeq),
            RecvPhone       = dbo._FWCOMEncrypt(A.RECEIVE_CEL, '_TSLFPShoppingMallOrder', 'RecvPhone', @CompanySeq),
            RecvEmail       = A.RECEIVE_EMAIL    ,
            RecvUserName    = A.RECEIVE_NAME     ,
            RecvZipCode     = A.RECEIVE_ZIPCODE  ,
            RecvAddr        = A.RECEIVE_ADDR     ,
            TransCustName   = A.DELIVERY_ID      ,
            TransNo         = A.INVOICE_NO       ,
            TranMSG         = A.DELV_MSG         ,
            LastUserSeq     = @UserSeq           ,
            LastDateTime    = GETDATE()
      FROM _TSLFPShoppingMallOrder AS B 
           JOIN #ShopOrderList     AS A ON B.CompanySeq   = @CompanySeq 
                                       AND OrderNo        = A.IDX
                                       AND MallOrderNo    = A.ORDER_ID 
                                       AND MallOrderSeq   = A.MALL_ORDER_SEQ
                                       AND MallOrderSerl  = A.MALL_ORDER_ID
                                       AND MallName       = A.MALL_ID 
                                       AND MallUserID     = A.MALL_USER_ID
           --LEFT OUTER JOIN #TSLShoppingMallSalesUPLOAD AS D ON B.MallOrderNo    = D.MallOrderNo
           --                                                AND B.MallOrderSeq   = D.MallOrderSeq
           --                                                AND B.MallOrderSerl  = D.MallOrderSerl
           --                                                AND B.MallName       = D.MallName
     WHERE (ISNULL(B.OrderSeq ,0) = 0 )-- OR D.MallOrderSeq IS NULL)

    INSERT INTO _TSLFPShoppingMallOrder
    (
        CompanySeq      , MallOrderNo     , MallOrderSeq    , MallOrderSerl   ,OrderDate       ,OrderNo         ,
        MallName        , MallUserID      , OrderStatus     , CfmItem         ,CfmOption       ,ItemSeq         , CompanyItem     ,MallItemID      ,
        ItemID          , SingleItemID    , ModelNo         , OptionName      ,Qty             ,OrderAmt        ,
        PayAmt          , OrdUserID       , OrdUserName     , OrdTelNo        ,OrdPhone        ,OrdEmail        ,
        RecvTelNo       , RecvPhone       , RecvEmail       , RecvUserName    ,RecvZipCode     ,RecvAddr        ,
        TransCustName   , TransNo         , TranMSG         , OrderSeq        ,OrderSerl       ,LastUserSeq         ,
        LastDateTime    
    )
    SELECT
            @CompanySeq      ,ORDER_ID         ,MALL_ORDER_SEQ   ,MALL_ORDER_ID    ,LEFT(ORDER_DATE,8),IDX             ,
            MALL_ID          ,MALL_USER_ID     ,ORDER_STATUS     ,P_PRODUCT_NAME   ,P_SKU_VALUE      ,ItemSeq          , COMPAYNY_GOODS_CD,MALL_PRODUCT_ID  ,
            PRODUCT_ID       ,SKU_ID           ,MODEL_NO         ,SKU_ALIAS        ,P_EA             ,TOTAL_COST       ,
            PAY_COST         ,USER_ID          ,USER_NAME        ,
            dbo._FWCOMEncrypt(USER_TEL, '_TSLShoppingMallOrder', 'OrdTelNo', @CompanySeq),
            dbo._FWCOMEncrypt(USER_CEL, '_TSLShoppingMallOrder', 'OrdPhone', @CompanySeq),
            USER_EMAIL       ,
            dbo._FWCOMEncrypt(RECEIVE_TEL, '_TSLShoppingMallOrder', 'RecvTelNo', @CompanySeq),
            dbo._FWCOMEncrypt(RECEIVE_CEL, '_TSLShoppingMallOrder', 'RecvPhone', @CompanySeq),
            RECEIVE_EMAIL    ,RECEIVE_NAME     ,RECEIVE_ZIPCODE  ,RECEIVE_ADDR     ,
            DELIVERY_ID      ,INVOICE_NO       ,DELV_MSG         ,0                ,0                ,@UserSeq          ,     
            GETDATE()
      FROM #ShopOrderList AS A 
     WHERE NOT EXISTS( SELECT TOP 1 1 
                         FROM _TSLFPShoppingMallOrder 
                        WHERE @CompanySeq   = CompanySeq 
                          AND OrderNo       = A.IDX
                          AND MallOrderNo   = A.ORDER_ID 
                          AND MallOrderSeq  = A.MALL_ORDER_SEQ
                          AND MallOrderSerl = A.MALL_ORDER_ID
                          AND MallName      = A.MALL_ID 
                          AND MallUserID    = A.MALL_USER_ID
                     )
     ORDER BY A.MALL_ID, A.ORDER_ID


     TRUNCATE TABLE _TSLShoppingMallOrderAPI

RETURN
