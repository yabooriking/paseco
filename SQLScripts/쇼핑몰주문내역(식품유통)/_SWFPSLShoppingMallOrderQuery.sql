IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallOrderQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallOrderQuery
GO

/********************************
 설    명 - 쇼핑몰주문내역 : 조회
 작 성 일 - 2020년 05월 22일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallOrderQuery  
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS    
    -- WITH(NOLOCK) 효과  
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
    EXEC _SYMMETRICKeyOpen

    DECLARE @OrderDateFr        NCHAR(8)     ,
            @OrderDateTo        NCHAR(8)     ,
            @MallOrderNo        NVARCHAR(200),
            @MallName           NVARCHAR(200),
            @OrdSearchTEXT      NVARCHAR(200),
            @RecvSearchTEXT     NVARCHAR(200),
            @ItemSearchTEXT     NVARCHAR(200),
            @SMOrdSearchSeq     INT          ,
            @SMRecvSearchSeq    INT          ,
            @SMItemSearchSeq    INT          ,
            @SMReceiptSeq       INT          ,
            @SMMoveSeq          INT          ,
            @OrderNo            NVARCHAR(100)

    SELECT  @OrderDateFr     = ISNULL(OrderDateFr    , ''),
            @OrderDateTo     = ISNULL(OrderDateTo    , ''),
            @MallOrderNo     = ISNULL(MallOrderNo    , ''),
            @MallName        = ISNULL(MallName       , ''),
            @OrdSearchTEXT   = ISNULL(OrdSearchTEXT  , ''),
            @RecvSearchTEXT  = ISNULL(RecvSearchTEXT , ''),
            @ItemSearchTEXT  = ISNULL(ItemSearchTEXT , ''),
            @SMOrdSearchSeq  = ISNULL(SMOrdSearchSeq , 0 ),
            @SMRecvSearchSeq = ISNULL(SMRecvSearchSeq, 0 ),
            @SMItemSearchSeq = ISNULL(SMItemSearchSeq, 0 ),
            @OrderNo         = ISNULL(OrderNo        , '')
      FROM  #BIZ_IN_DataBlock1
    
    -- JOIN 에 실테이블 걸면 너무 느림..
    SELECT ItemSeq, ItemName, ItemNo, Spec
      INTO #TDAItem
      FROM _TDAItem AS A 
     WHERE A.CompanySeq = @CompanySeq
       AND EXISTS( SELECT 1 
                     FROM _TSLFPShoppingMallOrder 
                    WHERE ISNULL(ItemSeq ,0) <> 0 
                      AND A.ItemSeq    = ItemSeq 
                      AND A.Companyseq = CompanySeq
                 )

    SELECT CustSeq, CustName, CustNo
      INTO #TDACust
      FROM _TDACust AS A 
     WHERE A.CompanySeq = @CompanySeq
       AND EXISTS( SELECT 1 
                     FROM _TSLFPShoppingMallOrder 
                    WHERE ISNULL(TransCustName,'') <> '' 
                      AND A.CustNo     = TransCustName 
                      AND A.CompanySeq = CompanySeq)

    SELECT A.MallOrderNo,A.MallOrderSeq,A.MallOrderSerl, B.MallName, A.ReceiptAmt
      INTO #TSLShoppingMallSalesUPLOAD
      FROM _TSLShoppingMallSalesUPLOAD AS A 
           JOIN _TSLFPShoppingMall     AS B ON A.MallName = B.MallName
                                           AND A.CompanySeq = B.CompanySeq
     WHERE A.CompanySeq = @CompanySeq
       AND ISNULL(A.InvoiceSeq ,0) > 0


    SELECT  CASE WHEN ISNULL(F.CfmCode, '0') = '1' THEN '1' 
                                                   ELSE '0' END AS IsOrder ,
           A.OrderDate            AS OrderDate    , 
           A.OrderNo              AS OrderNo      ,
           A.MallOrderNo          AS MallOrderNo  ,
           A.MallOrderSeq         AS MallOrderSeq ,
           A.MallOrderSerl        AS MallOrderSerl,
           A.MallName             AS MallName     ,
           A.MallUserID           AS MallUserID   ,
           A.OrderStatus          AS OrderStatus  ,
           A.CfmItem              AS CfmItem      ,
           A.CfmOption            AS CfmOption    ,
           ISNULL(B.ItemName ,'') AS ItemName     ,
           ISNULL(B.ItemNo   ,'') AS ItemNo       ,
           A.CompanyItem          AS CompanyItem  ,
           A.MallItemID           AS MallItemID   ,
           A.ItemID               AS ItemID       ,
           A.SingleItemID         AS SingleItemID ,
           A.ModelNo              AS ModelNo      ,
           A.OptionName           AS OptionName   ,
           A.Qty                  AS Qty          ,
           0                      AS StockQty     ,
           A.OrderAmt             AS OrderAmt     ,
           A.PayAmt               AS PayAmt       ,
           D.ReceiptAmt           AS ReceiptAmt   ,
           A.OrdUserID            AS OrdUserID    ,
           A.OrdUserName          AS OrdUserName  ,
           A.OrdEmail             AS OrdEmail     ,
           A.RecvEmail            AS RecvEmail    ,
           A.RecvUserName         AS RecvUserName ,
           A.RecvZipCode          AS RecvZipCode  ,
           A.RecvAddr             AS RecvAddr     ,
           ISNULL(C.CustName ,'') AS TransCustName,
           A.TransNo              AS TransNo      ,
           A.TranMSG              AS TranMSG      ,
           A.ItemSeq              AS ItemSeq      ,
           E.OrderNo              AS ERPOrderNo   ,
           ISNULL(A.OrderSeq ,0)  AS OrderSeq     , 
           ISNULL(A.OrderSerl,0)  AS OrderSerl    ,
           dbo._FWCOMDecrypt(A.OrdTelNo, '_TSLFPShoppingMallOrder', 'OrdTelNo', @CompanySeq) AS OrdTelNo     ,
           dbo._FWCOMDecrypt(A.OrdPhone, '_TSLFPShoppingMallOrder', 'OrdPhone', @CompanySeq) AS OrdPhone     ,
           dbo._FWCOMDecrypt(A.RecvTelNo, '_TSLFPShoppingMallOrder', 'RecvTelNo', @CompanySeq)   AS RecvTelNo,
           dbo._FWCOMDecrypt(A.RecvPhone, '_TSLFPShoppingMallOrder', 'RecvPhone', @CompanySeq)   AS RecvPhone
      FROM _TSLFPShoppingMallOrder                     AS A 
           LEFT OUTER JOIN #TDAItem                    AS B              ON A.ItemSeq       = B.ItemSeq
           LEFT OUTER JOIN #TDACust                    AS C              ON A.TransCustName = C.CustNo
                                                                        AND ISNULL(A.TransCustName,'') <> ''
           LEFT OUTER JOIN #TSLShoppingMallSalesUPLOAD AS D              ON A.MallOrderNo   = D.MallOrderNo
                                                                        AND A.MallOrderSeq  = D.MallOrderSeq
                                                                        AND A.MallOrderSerl = D.MallOrderSerl
                                                                        AND A.MallName      = D.MallName
           LEFT OUTER JOIN _TSLOrder                   AS E WITH(NOLOCK) ON E.CompanySeq    = A.CompanySeq
                                                                        AND E.OrderSeq      = A.OrderSeq
           LEFT OUTER JOIN _TCOMConfirm                AS F WITH(NOLOCK) ON F.CompanySeq    = A.CompanySeq
                                                                        AND F.CfmSeq        = A.OrderSeq 
                                                                        AND F.ConfirmSeq    = 6303
                                                                            
                                                                
     WHERE @CompanySeq = A.CompanySeq
       AND A.OrderDate BETWEEN @OrderDateFr AND @OrderDateTo        
       AND (@MallOrderNo        = '' OR A.MallOrderNo        LIKE @MallOrderNo        + '%')
       AND (@MallName           = '' OR A.MallName           LIKE @MallName           + '%')
       AND (@OrderNo            = '' OR A.OrderNo            LIKE @OrderNo            + '%')
       AND ((@SMOrdSearchSeq = 8156001 AND (@OrdSearchTEXT   = '' OR A.OrdUserID     LIKE '%' + @OrdSearchTEXT + '%')) --주문자ID
         OR (@SMOrdSearchSeq = 8156002 AND (@OrdSearchTEXT   = '' OR A.OrdUserName   LIKE '%' + @OrdSearchTEXT + '%')) --주문자명
         OR (@SMOrdSearchSeq = 8156003 AND (@OrdSearchTEXT   = '' OR dbo._FWCOMDecrypt(A.OrdTelNo, '_TSLFPShoppingMallOrder', 'OrdTelNo', @CompanySeq) LIKE '%' + @OrdSearchTEXT + '%')) --주문자전화번호
         OR (@SMOrdSearchSeq = 8156004 AND (@OrdSearchTEXT   = '' OR dbo._FWCOMDecrypt(A.OrdPhone, '_TSLFPShoppingMallOrder', 'OrdPhone', @CompanySeq) LIKE '%' + @OrdSearchTEXT + '%')) --주문자핸드폰번호
         OR (@SMOrdSearchSeq = 8156005 AND (@OrdSearchTEXT   = '' OR A.OrdEmail      LIKE '%' + @OrdSearchTEXT + '%')) --주문자이메일주소
         OR @SMOrdSearchSeq  = 0
       )
       AND ((@SMRecvSearchSeq = 8157001 AND (@RecvSearchTEXT = '' OR A.RecvUserName LIKE '%' + @RecvSearchTEXT + '%')) --수취인명
         OR (@SMRecvSearchSeq = 8157002 AND (@RecvSearchTEXT = '' OR A.RecvZipCode  LIKE '%' + @RecvSearchTEXT + '%')) --수취인우편번호
         OR (@SMRecvSearchSeq = 8157003 AND (@RecvSearchTEXT = '' OR A.RecvAddr     LIKE '%' + @RecvSearchTEXT + '%')) --수취인주소
         OR (@SMRecvSearchSeq = 8157004 AND (@RecvSearchTEXT = '' OR dbo._FWCOMDecrypt(A.RecvTelNo, '_TSLFPShoppingMallOrder', 'RecvTelNo', @CompanySeq) LIKE '%' + @RecvSearchTEXT + '%')) --수취인전화번호
         OR (@SMRecvSearchSeq = 8157005 AND (@RecvSearchTEXT = '' OR dbo._FWCOMDecrypt(A.RecvPhone, '_TSLFPShoppingMallOrder', 'RecvPhone', @CompanySeq) LIKE '%' + @RecvSearchTEXT + '%')) --수취인핸드폰번호
         OR (@SMRecvSearchSeq = 8157006 AND (@RecvSearchTEXT = '' OR A.RecvEmail    LIKE '%' + @RecvSearchTEXT + '%')) --수취인이메일주소
         OR @SMRecvSearchSeq = 0
       )
       AND ((@SMItemSearchSeq = 8154001 AND (@ItemSearchTEXT = '' OR A.MallItemID   LIKE '%' + @ItemSearchTEXT + '%')) --상품코드(쇼핑몰)
         OR (@SMItemSearchSeq = 8154002 AND (@ItemSearchTEXT = '' OR A.ItemID       LIKE '%' + @ItemSearchTEXT + '%')) --품번코드(사방넷)
         OR (@SMItemSearchSeq = 8154003 AND (@ItemSearchTEXT = '' OR A.SingleItemID LIKE '%' + @ItemSearchTEXT + '%')) --단품코드(사방넷)
         OR (@SMItemSearchSeq = 8154004 AND (@ItemSearchTEXT = '' OR A.CompanyItem  LIKE '%' + @ItemSearchTEXT + '%')) --자체상품코드
         OR (@SMItemSearchSeq = 8154005 AND (@ItemSearchTEXT = '' OR A.OptionName   LIKE '%' + @ItemSearchTEXT + '%')) --옵션별칭
         OR (@SMItemSearchSeq = 8154006 AND (@ItemSearchTEXT = '' OR A.ModelNo      LIKE '%' + @ItemSearchTEXT + '%')) --모델NO
         OR (@SMItemSearchSeq = 8154007 AND (@ItemSearchTEXT = '' OR A.CfmItem      LIKE '%' + @ItemSearchTEXT + '%')) --상품명(확정)
         OR @SMItemSearchSeq = 0
       ) 
     ORDER BY A.OrderNo, A.MallOrderNo, A.MallOrderSeq
RETURN