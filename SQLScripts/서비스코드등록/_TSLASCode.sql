
CREATE TABLE _TSLASCode (
    CompanySeq   INT          ,
    ASCodeSeq    INT          ,
    SMASCode     INT          ,
    ASCode       NVARCHAR(100),
    UMItemClass  INT          ,
    Remark       NVARCHAR(400),
    IsUse        NCHAR(1)     ,
    PgmSeq       INT          ,
    LastUserSeq  INT          ,
    LastDateTime DATETIME     )

CREATE CLUSTERED INDEX IDX_TSLASCode ON _TSLASCode (CompanySeq, ASCodeSeq)