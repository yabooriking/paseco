IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASCodeCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASCodeCheck
GO

/********************************
 설    명 - 서비스코드등록:체크
 작 성 일 - 2020년 05월 14일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASCodeCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @Phone          INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @Count        INT          ,
            @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT  

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock2)
    BEGIN

        -------------------------------------------      
        -- 서비스코드(고장증상, 처리유형, 처리부위) 삭제 시, 해당 서비스코드가 입력된 서비스 접수, 처리 건이 있으면 삭제 불가
        /*
            참고화면
            서비스접수 - [서비스접수]
            서비스처리 - [서비스처리]
        */
        -------------------------------------------  

        -- 서비스접수내역이 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
                               @Status      OUTPUT     ,  
                               @Results     OUTPUT     ,  
                               1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq            ,  
                               24355, N'서비스접수정보', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스접수정보%' 
                               308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock2 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS ( SELECT TOP 1 1
                          FROM _TSLASServiceReceipt WITH(NOLOCK)
                         WHERE CompanySeq = @CompanySeq
                           AND TroubleSeq = A.ASCodeSeq
                      )


		-------------------------------------------      
		-- 코드분류 + 코드 중복체크 
		-- 하나의 코드 분류 아래에 동일한 명칭의 코드 저장 불가 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT  ,  
                               @Status      OUTPUT  ,  
                               @Results     OUTPUT  ,  
                               6                    , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq         ,  
                               0, N''                 -- SELECT * FROM _TCADictionary WHERE Word like '%고객명%'   
							 --  1396, N'전화번호'      -- SELECT * FROM _TCADictionary WHERE Word like '%전화번호%' 

        UPDATE #BIZ_OUT_DataBlock2 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock2 AS A
               JOIN ( SELECT  S.SMASCode, S.ASCode
                        FROM (
                               SELECT A.SMASCode, A.ASCode
                                 FROM #BIZ_OUT_DataBlock2 AS A
                                WHERE A.WorkingTag IN ('A', 'U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.SMASCode, A.ASCode
                                 FROM _TSLASCode AS A WITH(NOLOCK)
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock2
                                                    WHERE WorkingTag IN ('A', 'U')
                                                      AND Status	 = 0
                                                      AND ASCodeSeq  = A.ASCodeSeq
                                                 )
					         ) AS S
				       GROUP BY S.SMASCode, S.ASCode
				      HAVING COUNT(1) > 1
                    ) AS B
                 ON B.SMASCode = A.SMASCode
				AND B.ASCode   = A.ASCode
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0  

    	-------------------------------------------      
		-- INSERT 번호부여(맨 마지막 처리) 
		-------------------------------------------         
		SELECT  @Count = COUNT(1) FROM #BIZ_OUT_DataBlock2 WHERE (WorkingTag = 'A' AND Status = 0)  

		IF(@Count > 0)  
		BEGIN  
            
			-- 키값생성코드부분 시작  
			EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLASCode', 'ASCodeSeq', @Count  
  
			-- Temp Table 에 생성된 키값 UPDATE  
			UPDATE #BIZ_OUT_DataBlock2
			   SET ASCodeSeq = @Seq + DataSeq
			  FROM #BIZ_OUT_DataBlock2 AS A
			 WHERE A.WorkingTag = 'A' 
			   AND A.Status     = 0  

  
		END  


    END

RETURN
