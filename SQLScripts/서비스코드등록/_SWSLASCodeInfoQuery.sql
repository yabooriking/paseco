IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASCodeInfoQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASCodeInfoQuery
GO

/********************************
 설    명 - 서비스코드등록:코드분류조회
 작 성 일 - 2020년 05월 14일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASCodeInfoQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

	SELECT A.MinorName AS SMASCodeName,
           A.MinorSeq  AS SMASCode
      FROM _TDASMinor AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.MajorSeq   = 100040


RETURN
