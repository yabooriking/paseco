IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASCodeSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASCodeSave
GO

/********************************
 설    명 - 서비스코드등록:저장
 작 성 일 - 2020년 05월 14일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASCodeSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq          ,      
                      @UserSeq             ,      
                      '_TSLASCode'         ,     
                      '#BIZ_OUT_DataBlock2', -- 템프테이블명      
                      'ASCodeSeq'          , -- 로그 생성 대상 테이블 키, 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq              
                      --'SMASCode,ASCode_Old' -- 템프테이블 키 
                              

    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
        
        -- DELETE 
        DELETE _TSLASCode    
          FROM _TSLASCode               AS A    
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.ASCodeSeq = A.ASCodeSeq
         WHERE A.CompanySeq = @CompanySeq
		   AND B.WorkingTag = 'D'   
		   AND B.Status     = 0  
           
        IF @@ERROR <> 0 RETURN      

		   
    END  
  
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  

       UPDATE _TSLASCode    
          SET ASCode       = ISNULL(B.ASCode, '')    ,	  
              UMItemClass  = ISNULL(B.ItemClassSSeq, 0),
              Remark       = ISNULL(B.Remark, '')    ,
              IsUse        = ISNULL(B.IsUse , '1')   ,
			  PgmSeq       = @PgmSeq                 , 
              LastUserSeq  = @UserSeq                ,
              LastDateTime = GETDATE()  
         FROM _TSLASCode               AS A 
              JOIN #BIZ_OUT_DataBlock2 AS B ON B.ASCodeSeq = A.ASCodeSeq
        WHERE A.CompanySeq = @CompanySeq   
          AND B.WorkingTag = 'U' 
		  AND B.Status     = 0  
  
        IF @@ERROR <> 0 RETURN   
         

    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLASCode(CompanySeq  , ASCodeSeq    , SMASCode     , ASCode      , UMItemClass , 
                               Remark      , IsUse        , PgmSeq	     , LastUserSeq , LastDateTime)   
        SELECT @CompanySeq  , ASCodeSeq    , SMASCode     , ASCode       , ItemClassSSeq, 
               Remark       ,'1'           , @PgmSeq      , @UserSeq     , GETDATE()
          FROM #BIZ_OUT_DataBlock2   
         WHERE WorkingTag = 'A' 
		   AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN    


    END

RETURN