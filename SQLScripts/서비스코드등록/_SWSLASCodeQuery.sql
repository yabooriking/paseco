IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASCodeQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASCodeQuery
GO

/********************************
 설    명 - 서비스코드등록:조회
 작 성 일 - 2020년 05월 14일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASCodeQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    DECLARE @ItemClassSSeq INT          ,
            @ASCode        NVARCHAR(100),
            @IsUse         NCHAR(1)     ,
            @SMASCode      INT 

    SELECT @ItemClassSSeq = ISNULL(ItemClassSSeq, 0),
	       @ASCode        = ISNULL(ASCode, '')      , 
           @IsUse         = ISNULL(IsUse, '0')      ,
           @SMASCode      = ISNULL(SMASCode, 0)
	  FROM #BIZ_IN_DataBlock1

	SELECT B.MinorName AS ItemClassSName,
           B.MinorSeq AS ItemClassSSeq  ,
           A.ASCode                     ,
           A.Remark                     ,
           A.IsUse                      ,
           A.SMASCode                   ,
           A.ASCodeSeq     
	  FROM _TSLASCode                 AS A WITH(NOLOCK)
	       LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
	                                                   AND B.MinorSeq   = A.UMItemClass 
	 WHERE A.CompanySeq = @CompanySeq 
       AND A.SMASCode   = @SMASCode
       AND A.ASCode     LIKE @ASCode + '%' 
       AND (@ItemClassSSeq = 0   OR A.UMItemClass = @ItemClassSSeq)
       AND (@IsUse         = '0' OR (@IsUse = '1' AND A.IsUse = '1'))

RETURN
