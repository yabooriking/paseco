IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWCCodeHelpASCode') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWCCodeHelpASCode
GO

/********************************
 설    명 - 서비스코드분류(고장증상, 처리방법, 처리부위) 코드도움 SP
 작 성 일 - 2020년 05월 18일
 작 성 자 - 김재호
 수정내역 - 
*********************************/ 

CREATE PROC dbo._SWCCodeHelpASCode
    @WorkingTag      NVARCHAR(1)             ,                        
    @LanguageSeq     INT                     ,                        
    @CodeHelpSeq     INT                     ,                        
    @DefQueryOption  INT                     , -- 2: direct search                        
    @CodeHelpType    TINYINT                 ,                        
    @PageCount       INT               = 20  ,             
    @CompanySeq      INT               = 1   ,     
    @Keyword         NVARCHAR(100)     = ''  ,                        
    @Param1          NVARCHAR(50)      = ''  ,            
    @Param2          NVARCHAR(50)      = ''  ,            
    @Param3          NVARCHAR(50)      = ''  ,            
    @Param4          NVARCHAR(50)      = ''  ,
    @SubConditionSql NVARCHAR(500)     ='1=1',
    @AccUnit         INT               = 0   , 
    @BizUnit         INT               = 0   , 
    @FactUnit        INT               = 0   , 
    @DeptSeq         INT               = 0   , 
    @WkDeptSeq       INT               = 0   , 
    @EmpSeq          INT               = 0   ,
    @UserSeq         INT               = 0
AS

	SET ROWCOUNT @PageCount

    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen


    SELECT A.ASCode,
           A.ASCodeSeq
      FROM _TSLASCode                 AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDASMinor AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq
                                                       AND B.MinorSeq   = A.SMASCode
     WHERE A.CompanySeq = @CompanySeq
       AND A.ASCode     LIKE @Keyword
       AND @Param1      = A.SMASCode
       AND (@Param2 = '' OR A.UMItemClass = @Param2)
RETURN
