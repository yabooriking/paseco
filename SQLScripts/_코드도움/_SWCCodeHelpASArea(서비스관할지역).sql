IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWCCodeHelpASArea') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWCCodeHelpASArea
GO

/********************************
 설    명 - 서비스관할지역 코드도움 SP
 작 성 일 - 2020년 05월 18일
 작 성 자 - 김재호
 수정내역 - 
*********************************/ 

CREATE PROC dbo._SWCCodeHelpASArea
    @WorkingTag      NVARCHAR(1)             ,                        
    @LanguageSeq     INT                     ,                        
    @CodeHelpSeq     INT                     ,                        
    @DefQueryOption  INT                     , -- 2: direct search                        
    @CodeHelpType    TINYINT                 ,                        
    @PageCount       INT               = 20  ,             
    @CompanySeq      INT               = 1   ,     
    @Keyword         NVARCHAR(100)     = ''  ,                        
    @Param1          NVARCHAR(50)      = ''  ,            
    @Param2          NVARCHAR(50)      = ''  ,            
    @Param3          NVARCHAR(50)      = ''  ,            
    @Param4          NVARCHAR(50)      = ''  ,
    --@SubConditionSql NVARCHAR(500)     ='1=1', -- 로그인정보를 사용하며, 하부조건을 사용하지 않을 경우 이 파라미터는 주석처리
    @AccUnit         INT               = 0   , 
    @BizUnit         INT               = 0   , 
    @FactUnit        INT               = 0   , 
    @DeptSeq         INT               = 0   , 
    @WkDeptSeq       INT               = 0   , 
    @EmpSeq          INT               = 0   ,
    @UserSeq         INT               = 0

AS

	SET ROWCOUNT @PageCount

    DECLARE @UserType  INT     ,
            @CustSeq   INT     , 
            @IsDefault NCHAR(1) = '0' -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터의 관할지역만 조회 가능 

    SELECT @UserType = ISNULL(UserType, 0),
           @CustSeq  = ISNULL(CustSeq, 0)  
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq 

    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              )
    BEGIN

        SET @IsDefault = '1'


    END

    SELECT A.MinorName AS UMArea,
           B.UMAreaSeq
      FROM _TDAUMinor                        AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TSLASArea        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
                                                              AND B.UMAreaSeq        = A.MinorSeq    
           LEFT OUTER JOIN _TSLServiceCenter AS C WITH(NOLOCK) ON C.CompanySeq       = B.CompanySeq
                                                              AND C.ServiceCenterSeq = B.ServiceCenterSeq
     WHERE A.CompanySeq = @CompanySeq 
       AND A.MajorSeq   = 100116
       AND A.MinorName  LIKE @Keyword
       AND (@Param1 = '' OR B.ServiceCenterSeq = @Param1)
       AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND C.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND C.CustSeq = @CustSeq))
  

    --IF @UserType = 1001 
    --BEGIN

    --    SELECT A.MinorName AS UMArea,
    --           B.UMAreaSeq
    --      FROM _TDAUMinor                        AS A WITH(NOLOCK)
    --           LEFT OUTER JOIN _TSLASArea        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
    --                                                              AND B.UMAreaSeq        = A.MinorSeq    
    --           LEFT OUTER JOIN _TSLServiceCenter AS C WITH(NOLOCK) ON C.CompanySeq       = B.CompanySeq
    --                                                              AND C.ServiceCenterSeq = B.ServiceCenterSeq
    --     WHERE A.CompanySeq = @CompanySeq 
    --       AND A.MajorSeq   = 100116
    --       AND A.MinorName  LIKE @Keyword
    --       AND (@Param1 = '' OR B.ServiceCenterSeq = @Param1)
    --       AND (@IsDefault = '0' OR (@IsDefault ='1' AND C.EmpSeq = @EmpSeq))


    --END

    --ELSE 
    --BEGIN

    --    SELECT A.MinorName AS UMArea,
    --           B.UMAreaSeq
    --      FROM _TDAUMinor             AS A WITH(NOLOCK)
    --           JOIN _TSLASArea        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
    --                                                   AND B.UMAreaSeq        = A.MinorSeq        
    --           JOIN _TSLServiceCenter AS C WITH(NOLOCK) ON C.CompanySeq       = B.CompanySeq
    --                                                   AND C.ServiceCenterSeq = B.ServiceCenterSeq
    --     WHERE A.CompanySeq = @CompanySeq 
    --       AND A.MajorSeq   = 100116
    --       AND C.CustSeq    = @CustSeq
    --       AND A.MinorName  LIKE @Keyword
    --       AND (@Param1 = '' OR B.ServiceCenterSeq = @Param1)


    --END

RETURN
