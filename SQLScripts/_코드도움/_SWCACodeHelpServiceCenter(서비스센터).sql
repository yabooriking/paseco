IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWCACodeHelpServiceCenter') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWCACodeHelpServiceCenter
GO

/********************************
 설    명 - 서비스센터 코드도움 SP
 작 성 일 - 2020년 05월 08일
 작 성 자 - 김재호
 수정내역 - 
*********************************/ 

CREATE PROC dbo._SWCACodeHelpServiceCenter
    @WorkingTag      NVARCHAR(1)             ,                        
    @LanguageSeq     INT                     ,                        
    @CodeHelpSeq     INT                     ,                        
    @DefQueryOption  INT                     , -- 2: direct search                        
    @CodeHelpType    TINYINT                 ,                        
    @PageCount       INT               = 20  ,             
    @CompanySeq      INT               = 1   ,     
    @Keyword         NVARCHAR(100)     = ''  ,                        
    @Param1          NVARCHAR(50)      = ''  ,            
    @Param2          NVARCHAR(50)      = ''  ,            
    @Param3          NVARCHAR(50)      = ''  ,            
    @Param4          NVARCHAR(50)      = ''  ,
    --@SubConditionSql NVARCHAR(500)     ='1=1',
    @AccUnit         INT               = 0   , 
    @BizUnit         INT               = 0   , 
    @FactUnit        INT               = 0   , 
    @DeptSeq         INT               = 0   , 
    @WkDeptSeq       INT               = 0   , 
    @EmpSeq          INT               = 0   ,
    @UserSeq         INT               = 0
AS

	SET ROWCOUNT @PageCount

    DECLARE @UserType  INT           ,
            @CustSeq   INT           , 
            @IsDefault NCHAR(1) = '0' -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터만 조회 가능 

    SELECT @UserType = ISNULL(UserType, 0),
           @CustSeq  = ISNULL(CustSeq,0 ) 
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq 


    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              )
    BEGIN

        SET @IsDefault = '1'


    END

        SELECT A.ServiceCenterName, 
		       A.ServiceCenterSeq 
	      FROM _TSLServiceCenter AS A WITH(NOLOCK)
	     WHERE A.CompanySeq        = @CompanySeq
           AND A.ServiceCenterName LIKE @Keyword
           AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND A.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND A.CustSeq = @CustSeq))

    ---- 서비스센터의 담당자로 지정되지 않은 내부사용자
    --IF @UserType = 1001
    --BEGIN
    --    SELECT A.ServiceCenterName, 
		  --     A.ServiceCenterSeq 
	   --   FROM _TSLServiceCenter AS A WITH(NOLOCK)
	   --  WHERE A.CompanySeq        = @CompanySeq
    --       AND A.ServiceCenterName LIKE @Keyword
    --       AND (@IsDefault = '0' OR (@IsDefault ='1' AND A.EmpSeq = @EmpSeq))
    --END

    ---- 외부사용자
    --ELSE
    --BEGIN

    --    SELECT A.ServiceCenterName, 
		  --     A.ServiceCenterSeq 
	   --   FROM _TSLServiceCenter AS A WITH(NOLOCK)       
	   --  WHERE A.CompanySeq        = @CompanySeq
    --       AND A.CustSeq           = @CustSeq
    --       AND A.ServiceCenterName LIKE @Keyword


    --END


RETURN