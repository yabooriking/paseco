IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWCACodeHelpASClient') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWCACodeHelpASClient
GO

/********************************
 설    명 - 서비스고객 코드도움 SP
 작 성 일 - 2020년 05월 18일
 작 성 자 - 김재호
 수정내역 - 
*********************************/ 

CREATE PROC dbo._SWCACodeHelpASClient
    @WorkingTag      NVARCHAR(1)             ,                        
    @LanguageSeq     INT                     ,                        
    @CodeHelpSeq     INT                     ,                        
    @DefQueryOption  INT                     , -- 2: direct search                        
    @CodeHelpType    TINYINT                 ,                        
    @PageCount       INT               = 20  ,             
    @CompanySeq      INT               = 1   ,     
    @Keyword         NVARCHAR(100)     = ''  ,                        
    @Param1          NVARCHAR(50)      = ''  ,            
    @Param2          NVARCHAR(50)      = ''  ,            
    @Param3          NVARCHAR(50)      = ''  ,            
    @Param4          NVARCHAR(50)      = ''  ,
   --@SubConditionSql NVARCHAR(500)     ='1=1', -- 로그인정보를 사용하며, 하부조건을 사용하지 않을 경우 이 파라미터는 주석처리
    @AccUnit         INT               = 0   , 
    @BizUnit         INT               = 0   , 
    @FactUnit        INT               = 0   , 
    @DeptSeq         INT               = 0   , 
    @WkDeptSeq       INT               = 0   , 
    @EmpSeq          INT               = 0   ,
    @UserSeq         INT               = 0
AS

	SET ROWCOUNT @PageCount

    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    DECLARE @ClientSeq      INT           ,
            @UMSetDomain    NVARCHAR(200) ,
            @UMSetDomainSeq INT           ,
            @UserType       INT           , 
            @CustSeq        INT           ,
            @IsDefault      NCHAR(1) = '0' -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터의 관할지역에 해당하는 고객만 조회 가능 

    -- 사용자정의코드(이메일) 의 소분류값 - '직접입력'
    SELECT @UMSetDomain    = ISNULL(MinorName, ''),
           @UMSetDomainSeq = ISNULL(MinorSeq, 0)
      FROM _TDAUMinor AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.MinorSeq   = 100115001

    SELECT @UserType = ISNULL(UserType, 0),
           @CustSeq  = ISNULL(CustSeq, 0)
      FROM _TCAUSer AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq 
       AND A.UserSeq    = @UserSeq 


    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              ) OR @UserType = 1002 
    BEGIN

        SET @IsDefault = '1'


    END

    SELECT A.ClientName                              ,
           A.Email AS FullEmail                      ,
		   CASE WHEN B.MinorSeq IS NULL                                                          
		       THEN @UMSetDomain                                                                
			ELSE B.MinorName END AS UMSetDomain      ,
		   CASE WHEN B.MinorSeq IS NULL                                                          
		       THEN @UMSetDomainSeq                                                             
			ELSE B.MinorSeq END AS UMSetDomainSeq    ,
		   A.Address                                 ,
           C.MinorName AS UMArea                     ,
           A.UMAreaSeq                               ,
		   A.ClientSeq                               ,
           SUBSTRING(A.Email, 0, CHARINDEX('@', A.Email) ) AS Email                              , -- 이메일 아이디 
		   SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email)) AS Domain               , -- 이메일 도메인
           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		   ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	  FROM _TSLASClient                       AS A WITH(NOLOCK)
	        LEFT OUTER JOIN _TDAUMinor        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
										         		       AND B.MajorSeq         = 100115
										         		       AND B.MinorName        = SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email))
	        LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
										         		       AND C.MinorSeq         = A.UMAreaSeq
            LEFT OUTER JOIN _TSLASArea        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq
                                                               AND D.UMAreaSeq        = A.UMAreaSeq
            LEFT OUTER JOIN _TSLServiceCenter AS E WITH(NOLOCK) ON E.CompanySeq       = D.CompanySeq
                                                               AND E.ServiceCenterSeq = D.ServiceCenterSeq
	 WHERE A.CompanySeq = @CompanySeq 
       AND CASE @DefQueryOption WHEN 1 THEN A.ClientName WHEN 2 THEN ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') END LIKE @KeyWord 
       AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND E.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND E.CustSeq = @CustSeq))
       --AND (@IsDefault = '0' OR (@IsDefault ='1' AND E.EmpSeq = @EmpSeq))

    --IF @UserType = 1001 
    --BEGIN

    --    SELECT A.ClientName                              ,
    --           A.Email AS FullEmail                      ,
		  --     CASE WHEN B.MinorSeq IS NULL                                                          
		  --          THEN @UMSetDomain                                                                
				--    ELSE B.MinorName END AS UMSetDomain  ,
		  --     CASE WHEN B.MinorSeq IS NULL                                                          
		  --          THEN @UMSetDomainSeq                                                             
				--    ELSE B.MinorSeq END AS UMSetDomainSeq,
		  --     A.Address                                 ,
    --           C.MinorName AS UMArea                     ,
		  --     A.ClientSeq                               ,
    --           SUBSTRING(A.Email, 0, CHARINDEX('@', A.Email) ) AS Email                              , -- 이메일 아이디 
		  --     SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email)) AS Domain               , -- 이메일 도메인
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	   --   FROM _TSLASClient                      AS A WITH(NOLOCK)
		  --     LEFT OUTER JOIN _TDAUMinor        AS B WITH(NOLOCK) ON B.CompanySeq        = A.CompanySeq 
				--							         		      AND B.MajorSeq          = 100115
				--							         		      AND B.MinorName         = SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email))
		  --     LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq        = A.CompanySeq 
				--							         		      AND C.MinorSeq          = A.UMAreaSeq
    --           LEFT OUTER JOIN _TSLASArea        AS D WITH(NOLOCK) ON D.CompanySeq        = A.CompanySeq
    --                                                              AND D.UMAreaSeq         = A.UMAreaSeq
    --           LEFT OUTER JOIN _TSLServiceCenter AS E WITH(NOLOCK) ON E.CompanySeq        = D.CompanySeq
    --                                                              AND E.ServiceCenterSeq  = D.ServiceCenterSeq
	   --  WHERE A.CompanySeq = @CompanySeq 
    --       AND CASE @DefQueryOption WHEN 1 THEN A.ClientName WHEN 2 THEN ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') END LIKE @KeyWord 
    --       AND (@IsDefault = '0' OR (@IsDefault ='1' AND E.EmpSeq = @EmpSeq))
           

    --END

    --ELSE
    --BEGIN

    --    SELECT A.ClientName                              ,
    --           A.Email AS FullEmail                      ,
		  --     CASE WHEN B.MinorSeq IS NULL                                                          
		  --          THEN @UMSetDomain                                                                
				--    ELSE B.MinorName END AS UMSetDomain  ,
		  --     CASE WHEN B.MinorSeq IS NULL                                                          
		  --          THEN @UMSetDomainSeq                                                             
				--    ELSE B.MinorSeq END AS UMSetDomainSeq,
		  --     A.Address                                 ,
    --           C.MinorName AS UMArea                     ,
		  --     A.ClientSeq                               ,
    --           SUBSTRING(A.Email, 0, CHARINDEX('@', A.Email) ) AS Email                              , -- 이메일 아이디 
		  --     SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email)) AS Domain               , -- 이메일 도메인
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLASClient', 'TelNo', @CompanySeq), '') AS TelNo
	   --   FROM _TSLASClient                      AS A WITH(NOLOCK)
		  --     LEFT OUTER JOIN _TDAUMinor        AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
				--							         		      AND B.MajorSeq         = 100115
				--							         		      AND B.MinorName        = SUBSTRING(A.Email, CHARINDEX('@', A.Email) + 1, LEN(A.Email))
		  --     LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
				--									              AND C.MinorSeq         = A.UMAreaSeq
    --           LEFT OUTER JOIN _TSLASArea        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq
    --                                                              AND D.UMAreaSeq        = A.UMAreaSeq
    --           LEFT OUTER JOIN _TSLServiceCenter AS E WITH(NOLOCK) ON E.CompanySeq       = D.CompanySeq
    --                                                              AND E.ServiceCenterSeq = D.ServiceCenterSeq

	   --  WHERE A.CompanySeq = @CompanySeq 
    --       AND E.CustSeq    = @CustSeq
    --       AND CASE @DefQueryOption WHEN 1 THEN A.ClientName WHEN 2 THEN ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLASClient', 'Phone', @CompanySeq), '') END LIKE @KeyWord 


    --END

RETURN

