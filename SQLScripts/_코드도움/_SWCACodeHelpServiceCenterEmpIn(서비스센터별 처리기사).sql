IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWCACodeHelpServiceCenterEmpIn') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWCACodeHelpServiceCenterEmpIn
GO

/********************************
 설    명 - 서비스센터별 처리기사 코드도움 SP
 작 성 일 - 2020년 05월 18일
 작 성 자 - 김재호
 수정내역 - 
*********************************/ 

CREATE PROC dbo._SWCACodeHelpServiceCenterEmpIn
    @WorkingTag      NVARCHAR(1)             ,                        
    @LanguageSeq     INT                     ,                        
    @CodeHelpSeq     INT                     ,                        
    @DefQueryOption  INT                     , -- 2: direct search                        
    @CodeHelpType    TINYINT                 ,                        
    @PageCount       INT               = 20  ,             
    @CompanySeq      INT               = 1   ,     
    @Keyword         NVARCHAR(100)     = ''  ,                        
    @Param1          NVARCHAR(50)      = ''  ,            
    @Param2          NVARCHAR(50)      = ''  ,            
    @Param3          NVARCHAR(50)      = ''  ,            
    @Param4          NVARCHAR(50)      = ''  ,
   --@SubConditionSql NVARCHAR(500)     ='1=1', -- 로그인정보를 사용하며, 하부조건을 사용하지 않을 경우 이 파라미터는 주석처리
    @AccUnit         INT               = 0   , 
    @BizUnit         INT               = 0   , 
    @FactUnit        INT               = 0   , 
    @DeptSeq         INT               = 0   , 
    @WkDeptSeq       INT               = 0   , 
    @EmpSeq          INT               = 0   ,
    @UserSeq         INT               = 0
AS

	SET ROWCOUNT @PageCount

    DECLARE @UserType  INT           ,
            @CustSeq   INT           , 
            @IsDefault NCHAR(1) = '0' -- 외부사용자이거나, 서비스센터의 담당자로 등록된 내부사용자의 경우 = '1' , 본인이 소속된 서비스센터의 처리기사만 조회 가능 

    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    SELECT @UserType = ISNULL(UserType, 0),
           @CustSeq  = ISNULL(Custseq, 0) 
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE A.CompanySeq = @CompanySeq
       AND A.UserSeq    = @UserSeq 

    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                WHERE A.CompanySeq = @CompanySeq
                  AND A.EmpSeq     = @EmpSeq
              ) OR @UserType = 1002 
    BEGIN

        SET @IsDefault = '1'


    END

    SELECT B.ServiceCenterName     ,
           A.ServiceCenterSeq      ,
           A.ASEmpName             , 
           A.ASEmpSeq              ,
           C.MinorName AS UMJdName ,
           A.UMJdSeq               ,
           D.MinorName AS SMSexName,
           A.SMSexSeq              , 
           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq), '') AS Phone,
		   ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLServiceCenterEmpIn', 'TelNo', @CompanySeq), '') AS TelNo

      FROM _TSLServiceCenterEmpIn            AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TSLServiceCenter AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
                                                              AND B.ServiceCenterSeq = A.ServiceCenterSeq 
           LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
                                                              AND C.MinorSeq         = A.UMJdSeq 
           LEFT OUTER JOIN _TDASMinor        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq 
                                                              AND D.MinorSeq         = A.SMSexSeq 
     WHERE A.CompanySeq = @CompanySeq
       AND A.ASEmpName  LIKE @Keyword
       AND (@Param1 = '' OR A.ServiceCenterSeq = @Param1)
       AND ((@UserType = 1001 AND @IsDefault ='0') OR (@UserType = 1001 AND @IsDefault ='1' AND B.EmpSeq = @EmpSeq ) OR (@UserType = 1002 AND B.CustSeq = @CustSeq))

    --IF @UserType = 1001
    --BEGIN

    --    SELECT B.ServiceCenterName     ,
    --           A.ASEmpName             , 
    --           A.ASEmpSeq              ,
    --           C.MinorName AS UMJdName ,
    --           A.UMJdSeq               ,
    --           D.MinorName AS SMSexName,
    --           A.SMSexSeq              , 
    --           A.Remark                , 
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLServiceCenterEmpIn', 'TelNo', @CompanySeq), '') AS TelNo

    --      FROM _TSLServiceCenterEmpIn            AS A WITH(NOLOCK)
    --           LEFT OUTER JOIN _TSLServiceCenter AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
    --                                                              AND B.ServiceCenterSeq = A.ServiceCenterSeq 
    --           LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
    --                                                              AND C.MinorSeq         = A.UMJdSeq 
    --           LEFT OUTER JOIN _TDASMinor        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq 
    --                                                              AND D.MinorSeq         = A.SMSexSeq 
    --     WHERE A.CompanySeq = @CompanySeq
    --       AND A.ASEmpName  LIKE @Keyword
    --       AND (@Param1 = '' OR A.ServiceCenterSeq = @Param1)
    --       AND (@IsDefault = '0' OR (@IsDefault ='1' AND B.EmpSeq = @EmpSeq))


    --END

    --ELSE
    --BEGIN

    --    SELECT B.ServiceCenterName     ,
    --           A.ASEmpName             , 
    --           A.ASEmpSeq              ,
    --           C.MinorName AS UMJdName ,
    --           A.UMJdSeq               ,
    --           D.MinorName AS SMSexName,
    --           A.SMSexSeq              , 
    --           A.Remark                , 
    --           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq), '') AS Phone,
		  --     ISNULL(dbo._FWCOMDecrypt(A.TelNo, N'_TSLServiceCenterEmpIn', 'TelNo', @CompanySeq), '') AS TelNo

    --      FROM _TSLServiceCenterEmpIn            AS A WITH(NOLOCK)
    --           LEFT OUTER JOIN _TSLServiceCenter AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
    --                                                              AND B.ServiceCenterSeq = A.ServiceCenterSeq 
    --           LEFT OUTER JOIN _TDAUMinor        AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
    --                                                              AND C.MinorSeq         = A.UMJdSeq 
    --           LEFT OUTER JOIN _TDASMinor        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq 
    --                                                              AND D.MinorSeq         = A.SMSexSeq 
    --     WHERE A.CompanySeq = @CompanySeq
    --       AND B.CustSeq    = @CustSeq
    --       AND A.ASEmpName  LIKE @Keyword
    --       AND (@Param1 = '' OR A.ServiceCenterSeq = @Param1)
    --       AND (@IsDefault = '0' OR (@IsDefault ='1' AND B.EmpSeq = @EmpSeq))


    --END




RETURN

