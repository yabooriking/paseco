begin tran

INSERT INTO _TWCOMEncryptInfo
SELECT 1,(SELECT MAX(Seq) FROM _TWCOMEncryptInfo where companyseq = 1) + 1 , '_TSLASClient', 'Phone', 1, getdate()
UNION 
SELECT 1,(SELECT MAX(Seq) FROM _TWCOMEncryptInfo where companyseq = 1) + 2 , '_TSLASClient', 'TelNo', 1, getdate()
UNION
SELECT 1,(SELECT MAX(Seq) FROM _TWCOMEncryptInfo where companyseq = 1) + 1 , '_TSLServiceCenterEmpIn', 'Phone', 1, getdate()

select * from _TWCOMEncryptInfo where TableName = '_TSLASClient'

rollback tran 

--begin tran
--	delete from _TWCOMEncryptInfo where companyseq = 1 and seq in (196,197)
--rollback tran 

GetINsertScriptTableNULL '', '_TWCOMEncryptInfo'


SELECT 'INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime)' + CHAR(13) + 'SELECT '  + CONVERT(VARCHAR(30), ISNULL(CompanySeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(Seq, 0)) + ','   + 'N''' + ISNULL(RTRIM(REPLACE(TableName, '''', '''''')), '') + ''','   + 'N''' + ISNULL(RTRIM(REPLACE(ColumnName, '''', '''''')), '') + ''','   + CASE WHEN LastUserSeq IS NULL THEN 'NULL, ' ELSE CONVERT(VARCHAR(30), LastUserSeq) + ',' END   + CASE WHEN LastDateTime IS NULL THEN 'NULL ' ELSE '''' + CONVERT(VARCHAR(30), LastDateTime, 121) + '''' END   FROM _TWCOMEncryptInfo
--where seq in (196,197,198)


INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,196,N'_TSLASClient',N'Phone',1,'2020-05-12 15:01:58.953'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,197,N'_TSLASClient',N'TelNo',1,'2020-05-12 15:01:58.953'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,198,N'_TSLServiceCenterEmpIn',N'Phone',1,'2020-05-13 11:32:31.480'



---- 아래의 Query 참고 
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime)
SELECT A.CompanySeq,196,N'_TSLASClient',N'Phone',1,'2020-05-12 15:01:58.953'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 196)
 UNION 
SELECT A.CompanySeq,197,N'_TSLASClient',N'TelNo',1,'2020-05-12 15:01:58.953'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 197)
 UNION 
SELECT A.CompanySeq,198,N'_TSLServiceCenterEmpIn',N'Phone',1,'2020-05-13 11:32:31.480'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 198)
