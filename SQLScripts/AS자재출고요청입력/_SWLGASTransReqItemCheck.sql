IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqItemCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqItemCheck
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-품목체크   
 작성일 - 2020.04.22
 작성자 - 유태우 
*************************************************************************************************/ 

CREATE PROC _SWLGASTransReqItemCheck  
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0    
AS 
        
    DECLARE @Count            INT          ,    
            @MaxSerl          INT          ,    
            @MessageType      INT          ,    
            @Status           INT          ,    
            @Results          NVARCHAR(250),  
            @TableSeq         INT          ,
            @GoodQtyDecLength INT       
     
    -----------------------------------------    
    -- 진행여부체크(MASTER)
    -----------------------------------------   
     
    EXEC dbo._SWCOMProgressCheck  @CompanySeq, '_TLGASTransReqItem', 2,  '#BIZ_OUT_DataBlock2', 'ReqSeq', 'ReqSerl', '', 'Status'    

    EXEC dbo._SWCOMMessage @MessageType OUTPUT,    
                           @Status      OUTPUT,    
                           @Results     OUTPUT,    
                           1044               , -- 다음 작업이 진행되어서 변경,삭제할 수 없습니다..(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1044)    
                           @LanguageSeq       ,     
                           0,''                 -- SELECT * FROM _TCADictionary WHERE Word like '%단위%'    

    UPDATE #BIZ_OUT_DataBlock2    
       SET Result        = @Results    ,    
           MessageType   = @MessageType,    
           Status        = @Status    
      FROM #BIZ_OUT_DataBlock2 AS A    
     WHERE A.WorkingTag IN ('U','D')    
       AND A.Status = 1   


     EXEC dbo._SWCOMMessage @MessageType OUTPUT ,    
                           @Status      OUTPUT ,    
                           @Results     OUTPUT ,    
                           1285                , -- @1으로(로) @2된 자료는 @3할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1285)    
                           @LanguageSeq        ,
                           2585 , N'거래명세서', -- SELECT * FROM _TCADictionary WHERE WordSeq = 2585
                           1550 , N'생성'      , -- SELECT * FROM _TCADictionary WHERE WordSeq = 1550
                           61117, N'수정/삭제'   -- SELECT * FROM _TCADictionary WHERE WordSeq = 61117

    UPDATE #BIZ_OUT_DataBlock2    
       SET Result      = @Results    ,     
           MessageType = @MessageType,    
           Status      = @Status    
      FROM #BIZ_OUT_DataBlock2           AS A
           JOIN _TSLInvoiceCommitReqLink AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                          AND B.ReqSeq     = A.ReqSeq
     WHERE A.Status       = 0
       AND A.InOutReqKind = 8023005 -- 위탁출고요청


    EXEC dbo._SWCOMMessage @MessageType OUTPUT ,    
                           @Status      OUTPUT ,    
                           @Results     OUTPUT ,    
                           1285                , -- @1으로(로) @2된 자료는 @3할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1285)    
                           @LanguageSeq        ,
                           2585 , N'거래명세서', -- SELECT * FROM _TCADictionary WHERE WordSeq = 2585
                           1550 , N'생성'      , -- SELECT * FROM _TCADictionary WHERE WordSeq = 1550
                           61117, N'수정/삭제'   -- SELECT * FROM _TCADictionary WHERE WordSeq = 61117

    UPDATE #BIZ_OUT_DataBlock2    
       SET Result      = @Results    ,     
           MessageType = @MessageType,    
           Status      = @Status    
      FROM #BIZ_OUT_DataBlock2           AS A
           JOIN _TSLInvoiceCommitReqLink AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                          AND B.ReqSeq     = A.ReqSeq
     WHERE A.Status       = 0
       AND A.InOutReqKind = 8023005 -- 위탁출고요청
  
     EXEC @GoodQtyDecLength  = dbo._SWCOMEnvR @CompanySeq, 8, @UserSeq, @@PROCID -- 판매/제품 소수점자리수

     UPDATE #BIZ_OUT_DataBlock2    
        SET STDQty = ROUND((CASE WHEN ISNULL(B.ConvDen,0) = 0 THEN 0 ELSE ISNULL(A.Qty,0) * ISNULL(B.ConvNum,0) / ISNULL(B.ConvDen,0) END), @GoodQtyDecLength)  
       FROM #BIZ_OUT_DataBlock2 AS A    
            JOIN _TDAItemUnit   AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq    
                                                 AND B.ItemSeq    = A.ItemSeq    
                                                 AND B.UnitSeq    = A.UnitSeq    
            JOIN _TDAItemStock  AS C WITH(NOLOCK) ON C.CompanySeq = @CompanySeq    
                                                 AND C.ItemSeq    = A.ItemSeq    
      WHERE A.Status = 0    
        AND ISNULL(A.Qty,0) <> 0    
        AND ISNULL(C.IsQtyChange,'') <> '1'    
      
    SELECT @Count = COUNT(1) FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A' --@Count값수정(AND Status = 0 제외)  
       
    IF @Count > 0    
    BEGIN   
       
        -- 키값생성코드부분 시작      
        SELECT @MaxSerl = ISNULL(( SELECT MAX(A.ReqSerl)    
                                 FROM _TLGASTransReqItem AS A WITH(NOLOCK)   
                                WHERE A.CompanySeq = @CompanySeq    
                                  AND A.ReqSeq     IN ( SELECT ReqSeq  
                                                          FROM #BIZ_OUT_DataBlock2    
                                                         WHERE ReqSeq = A.ReqSeq
                                                      )
                             ), 0)    
    
        -- Temp Table 에 생성된 키값 UPDATE    
        UPDATE #BIZ_OUT_DataBlock2    
           SET ReqSerl = @MaxSerl + A.DataSeq  
          FROM #BIZ_OUT_DataBlock2 AS A   
         WHERE A.WorkingTag = 'A'    
           AND A.Status     = 0  
           
             
    END      
       
 RETURN    
