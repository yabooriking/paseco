IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqItemSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqItemSave
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-품목저장
 작성일 - 2020.04.22
 작성자 - 유태우
*************************************************************************************************/ 
 
CREATE PROC _SWLGASTransReqItemSave  
    @ServiceSeq     INT     = 0,
    @WorkingTag     NVARCHAR(10)= '',
    @CompanySeq     INT     = 1,
    @LanguageSeq    INT     = 1,
    @UserSeq        INT     = 0,
    @PgmSeq         INT     = 0,
    @IsTransaction  BIT     = 0      

AS    
   
    EXEC _SCOMXMLLog @CompanySeq          ,
                     @UserSeq             ,                             
                     '_TLGASTransReqItem' , -- 원테이블명
                     '#BIZ_OUT_DataBlock2', -- 템프테이블명
                     'ReqSeq, ReqSerl'    , -- 키가여러개일경우는, 로연결한다. 
                     @PgmSeq              ,
                     ''--여기에 템프키스트링이 들어갑니다..      
      
    -- DELETE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    

        DELETE _TLGASTransReqItem    
          FROM _TLGASTransReqItem       AS A    
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.ReqSeq  = A.ReqSeq    
                                            AND B.ReqSerl = A.ReqSerl  
         WHERE A.CompanySeq = @CompanySeq
           AND B.WorkingTag = 'D' 
           AND B.Status     = 0     
  
        IF @@ERROR <> 0 RETURN   


    END    
  
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN     
  
        UPDATE _TLGASTransReqItem    
           SET  InOutReqKind = ISNULL(A.InOutReqKind, 0),  
                ItemSeq      = ISNULL(A.ItemSeq, 0)     ,  
                LotNo        = ISNULL(A.LotNo, '')      ,  
                UnitSeq      = ISNULL(A.UnitSeq, 0)     ,  
                Qty          = ISNULL(A.Qty, 0)         ,  
                STDQty       = ISNULL(A.STDQty, 0)      ,  
                STDUnitSeq   = ISNULL(A.STDUnitSeq, 0)  ,
                Remark       = ISNULL(A.Remark, '')     ,  
                LastUserSeq  = @UserSeq                ,  
                LastDateTime = GETDATE()               ,  
                PgmSeq       = @PgmSeq  
          FROM #BIZ_OUT_DataBlock2 AS A    
               JOIN _TLGASTransReqItem AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq    
                                                        AND B.ReqSeq     = A.ReqSeq    
                                                        AND B.ReqSerl    = A.ReqSerl  
  
         WHERE A.WorkingTag = 'U' 
           AND A.Status     = 0  
     
        IF @@ERROR <> 0 RETURN   
           
  
    END    
       
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TLGASTransReqItem(CompanySeq  , ReqSeq      , ReqSerl     , InOutReqKind, ItemSeq      ,
                                       UnitSeq     , Qty         , STDQty      , STDUnitSeq  , LotNo        ,
                                       Remark      , LastUserSeq , LastDateTime, Pgmseq)       
        SELECT @CompanySeq , ReqSeq      , ReqSerl     , InOutReqKind , ItemSeq      ,
               UnitSeq     , Qty         , STDQty      , STDUnitSeq   , LotNo        ,
               Remark      , @UserSeq    , GETDATE()   , @PgmSeq
          FROM #BIZ_OUT_DataBlock2 A    
         WHERE WorkingTag = 'A' AND Status = 0  
    
        IF @@ERROR <> 0 RETURN   
        
          
    END        
      
RETURN    
