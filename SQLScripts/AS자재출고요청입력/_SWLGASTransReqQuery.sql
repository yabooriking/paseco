IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqQuery
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-Master조회    
 작성일 - 2020-04-22
 작성자 - 유태우 
*************************************************************************************************/ 
        
CREATE PROCEDURE _SWLGASTransReqQuery      
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0  
AS           

    DECLARE @ReqSeq INT          

    SELECT @ReqSeq = ISNULL(ReqSeq, 0)
      FROM #BIZ_IN_DataBlock1

    SELECT  ISNULL(A.ReqSeq, 0)             AS ReqSeq                ,
            ISNULL(B.BizUnitName, '')       AS BizUnitName           ,  
            ISNULL(A.BizUnit, 0)            AS BizUnit               ,
            ISNULL(C.BizUnitName, '')       AS TransBizUnitName      ,  
            ISNULL(A.TransBizUnit, 0)       AS TransBizUnit          ,  
            ISNULL(A.ReqNo, '')             AS ReqNo                 ,   
            ISNULL(D.EmpName, '')           AS EmpName               ,  
            ISNULL(A.EmpSeq, 0)             AS EmpSeq                ,  
            ISNULL(A.ReqDate, '')           AS ReqDate               , 
            ISNULL(A.CompleteWishDate, '')  AS CompleteWishDate      ,  
            ISNULL(E.WHName, '')            AS InWHName              ,  
            ISNULL(A.InWHSeq, 0)            AS InWHSeq               ,
            ISNULL(A.Remark, '')            AS Remark                ,
            ISNULL(A.IsAuto, '0')           AS IsAuto                ,
            ISNULL(A.IsTrans, '0')          AS IsTrans               ,
            ISNULL(A.InOutReqType, '')      AS InOutReqType          ,
            ISNULL(A.InOutReqDetailKind, 0) AS InOutReqDetailKind    , -- 요청구분코드
            ISNULL(F.MinorName, '')         AS InOutReqDetailKindName  -- 요청구분
     FROM _TLGASTransReq               AS A WITH(NOLOCK) 
           LEFT OUTER JOIN _TDABizUnit AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                        AND B.BizUnit    = A.BizUnit
           LEFT OUTER JOIN _TDABizUnit AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq 
                                                        AND C.BizUnit    = A.TransBizUnit
           LEFT OUTER JOIN _TDAEmp     AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq 
                                                        AND D.EmpSeq     = A.EmpSeq
           LEFT OUTER JOIN _TDAWH      AS E WITH(NOLOCK) ON E.CompanySeq = A.CompanySeq 
                                                        AND E.WHSeq      = A.InWHSeq
           LEFT OUTER JOIN _TDAUMinor  AS F WITH(NOLOCK) ON F.CompanySeq = A.CompanySeq 
                                                        AND F.MinorSeq   = A.InOutReqDetailKind 
    WHERE A.CompanySeq = @CompanySeq  
      AND A.ReqSeq     = @ReqSeq 
      --AND EXISTS (SELECT TOP 1 1 FROM _TLGInOutReqItem WITH(NOLOCK) WHERE CompanySeq = A.CompanySeq AND ReqSeq = A.ReqSeq)  

  
RETURN      



