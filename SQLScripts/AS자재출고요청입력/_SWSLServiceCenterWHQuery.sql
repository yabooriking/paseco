IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterWHQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterWHQuery
GO

/********************************
 설    명 - AS자재출고 요청 INIT 에서 사용자에게 맵핑되어 있는 서비스센터 창고 가져오기
 작 성 일 - 2020년 04월 21일
 작 성 자 - 유태우
 수정내역 - 2020.05.14 김재호 : 본사반납창고, 본사반납창고의 사업부문, 내/외부 사용자 별 담당자 및  처리부서
                                내부사용자 - 담당자 = 본인, 처리부서 = 본인 소속 부서
                                외부사용자 - 담당자 = [서비스센터등록] 화면에서 입력한 담당자, 처리부서 = 담당자의 부서  
*********************************/

-- SP파라미터들
CREATE PROC dbo._SWSLServiceCenterWHQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    -- 사용할 변수를 선언한다.
    DECLARE @EmpName   NVARCHAR(100),
            @EmpSeq    INT          , 
            @DeptName  NVARCHAR(100),
            @DeptSeq   INT          , 
            @UserName  NVARCHAR(100),
            @UserType  INT          ,
            @IsDefault NCHAR(1) = 0    -- 로그인계정이 외부사용자계정일 경우나, 서비스센터 담당자에 매핑되어있는 계정인 경우 서비스센터와 접수자를 화면 오픈 시 세팅하고 DIS 처리 하기위한 플래그값 
            
    --내,외부 사용자 코드
    SELECT @EmpName   = ISNULL(B.EmpName, '') ,
           @EmpSeq    = ISNULL(A.EmpSeq , 0)  ,
           @DeptName  = ISNULL(C.DeptName, ''),
           @DeptSeq   = ISNULL(A.DeptSeq , 0) ,
           @UserName  = ISNULL(A.UserName, ''),
           @UserType  = ISNULL(Usertype, 0)
      FROM _TCAUser                  AS A WITH(NOLOCK)
            LEFT OUTER JOIN _TDAEmp  AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                      AND B.EmpSeq     = A.EmpSeq
            LEFT OUTER JOIN _TDADept AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq 
                                                      AND C.DeptSeq    = A.DeptSeq 
     WHERE A.Companyseq = @Companyseq 
       AND A.UserSeq    = @UserSeq


    IF EXISTS (SELECT 1 
                 FROM _TSLServiceCenter AS A WITH(NOLOCK)
                      JOIN _TCAUser     AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                         AND B.EmpSeq     = A.EmpSeq 
                WHERE A.CompanySeq = @CompanySeq
                  AND B.UserSeq    = @UserSeq
              )
    BEGIN

        SET @IsDefault = '1'


    END


    IF @UserType = 1001 -- 내부사용자 : 서비스센터등록 화면에서 담당자로 지정되어 있는 서비스센터창고
    BEGIN

          SELECT A.ServiceCenterWHSeq AS ServiceCenterWHSeq       ,
                 B.WHName             AS ServiceCenterWHName      ,
                 B.BizUnit            AS ServiceCenterBizUnit     ,
                 B.BizUnit            AS ServiceCenterTransBizUnit,
                 A.ReturnWHSeq        AS ReturnWHSeq              , 
                 C.WHName             AS ReturnWHName             ,
                 C.BizUnit            AS ReturnBizUnit            ,
                 C.BizUnit            AS ReturnTransBizUnit       ,
                 @EmpName             AS EmpName                  ,
                 @EmpSeq              AS EmpSeq                   , 
                 @DeptName            AS DeptName                 ,
                 @DeptSeq             AS DeptSeq                  ,
                 A.ServiceCenterName                              ,
                 A.ServiceCenterSeq                               ,
                 @UserName            AS UserName                 ,
                 @UserSeq             AS UserSeq                  ,
                 @IsDefault           AS IsDefault                      
            FROM _TSLServiceCenter      AS A WITH(NOLOCK)
                 LEFT OUTER JOIN _TDAWH AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                         AND B.WHSeq      = A.ServiceCenterWHSeq
                 LEFT OUTER JOIN _TDAWH AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq
                                                         AND C.WHSeq      = A.ReturnWHSeq
           WHERE A.CompanySeq = @CompanySeq
             AND A.EmpSeq     = @EmpSeq


    END

    ELSE IF @UserType = 1002 -- 외부사용자 : 외부사용자등록에 등록된 거래처와 서비스센터등록 화면에 등록된 거래처와 맵핑하여 서비스센터 창고 조회
    BEGIN

          SELECT A.ServiceCenterWHSeq  AS ServiceCenterWHSeq       ,
                 C.WHName              AS ServiceCenterWHName      ,
                 C.BizUnit             AS ServiceCenterBizUnit     ,
                 C.BizUnit             AS ServiceCenterTransBizUnit,
                 A.ReturnWHSeq         AS ReturnWHSeq              , 
                 D.WHName              AS ReturnWHName             ,
                 D.BizUnit             AS ReturnBizUnit            ,
                 D.BizUnit             AS ReturnTransBizUnit       ,
                 F.EmpName             AS EmpName                  ,
                 F.EmpSeq              AS EmpSeq                   , 
                 G.DeptName            AS DeptName                 ,
                 G.DeptSeq             AS DeptSeq                  ,
                 A.ServiceCenterName                               ,
                 A.ServiceCenterSeq                                ,
                 @UserName             AS UserName                 ,
                 @UserSeq              AS UserSeq                  ,
                 '1'                   AS IsDefault
            FROM _TSLServiceCenter        AS A WITH(NOLOCK)
                 LEFT OUTER JOIN _TCAUser AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq
                                                           AND B.CustSeq    = A.CustSeq
                 LEFT OUTER JOIN _TDAWH   AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq 
                                                           AND C.WHSeq      = A.ServiceCenterWHSeq
                 LEFT OUTER JOIN _TDAWH   AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq
                                                           AND D.WHSeq      = A.ReturnWHSeq
                 LEFT OUTER JOIN _TCAUser AS E WITH(NOLOCK) ON E.CompanySeq = A.CompanySeq 
                                                           AND E.EmpSeq     = A.EmpSeq 
                 LEFT OUTER JOIN _TDAEmp  AS F WITH(NOLOCK) ON F.CompanySeq = E.CompanySeq 
                                                           AND F.EmpSeq     = E.EmpSeq 
                 LEFT OUTER JOIN _TDADept AS G WITH(NOLOCK) ON G.CompanySeq = E.CompanySeq 
                                                           AND G.DeptSeq    = E.DeptSeq 
           WHERE A.CompanySeq = @CompanySeq
             AND B.UserSeq    = @UserSeq
    END

    ELSE
    BEGIN

          SELECT 0,0,0


    END

RETURN

