IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqSave
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-Master저장    
 작성일 - 2020.04.22
 작성자 - 유태우
*************************************************************************************************/ 

CREATE PROC _SWLGASTransReqSave  
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0     
AS    
        
     EXEC _SCOMXMLLog @CompanySeq          ,
                      @UserSeq             ,                             
                      '_TLGASTransReq'     , -- 원테이블명
                      '#BIZ_OUT_DataBlock1', -- 템프테이블명
                      'ReqSeq'             , -- 키가여러개일경우는, 로연결한다. 
                      @PgmSeq              ,
                      ''                     --여기에 템프키스트링이 들어갑니다..  

    -- DELETE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
          
        DELETE _TLGASTransReq    
          FROM _TLGASTransReq           AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ReqSeq = A.ReqSeq    
         WHERE A.CompanySeq = @CompanySeq
           AND B.WorkingTag = 'D' 
           AND B.Status     = 0     
  
        IF @@ERROR <> 0 RETURN
  

    END    
    
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN     
        UPDATE _TLGASTransReq    
           SET  BizUnit            = ISNULL(B.BizUnit, 0)           , 
                TransBizUnit       = ISNULL(B.TransBizUnit, 0)      , 
                EmpSeq             = ISNULL(B.EmpSeq, 0)            ,  
                ReqDate            = ISNULL(B.ReqDate, '')          ,
                CompleteWishDate   = ISNULL(B.CompleteWishDate, '') ,
                InWHSeq            = ISNULL(B.InWHSeq, 0)           ,
                InOutReqDetailKind = ISNULL(B.InOutReqDetailKind, 0),
                Remark             = ISNULL(B.Remark, '')           ,
                IsAuto             = ISNULL(B.IsAuto, '0')          ,
                IsTrans            = ISNULL(B.IsTrans, '0')         ,
                LastUserSeq        = @UserSeq                       ,  
                LastDateTime       = GETDATE()                      ,  
                PgmSeq             = @PgmSeq  
          FROM _TLGASTransReq           AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ReqSeq = A.ReqSeq    
         WHERE A.CompanySeq = @CompanySeq    
           AND B.WorkingTag = 'U' 
           AND B.Status     = 0  
     
        IF @@ERROR <> 0 RETURN


    END    
       
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0 )    
    BEGIN    
  
        INSERT INTO _TLGASTransReq(CompanySeq        , ReqSeq            , ReqNo             , BizUnit           , TransBizUnit     ,
                                   EmpSeq            , ReqDate           , CompleteWishDate  , InWHSeq           , IsTrans          ,
                                   IsAuto            , InOutReqType      , InOutReqDetailKind, Remark            , LastUserSeq      ,
                                   LastDateTime      , PgmSeq)  

        SELECT @CompanySeq       , ReqSeq           , ReqNo              , BizUnit            ,  TransBizUnit    ,
               EmpSeq            , ReqDate          , CompleteWishDate   , InWHSeq            ,  IsTrans         ,
               IsAuto            , InOutReqType     , InOutReqDetailKind , Remark             ,  @UserSeq        ,  
               GETDATE()         , @PgmSeq                 
          FROM #BIZ_OUT_DataBlock1  
         WHERE WorkingTag = 'A' AND Status = 0  
    
        IF @@ERROR <> 0 RETURN
        
          
    END        
RETURN    
