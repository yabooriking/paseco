IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqItemQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqItemQuery
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-품목조회    
 작성일 - 2020.04.22
 작성자 - 유태우
*************************************************************************************************/ 
      
CREATE PROCEDURE _SWLGASTransReqItemQuery      
    @ServiceSeq    INT          = 0 ,
    @WorkingTag    NVARCHAR(10) = '',
    @CompanySeq    INT          = 1 ,
    @LanguageSeq   INT          = 1 ,
    @UserSeq       INT          = 0 ,
    @PgmSeq        INT          = 0 ,
    @IsTransaction BIT          = 0     

AS           

    DECLARE @ReqSeq  INT,  
            @ReqSerl INT 
              
    SELECT @ReqSeq  = ISNULL(ReqSeq,0) ,
           @ReqSerl = ISNULL(ReqSerl,0)
      FROM #BIZ_IN_DataBlock2
  
     SELECT ISNULL(A.ReqSeq, 0)       AS ReqSeq      ,  
            ISNULL(A.ReqSerl, 0)      AS ReqSerl     ,  
            ISNULL(A.InOutReqKind, 0) AS InOutReqKind,  
            ISNULL(B.ItemName, '')    AS ItemName    ,  
            ISNULL(B.ItemNo, '')      AS ItemNo      ,  
            ISNULL(A.ItemSeq, 0)      AS ItemSeq     ,  
            ISNULL(A.LotNo, '')       AS LotNo       ,  
            ISNULL(N.UnitName,'')     AS UnitName    ,  
            ISNULL(A.UnitSeq, 0)      AS UnitSeq     ,  
            ISNULL(A.Qty, 0)          AS Qty         ,
            ISNULL(L.UnitName,'')     AS STDUnitName ,  
            ISNULL(B.UnitSeq, 0)      AS STDUnitSeq  ,  
            ISNULL(A.STDQty, 0)       AS STDQty      ,  
            ISNULL(A.Remark, 0)       AS Remark  
       FROM _TLGASTransReqItem       AS A WITH (NOLOCK) 
            JOIN _TLGASTransReq      AS F WITH (NOLOCK) ON F.CompanySeq = A.CompanySeq
                                                       AND F.ReqSeq     = A.ReqSeq
            LEFT OUTER JOIN _TDAItem AS B WITH (NOLOCK) ON A.CompanySeq = B.CompanySeq  
                                                       AND A.ItemSeq    = B.ItemSeq
            LEFT OUTER JOIN _TDAUnit AS N WITH (NOLOCK) ON N.CompanySeq = A.CompanySeq 
                                                       AND N.UnitSeq    = A.UnitSeq
            LEFT OUTER JOIN _TDAUnit AS L WITH (NOLOCK) ON L.CompanySeq = B.CompanySeq 
                                                       AND L.UnitSeq    = B.UnitSeq
      WHERE A.CompanySeq  = @CompanySeq 
        AND A.ReqSeq      = @ReqSeq
      ORDER BY A.ReqSerl  

RETURN      
