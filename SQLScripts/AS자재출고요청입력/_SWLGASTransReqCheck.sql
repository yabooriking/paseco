IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqCheck
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청입력-Master체크    
 작성일 - 20200422
 작성자 - 유태우
*************************************************************************************************/ 
  
CREATE PROC _SWLGASTransReqCheck    
    @ServiceSeq    INT          = 0 ,
    @WorkingTag    NVARCHAR(10) = '',
    @CompanySeq    INT          = 1 ,
    @LanguageSeq   INT          = 1 ,
    @UserSeq       INT          = 0 ,
    @PgmSeq        INT          = 0 ,
    @IsTransaction BIT          = 0      
    
AS        

    DECLARE @Seq         INT          ,    
            @MessageType INT          ,    
            @Status      INT          ,    
            @Results     NVARCHAR(250),    
            @BizUnit     INT          ,      
            @Date        NVARCHAR(8)  ,      
            @MaxNo       NVARCHAR(50) ,   
            @TableSeq    INT     
 
    -----------------------------------------    
    -- 진행여부체크(MASTER)
    -----------------------------------------    
    EXEC dbo._SWCOMProgressCheck  @CompanySeq, '_TLGASTransReqItem', 1, '#BIZ_OUT_DataBlock1', 'ReqSeq', '', '', 'Status'    

    EXEC dbo._SWCOMMessage @MessageType OUTPUT,    
                           @Status      OUTPUT,    
                           @Results     OUTPUT,    
                           1044               , -- 다음 작업이 진행되어서 변경,삭제할 수 없습니다..(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1044)    
                           @LanguageSeq       ,     
                           0,''                 -- SELECT * FROM _TCADictionary WHERE Word like '%단위%'    

    UPDATE #BIZ_OUT_DataBlock1     
       SET Result      = @Results    ,    
           MessageType = @MessageType,    
           Status      = @Status    
      FROM #BIZ_OUT_DataBlock1 AS A    
     WHERE A.WorkingTag IN ('U','D')    
       AND A.Status     = 1   
  
    -----------------------------------------    
    -- 요청일 > 입고희망일 체크 
    -----------------------------------------    
    EXEC dbo._SWCOMMessage @MessageType OUTPUT,
                           @Status      OUTPUT,
                           @Results     OUTPUT,
                           31                 ,    -- @1이 @2 보다 커야 합니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1)
                           @LanguageSeq       , 
                           1914, '입고희망일' ,    -- SELECT * FROM _TCADictionary WHERE Word like '%입고희망일%'
                           200, '요청일'           -- SELECT * FROM _TCADictionary WHERE Word like '%요청일%'

    UPDATE #BIZ_OUT_DataBlock1
       SET Result      = @Results    ,
           MessageType = @MessageType,
           Status      = @Status
      FROM #BIZ_OUT_DataBlock1 AS A
     WHERE A.WorkingTag IN ('A', 'U')
       AND A.Status     = 0
       AND ISNULL(A.CompleteWishDate ,'') <> ''
       AND A.ReqDate    > A.CompleteWishDate

    EXEC dbo._SWCOMMessage @MessageType OUTPUT ,    
                           @Status      OUTPUT ,    
                           @Results     OUTPUT ,    
                           1285                , -- @1으로(로) @2된 자료는 @3할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1285)    
                           @LanguageSeq        ,
                           2585 , N'거래명세서', -- SELECT * FROM _TCADictionary WHERE WordSeq = 2585
                           1550 , N'생성'      , -- SELECT * FROM _TCADictionary WHERE WordSeq = 1550
                           61117, N'수정/삭제'   -- SELECT * FROM _TCADictionary WHERE WordSeq = 61117

    UPDATE #BIZ_OUT_DataBlock1    
       SET Result      = @Results    ,     
           MessageType = @MessageType,    
           Status      = @Status    
      FROM #BIZ_OUT_DataBlock1           AS A
           JOIN _TSLInvoiceCommitReqLink AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                          AND B.ReqSeq     = A.ReqSeq
     WHERE A.InOutReqType = 50 -- 위탁출고요청
       AND A.Status       = 0
   
    -------------------------------------------      
    -- INSERT 번호부여(맨 마지막 처리)      
    -------------------------------------------      

    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0) 
    BEGIN        
        
        -- 불필요한 구문이라고 생각함
        SELECT @BizUnit = ISNULL(MAX(BizUnit), 0),      
               @Date    = ISNULL(MAX(ReqDate),REPLACE(CONVERT(CHAR(10),GETDATE(),121),'-',''))      
          FROM #BIZ_OUT_DataBlock1 AS A 
         WHERE A.WorkingTag = 'A' 
           AND A.Status     = 0      
    
        -- 키값생성코드부분 시작        
        EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TLGASTransReq', 'ReqSeq', 1  
            
        -- Temp Table 에 생성된 키값 UPDATE      
        UPDATE #BIZ_OUT_DataBlock1  
           SET ReqSeq = @Seq + 1
          FROM #BIZ_OUT_DataBlock1 AS A       
         WHERE A.WorkingTag = 'A'      
           AND A.Status     = 0      
    
        -- 번호생성코드부분 시작        
        EXEC dbo._SWCOMCreateNo 'SL', '_TLGASTransReq', @CompanySeq, 0, @Date, @MaxNo OUTPUT 
             
        -- Temp Talbe 에 생성된 키값 UPDATE      
        UPDATE #BIZ_OUT_DataBlock1    
           SET ReqNo = @MaxNo      
          FROM #BIZ_OUT_DataBlock1 AS A   
         WHERE A.WorkingTag = 'A'      
           AND A.Status     = 0   
           
              
    END       
           
RETURN        
