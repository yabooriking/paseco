IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASAreaQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASAreaQuery
GO

/********************************
 설    명 - 서비스관할지역등록:조회
 작 성 일 - 2020년 05월 12일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASAreaQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    DECLARE @ServiceCenterSeq INT          ,
	        @Area             NVARCHAR(200),
			@IsNotReg         NCHAR(1)

    SELECT @ServiceCenterSeq  = ISNULL(ServiceCenterSeq, 0), 
		   @Area              = ISNULL(Area, '')           ,
		   @IsNotReg          = ISNULL(IsNotReg, '0')        
	  FROM #BIZ_IN_DataBlock1

	SELECT A.MinorName AS Area    ,
	       C.ServiceCenterName    ,
		   B.Distance             ,
		   B.Remark               ,
		   B.ServiceCenterSeq     , 
		   A.MinorSeq AS UMAreaSeq 
	  FROM _TDAUMinor AS A WITH(NOLOCK)
	        LEFT OUTER JOIN _TSLASArea        AS B WITH(NOLOCK) ON B.CompanySeq        = A.CompanySeq
			                                                   AND B.UMAreaSeq         = A.MinorSeq 
	        LEFT OUTER JOIN _TSLServiceCenter AS C WITH(NOLOCK) ON C.CompanySeq        = B.CompanySeq
			                                                   AND C.ServiceCenterSeq  = B.ServiceCenterSeq 
	 WHERE A.CompanySeq = @CompanySeq 
	   AND A.MajorSeq   = 100116
	   AND (@ServiceCenterSeq = 0   OR B.ServiceCenterSeq = @ServiceCenterSeq)
	   AND A.MinorName  LIKE @Area + '%'
	   AND (@IsNotReg   = '0' OR (@IsNotReg = '1' AND ISNULL(B.Distance, 0) = 0))

RETURN

