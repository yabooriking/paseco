CREATE TABLE _TSLASArea (
    CompanySeq       INT           ,
    UMAreaSeq        INT           ,
    ServiceCenterSeq INT           ,
    Distance         DECIMAL(19, 5),
    Remark           NVARCHAR(400) ,
    PgmSeq           INT           ,
    LastUserSeq      INT           ,
    LastDateTime     DATETIME      )

CREATE CLUSTERED INDEX IDX_TSLASArea ON _TSLASArea (CompanySeq, UMAreaSeq)


IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 521070)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLASArea', 1, 521070, '1', 1, GETDATE()END

