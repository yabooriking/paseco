IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASAreaSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASAreaSave
GO

/********************************
 설    명 - 서비스관할지역등록:저장
 작 성 일 - 2020년 05월 12일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASAreaSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq             ,      
                      @UserSeq                ,      
                      '_TSLASArea',     
                      '#BIZ_OUT_DataBlock1'   , -- 템프테이블명      
                      'UMAreaSeq'             , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq
            
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
  
        -- DELETE  
        DELETE _TSLASArea    
          FROM _TSLASArea               AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.UMAreaSeq  = A.UMAreaSeq  
         WHERE A.CompanySeq = @CompanySeq  
		   AND B.WorkingTag = 'D' 
		   AND B.Status     = 0  
  
        IF @@ERROR <> 0 RETURN  
            
		   
    END  
  
    -- UPDATE & INSERT       
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  

       UPDATE _TSLASArea    
          SET ServiceCenterSeq = ISNULL(B.ServiceCenterSeq, 0),
			  Distance         = ISNULL(B.Distance , 0)       ,
              Remark           = ISNULL(B.Remark, '')  	      ,
			  PgmSeq           = @PgmSeq                      , 
              LastUserSeq      = @UserSeq                     ,
              LastDateTime     = GETDATE()  
         FROM _TSLASArea               AS A WITH (NOLOCK)   
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.UMAreaSeq = A.UMAreaSeq  
        WHERE A.CompanySeq = @CompanySeq   
		  AND B.WorkingTag = 'U' 
          AND B.Status     = 0  

        IF @@ERROR <> 0 RETURN  

       
    END   

  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLASArea(CompanySeq      , UMAreaSeq       , ServiceCenterSeq, Distance        , Remark          ,
                               PgmSeq          , LastUserSeq     , LastDateTime)   
        SELECT  @CompanySeq      , UMAreaSeq       , ServiceCenterSeq, Distance       , Remark       ,
                @PgmSeq          , @UserSeq        , GETDATE()														    
          FROM #BIZ_OUT_DataBlock1   
         WHERE WorkingTag = 'A' 
		   AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN  


    END

RETURN