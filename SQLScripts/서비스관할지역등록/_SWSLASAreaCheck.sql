IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASAreaCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASAreaCheck
GO

/********************************
 설    명 - 서비스관할지역등록:체크
 작 성 일 - 2020년 05월 12일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASAreaCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @Count        INT          ,
            @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT  

    -------------------------------------------      
    -- 서비스관할지역(_TSLASArea) 테이블에 없는 데이터의 WorkingTag 를 'A'로 UPDATE 
    -------------------------------------------  

	UPDATE #BIZ_OUT_DataBlock1 
	   SET WorkingTag = 'A' 
	  FROM #BIZ_OUT_DataBlock1 AS A 
	 WHERE A.WorkingTag = 'U'
	   AND A.Status     = 0 
	   AND NOT EXISTS ( SELECT 1
						  FROM _TSLASArea WITH(NOLOCK)
						 WHERE CompanySeq = @CompanySeq
						   AND UMAreaSeq  = A.UMAreaSeq 
					  ) 


RETURN

