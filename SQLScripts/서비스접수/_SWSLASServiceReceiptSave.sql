IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceReceiptSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceReceiptSave
GO

/********************************
 설    명 - 서비스접수:저장
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceReceiptSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq             ,      
                      @UserSeq                ,      
                      '_TSLASServiceReceipt'  ,     
                      '#BIZ_OUT_DataBlock1'   , -- 템프테이블명      
                      'ServiceReceiptSeq'     , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq
                              
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
        
        -- DELETE 
        DELETE _TSLASServiceReceipt    
          FROM _TSLASServiceReceipt     AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ServiceReceiptSeq = A.ServiceReceiptSeq  
         WHERE A.CompanySeq = @CompanySeq
		   AND B.WorkingTag = 'D'   
		   AND B.Status     = 0  
           
        IF @@ERROR <> 0 RETURN      

		   
    END 
       
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  
      
       UPDATE _TSLASServiceReceipt    
          SET ClientSeq          =ISNULL(B.ClientSeq, '')         , 	  
              ServiceReceiptDate =ISNULL(B.ServiceReceiptDate, ''),
              ReceiveUserSeq     =ISNULL(B.ReceiveUserSeq, 0)       ,
              UMReceiptKind      =ISNULL(B.UMReceiptKind, 0)        ,
              UMServiceKind      =ISNULL(B.UMServiceKind, 0)        ,
              BuyDate            =ISNULL(B.BuyDate, '')             ,
              SMWarrantyKind     =ISNULL(B.SMWarrantyKind, 0)       ,
              ItemSeq            =ISNULL(B.ItemSeq, 0)              ,
              ItemClassSSeq      =ISNULL(B.ItemClassSSeq, 0)        ,
              TroubleSeq         =ISNULL(B.TroubleSeq, 0)           ,
              Memo               =ISNULL(B.Memo, '')                ,
              Remark             =ISNULL(B.Remark, '')              ,
              ReceiptClientName  =ISNULL(B.ReceiptClientName, '')   ,
              SMPayKind          =ISNULL(B.SMPayKind, 0)            ,
              SMDelvPayKind      =ISNULL(B.SMDelvPayKind, '')       ,
              ReceiptDate        =ISNULL(B.ReceiptDate, '')         ,
              ServiceCenterSeq   =ISNULL(B.ServiceCenterSeq, 0)     ,
              ASEmpSeq           =ISNULL(B.ASEmpSeq, 0)             ,
              VisitDate          =ISNULL(B.VisitDate, '')           ,
              UMVisitTime        =ISNULL(B.UMVisitTime, 0)          ,
			  PgmSeq             =@PgmSeq                         ,
              LastUserSeq        =@UserSeq                        ,
              LastDateTime       =GETDATE()
         FROM _TSLASServiceReceipt     AS A 
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.ServiceReceiptSeq = A.ServiceReceiptSeq  
        WHERE A.CompanySeq = @CompanySeq   
		  AND B.Status     = 0  
          AND B.WorkingTag = 'U' 
  
        IF @@ERROR <> 0 RETURN   
         

    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLASServiceReceipt(CompanySeq        , ServiceReceiptSeq , ClientSeq         , ServiceReceiptNo  , ServiceReceiptDate,
                                         ReceiveUserSeq    , UMReceiptKind     , UMServiceKind     , BuyDate           , SMWarrantyKind    , 
                                         ItemSeq           , ItemClassSSeq     , TroubleSeq        , Memo              , Remark            , 
                                         ReceiptClientName , SMPayKind         , SMDelvPayKind     , ReceiptDate       , ServiceCenterSeq  , 
                                         ASEmpSeq          , VisitDate         , UMVisitTime       , PgmSeq            , LastUserSeq       , 
                                         LastDateTime)
        SELECT @CompanySeq       , ServiceReceiptSeq , ClientSeq         , ServiceReceiptNo  , ServiceReceiptDate,
               ReceiveUserSeq    , UMReceiptKind     , UMServiceKind     , BuyDate           , SMWarrantyKind    , 
               ItemSeq           , ItemClassSSeq     , TroubleSeq        , Memo              , Remark            , 
               ReceiptClientName , SMPayKind         , SMDelvPayKind     , ReceiptDate       , ServiceCenterSeq  , 
               ASEmpSeq          , VisitDate         , UMVisitTime       , @PgmSeq           , @UserSeq          , 
               GETDATE() 
          FROM #BIZ_OUT_DataBlock1   
         WHERE WorkingTag = 'A' 
		   AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN    


    END

RETURN
