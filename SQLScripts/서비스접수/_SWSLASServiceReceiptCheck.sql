IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceReceiptCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceReceiptCheck
GO

/********************************
 설    명 - 서비스접수:체크
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceReceiptCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT          ,
            @Date         NCHAR(8)     ,
            @MAXNo        NVARCHAR(20) 

    SELECT @Date = ISNULL(MAX(ServiceReceiptDate),REPLACE(CONVERT(CHAR(10),GETDATE(),121),'-',''))      
      FROM #BIZ_OUT_DataBlock1 AS A 
     WHERE A.WorkingTag = 'A' 
       AND A.Status     = 0      

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock1)
    BEGIN

		-------------------------------------------      
		-- 행 삭제 시 해당 인원 사용여부 체크 
		-- 20200508 김재호 : 본 화면은 AS기사를 등록하는 화면으로, 해당 AS기사에 대해 서비스 접수된 건이 있는 경우 삭제 불가 등의 체크로직을 추가해야함
		------------------------------------------- 

		/*
			체크로직
		*/

		-------------------------------------------      
		-- 서비스센터가 고객의 관할지역에 해당하는 서비스센터가 아닐 경우 저장 불가 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               2179                , -- @1와(과) @2이(가) 일치하지 않습니다. 다시 확인하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 2179)  
                               @LanguageSeq        ,  
                               72892, N'관할지역'  , -- SELECT * FROM _TCADictionary WHERE Word like '%관할지역%'  
                               14955, N'서비스센터'  -- SELECT * FROM _TCADictionary WHERE Word like '%서비스센터%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1        AS A
               LEFT OUTER JOIN _TSLASArea AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq 
                                                           AND B.UMAreaSeq  = A.UMAreaSeq 
         WHERE A.WorkingTag       IN ('A', 'U')
           AND A.Status           = 0
           AND A.UMAreaSeq        <> ISNULL(B.UMAreaSeq, 0) 
           AND A.ServiceCenterSeq <> 0
  
		-------------------------------------------      
		-- 선택된 접수유형의 택배여부(추가정보정의)에 체크되지 않았으나 택배정보가 입력되어있을 경우 저장 불가 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               1306                , -- @1이(가) @2이(가) 아니라서 @3 할 수 없습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1306)  
                               @LanguageSeq        ,  
                               72884, N'접수유형'   , -- SELECT * FROM _TCADictionary WHERE Word like '%접수유형%'
							   22648, N'택배'    , -- SELECT * FROM _TCADictionary WHERE Word like '%택배%'
                               17053, N'저장'        -- SELECT * FROM _TCADictionary WHERE Word like '%저장%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1              AS A
               LEFT OUTER JOIN  _TDAUMInorValue AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq 
                                                                 AND B.MajorSeq   = 100118
                                                                 AND B.MinorSeq   = A.UMReceiptKind
         WHERE A.WorkingTag IN ('A', 'U')
           AND A.Status     = 0 
           AND ISNULL(B.ValueText, '') <> '1' AND (ISNULL(A.ReceiptClientName, '') <> '' OR ISNULL(A.SMPayKind, 0) <> 0 OR ISNULL(A.SMDelvPayKind, 0) <> 0 OR ISNULL(A.ReceiptDate, '') <> '')

		-------------------------------------------      
		-- 입력한 품목소분류 - 고장증상이 [서비스코드등록] 화면에서 입력한 품목소분류 - 서비스코드와 일치하지 않으면 저장 불가 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               2179                , -- @1와(과) @2이(가) 일치하지 않습니다. 다시 확인하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 2179)  
                               @LanguageSeq        ,  
                               592, N'관할지역'    , -- SELECT * FROM _TCADictionary WHERE Word like '%품목소분류%'  
                               3927, N'고장증상'     -- SELECT * FROM _TCADictionary WHERE Word like '%고장증상%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1        AS A   
               LEFT OUTER JOIN _TSLASCode AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                           AND B.ASCodeSeq  = A.TroubleSeq
         WHERE A.WorkingTag       IN ('A', 'U')
           AND A.Status           = 0    
           AND A.ItemClassSSeq    <> B.UMItemClass  
           AND A.TroubleSeq       <> 0 

           
		-------------------------------------------      
		-- 처리기사가 입력한 서비스센터의 소속이 아닌 경우 저장 불가 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               1293                , -- @1의 @2을(를) 확인하세요.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1293)  
                               @LanguageSeq        ,  
                               17907, N'처리기사'  , -- SELECT * FROM _TCADictionary WHERE Word like '%처리기사%'
							   14955, N'서비스센터'  -- SELECT * FROM _TCADictionary WHERE Word like '%서비스센터%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1                    AS A   
               LEFT OUTER JOIN _TSLServiceCenterEmpIn AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                                       AND B.ASEmpSeq   = A.ASEmpSeq
         WHERE A.WorkingTag       IN ('A', 'U')
           AND A.Status           = 0      
           AND B.ServiceCenterSeq <> A.ServiceCenterSeq 
           AND A.ServiceCenterSeq <> 0 
           AND A.ASEmpSeq         <> 0

		-------------------------------------------      
		-- 방문일과 방문시간은 현재일 및 현재시간 이후로만 등록 가능 
		-------------------------------------------        
        
        -- 방문일을 입력하지 않은 경우
        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               1008                , -- @1을(를) 입력하지 않았습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1008)  
                               @LanguageSeq        ,  
                               168, N'방문일'        -- SELECT * FROM _TCADictionary WHERE Word like '%방문일%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1        AS A   
               LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                           AND B.MinorSeq   = A.UMVisitTime
         WHERE A.WorkingTag IN ('A', 'U')
           AND A.Status      = 0      
           AND A.VisitDate   = '' 
           AND A.UMVisitTime <> 0

        -- 방문시간을 입력하지 않은 경우
        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               1008                , -- @1을(를) 입력하지 않았습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1008)  
                               @LanguageSeq        ,  
                               5578, N'방문시간'     -- SELECT * FROM _TCADictionary WHERE Word like '%방문시간%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1        AS A   
               LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                           AND B.MinorSeq   = A.UMVisitTime
         WHERE A.WorkingTag IN ('A', 'U')
           AND A.Status      = 0      
           AND A.VisitDate   <> '' 
           AND A.UMVisitTime = 0


        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               31                  , -- @1이(가) @2보다 커야 합니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 2133)  
                               @LanguageSeq        ,  
                               5578, N'방문시간'   , -- SELECT * FROM _TCADictionary WHERE Word like '%방문시간%'
							   0, N'현재시간'  -- SELECT * FROM _TCADictionary WHERE Word like '%현재시간%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1        AS A   
               LEFT OUTER JOIN _TDAUMinor AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                           AND B.MinorSeq   = A.UMVisitTime
         WHERE A.WorkingTag IN ('A', 'U')
           AND A.Status     = 0      
           AND CONVERT(DATETIME, A.VisitDate + ' ' + B.MinorName) < CONVERT(DATETIME, CONVERT(CHAR(16), GETDATE(), 21))
           AND A.VisitDate <> '' 
           AND A.UMVisitTime <> 0

		-------------------------------------------      
		-- INSERT 번호부여(맨 마지막 처리) 
		-------------------------------------------         

		IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)  
		BEGIN  

			-- 키값생성코드부분 시작  
			EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLASServiceReceipt', 'ServiceReceiptSeq', 1 
  
			-- Temp Table 에 생성된 키값 UPDATE  
			UPDATE #BIZ_OUT_DataBlock1  
			   SET ServiceReceiptSeq = @Seq + 1
			  FROM #BIZ_OUT_DataBlock1 AS A
			 WHERE A.WorkingTag = 'A' 
			   AND A.Status     = 0  

            -- 번호생성코드부분 시작        
            EXEC dbo._SWCOMCreateNo 'SL', '_TSLASServiceReceipt', @CompanySeq, 0, @Date, @MaxNo OUTPUT 
             
            -- Temp Talbe 에 생성된 키값 UPDATE      
            UPDATE #BIZ_OUT_DataBlock1    
               SET ServiceReceiptNo = @MaxNo      
              FROM #BIZ_OUT_DataBlock1 AS A   
             WHERE A.WorkingTag = 'A'      
               AND A.Status     = 0   

  
		END  

    END

RETURN
