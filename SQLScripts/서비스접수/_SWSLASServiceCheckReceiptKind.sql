IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceCheckReceiptKind') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceCheckReceiptKind
GO

/********************************
 설    명 - 접수유형:택배여부체크  
 작 성 일 - 2020년 04월 20일
 작 성 자 - 유태우
 수정내역 - 
*********************************/

-- SP파라미터들
CREATE PROC dbo._SWSLASServiceCheckReceiptKind
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    -- 사용할 변수를 선언한다.
    DECLARE @UMReceiptKind INT, 
            @IsDelivery    NCHAR(1)


    -- XML의 DataBlock1으로부터 값을 가져와 변수에 저장한다.  
    SELECT @UMReceiptKind = ISNULL(UMReceiptKind,  '')
      FROM #BIZ_IN_DataBlock1


    SELECT ISNULL(B.ValueText, '') AS IsDelivery 
      FROM _TDAUMinor           AS A WITH(NOLOCK)
           JOIN _TDAUMInorValue AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                 AND B.MinorSeq   = A.MinorSeq 
                                                 AND B.Serl       = 10000001
     WHERE A.MinorSeq = @UMReceiptKind
           
RETURN




