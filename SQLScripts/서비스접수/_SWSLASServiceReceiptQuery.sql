IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceReceiptQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceReceiptQuery
GO

/********************************
 설    명 - 서비스접수:조회
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceReceiptQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    DECLARE @ServiceReceiptSeq INT 

    SELECT @ServiceReceiptSeq = ISNULL(ServiceReceiptSeq, 0)
      FROM #BIZ_IN_DataBlock1 

    SELECT A.ServiceReceiptDate             , 
           B.MinorName AS UMServiceKindName ,
           A.UMServiceKind                  ,
           C.MinorName AS UMReceiptKindName ,
           A.UMReceiptKind                  ,
           A.ServiceReceiptNo               ,
           D.ItemName                       ,
           A.ItemSeq                        ,
           E.MinorName AS ItemClassSName    ,
           A.ItemClassSSeq                  ,
           F.ASCode AS TroubleName          ,
           A.TroubleSeq                     ,
           A.Memo                           ,
           A.Remark                         ,
           G.UserName AS ReceiveUserName    ,
           A.ReceiveUserSeq                 ,
           A.BuyDate                        ,
           H.MinorName AS SMWarrantyKindName,
           A.SMWarrantyKind                 ,
           A.ReceiptClientName              ,
           I.MinorName AS SMPayKindName     ,
           A.SMPayKind                      ,
           J.MinorName AS SMDelvPayKindName ,
           A.SMDelvPayKind                  ,
           A.ReceiptDate                    ,
           K.ServiceCenterName              ,
           A.ServiceCenterSeq               ,
           L.ASEmpName                      ,
           A.ASEmpSeq                       ,
           A.VisitDate                      ,
           M.MinorName AS UMVisitTimeName   ,
           A.UMVisitTime                    ,
           A.ClientSeq                      ,
           A.ServiceReceiptSeq              
      FROM _TSLASServiceReceipt                   AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDAUMinor             AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq
                                                                   AND B.MinorSeq         = A.UMServiceKind
           LEFT OUTER JOIN _TDAUMinor             AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq
                                                                   AND C.MinorSeq         = A.UMReceiptKind
           LEFT OUTER JOIN _TDAItem               AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq
                                                                   AND D.ItemSeq          = A.ItemSeq    
           LEFT OUTER JOIN _TDAUMinor             AS E WITH(NOLOCK) ON E.CompanySeq       = A.CompanySeq
                                                                   AND E.MinorSeq         = A.ItemClassSSeq                                                             
           LEFT OUTER JOIN _TSLASCode             AS F WITH(NOLOCK) ON F.CompanySeq       = A.CompanySeq
                                                                   AND F.ASCodeSeq        = A.TroubleSeq
           LEFT OUTER JOIN _TCAUser               AS G WITH(NOLOCK) ON G.CompanySeq       = A.CompanySeq
                                                                   AND G.UserSeq          = A.ReceiveUserSeq
           LEFT OUTER JOIN _TDASMinor             AS H WITH(NOLOCK) ON H.CompanySeq       = A.CompanySeq
                                                                   AND H.MinorSeq         = A.SMWarrantyKind                                                      
           LEFT OUTER JOIN _TDASMinor             AS I WITH(NOLOCK) ON I.CompanySeq       = A.CompanySeq
                                                                   AND I.MinorSeq         = A.SMPayKind    
           LEFT OUTER JOIN _TDASMinor             AS J WITH(NOLOCK) ON J.CompanySeq       = A.CompanySeq
                                                                   AND J.MinorSeq         = A.SMDelvPayKind
           LEFT OUTER JOIN _TSLServiceCenter      AS K WITH(NOLOCK) ON K.CompanySeq       = A.CompanySeq
                                                                   AND K.ServiceCenterSeq = A.ServiceCenterSeq
           LEFT OUTER JOIN _TSLServiceCenterEmpIn AS L WITH(NOLOCK) ON L.CompanySeq       = A.CompanySeq
                                                                   AND L.ASEmpSeq         = A.ASEmpSeq
           LEFT OUTER JOIN _TDAUMinor             AS M WITH(NOLOCK) ON M.CompanySeq       = A.CompanySeq
                                                                   AND M.MinorSeq         = A.UMVisitTime    
     WHERE A.CompanySeq        = @CompanySeq
       AND A.ServiceReceiptSeq = @ServiceReceiptSeq   
                                                                    
RETURN
