CREATE TABLE _TSLASServiceReceipt (
    CompanySeq          INT          ,
    ServiceReceiptSeq   INT          ,
    ClientSeq           INT          ,
    ServiceReceiptNo    NVARCHAR(20) ,
    ServiceReceiptDate  NCHAR(8)     ,
    ReceiveUserSeq      INT          ,
    UMReceiptKind       INT          ,
    UMServiceKind       INT          ,
    BuyDate             NCHAR(8)     ,
    SMWarrantyKind      INT          ,
    ItemSeq             INT          ,
    ItemClassSSeq       INT          ,
    TroubleSeq          INT          ,
    Memo                NVARCHAR(400),
    Remark              NVARCHAR(400),
    ReceiptClientName   NVARCHAR(100),
    SMPayKind           INT          ,
    SMDelvPayKind       INT          ,
    ReceiptDate         NCHAR(8)     ,
    ServiceCenterSeq    INT          ,
    ASEmpSeq            INT          ,
    VisitDate           NCHAR(8)     ,
    UMVisitTime         INT          ,
    PgmSeq              INT          ,
    LastUserSeq         INT          ,
    LastDateTime        DATETIME     )

CREATE CLUSTERED INDEX IDX_TSLASServiceReceipt ON _TSLASServiceReceipt (CompanySeq, ServiceReceiptSeq)

IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 521074)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLASServiceReceipt', 1, 521074, '1', 1, GETDATE()END