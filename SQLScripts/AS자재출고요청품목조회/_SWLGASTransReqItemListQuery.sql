IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWLGASTransReqItemListQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWLGASTransReqItemListQuery
GO

/*************************************************************************************************        
 설  명 - AS자재출고요청품목조회-조회   
 작성일 - 2020.04.28 
 작성자 - 유태우   
 수정일 - 
*************************************************************************************************/          
CREATE PROCEDURE _SWLGASTransReqItemListQuery      
    @ServiceSeq     INT     = 0,
    @WorkingTag     NVARCHAR(10)= '',
    @CompanySeq     INT     = 1,
    @LanguageSeq    INT     = 1,
    @UserSeq        INT     = 0,
    @PgmSeq         INT     = 0,
    @IsTransaction  BIT     = 0    
AS           
    DECLARE   @BizUnit            INT          ,        
              @ReqNo              NVARCHAR(30) ,  
              @EmpSeq             INT          ,  
              @ReqDateFr          NCHAR(8)     ,  
              @ReqDateTo          NCHAR(8)     ,
              @CompleteWishDateFr NCHAR(8)     ,  
              @CompleteWishDateTo NCHAR(8)     ,  
              @InWHSeq            INT          ,
              @ItemName           NVARCHAR(200),  
              @ItemNo             NVARCHAR(100),   
              @SMProgressType     INT          ,  
              @InOutReqDetailKind INT
    
    SELECT  @BizUnit            = ISNULL(BizUnit, 0)            ,  
            @ReqNo              = ISNULL(ReqNo, '')             ,  
            @EmpSeq             = ISNULL(EmpSeq, 0)             ,  
            @ReqDateFr          = ISNULL(ReqDateFr, '')         ,  
            @ReqDateTo          = ISNULL(ReqDateTo, '')         ,
            @CompleteWishDateFr = ISNULL(CompleteWishDateFr, ''),  
            @CompleteWishDateTo = ISNULL(CompleteWishDateTo, ''),  
            @InWHSeq            = ISNULL(InWHSeq, 0)            ,
            @InOutReqDetailKind = ISNULL(InOutReqDetailKind, 0) ,  
            @ItemName           = ISNULL(ItemName, '')          ,  
            @ItemNo             = ISNULL(ItemNo, '')            ,  
            @SMProgressType     = ISNULL(SMProgressType, 0) 
      FROM #BIZ_IN_DataBlock1
    
    CREATE TABLE #Temp_ASTransReqItemProg(
        IDX_NO         INT IDENTITY , 
        ReqSeq         INT          , 
        ReqSerl        INT          , 
        CompleteCHECK  INT          , 
        SMProgressType INT NULL     ,
        Confirm        INT          ,  
        ConfirmEmp     NVARCHAR(100),  
        ConfirmDate    NCHAR(8)     ,  
        ConfirmReason  NVARCHAR(500),  
        IsStop         NCHAR(1)     ,  
        StopEmp        NVARCHAR(100),  
        StopDate       NCHAR(8)     ,  
        StopReason     NVARCHAR(500))

    -- 진행체크할 테이블값 테이블      
    CREATE TABLE #TMP_PROGRESSTABLE(IDOrder INT, TABLENAME   NVARCHAR(100))      
        
    --    -- 진행체크할 데이터 테이블      
    --    CREATE TABLE #Temp_InOutReq(IDX_NO INT IDENTITY, ReqSeq INT, ReqSerl INT)      
      
    -- 진행된 내역 테이블 : _SCOMProgressTracking 에서 사용      
    CREATE TABLE #TCOMProgressTracking(
        IDX_NO  INT           ,            
        IDInOut INT           ,            
        Seq     INT           ,           
        Serl    INT           ,            
        SubSerl INT           ,      
        Qty     DECIMAL(19, 5), 
        StdQty  DECIMAL(19, 5), 
        Amt     DECIMAL(19, 5),
        VAT     DECIMAL(19, 5), 
        DOMAmt  DECIMAL(19, 5),
        DOMVAT  DECIMAL(19, 5))          
    
    -- 진행된 내역을 가로로 펼쳐주는 테이블      
    CREATE TABLE #InOutTracking(IDX_NO INT, InOutQty   DECIMAL(19,5), InOutAmt   DECIMAL(19,5))   
    
    INSERT INTO #Temp_ASTransReqItemProg(ReqSeq, ReqSerl,CompleteCHECK)      
    SELECT A.ReqSeq, A.ReqSerl, -1     
      FROM _TLGASTransReqItem       AS A WITH (NOLOCK)  
           JOIN _TLGASTransReq      AS B WITH (NOLOCK) ON A.CompanySeq = B.CompanySeq  
                                                      AND A.ReqSeq     = B.ReqSeq     
           LEFT OUTER JOIN _TDAItem AS C WITH (NOLOCK) ON A.CompanySeq = C.CompanySeq  
                                                      AND A.ItemSeq    = C.ItemSeq    
     WHERE (@BizUnit = 0             OR B.BizUnit            = @BizUnit               )
      --AND (@SMProgressType = 0 OR X.SMProgressType = @SMProgressType)           
       AND (@ReqDateFr          = '' OR B.ReqDate            >=   @ReqDateFr          )  
       AND (@ReqDateTo          = '' OR B.ReqDate            <=   @ReqDateTo          )
       AND (@CompleteWishDateFr = '' OR B.CompleteWishDate   >=   @CompleteWishDateFr )  
       AND (@CompleteWishDateTo = '' OR B.CompleteWishDate   <=   @CompleteWishDateTo )  
       AND (@ReqNo              = '' OR B.ReqNo              LIKE @ReqNo + N'%'       )  
       AND (@EmpSeq             = 0  OR B.EmpSeq             =    @EmpSeq             )  
       AND (@InWHSeq            = 0  OR B.InWHSeq            =    @InWHSeq            )
       AND (@InOutReqDetailKind = 0  OR B.InOutReqDetailKind =    @InOutReqDetailKind )  
       AND (@ItemName           = '' OR C.ItemName           LIKE @ItemName + N'%'    )  
       AND (@ItemNo             = '' OR C.ItemNo             LIKE @ItemNo + N'%'      )
       AND A.CompanySeq         = @CompanySeq
    ORDER BY A.ReqSeq, A.ReqSerl
    
    EXEC _SWCOMConfirmStopUpdate @CompanySeq, '#Temp_ASTransReqItemProg', 'ReqSeq', 'ReqSerl', '', @PgmSeq 
    --진행상태  
    EXEC _SWCOMProgStatus @CompanySeq, '_TLGASTransReqItem', 1036004, '#Temp_ASTransReqItemProg', 'ReqSeq', 'ReqSerl', '', '', '', '', '', '', 'CompleteCHECK',   
                          1, 'Qty', 'STDQty', '', '', 'ReqSeq', 'ReqSerl', '', '_TLGASTransReq', @PgmSeq  
                         
    UPDATE #Temp_ASTransReqItemProg       
       SET SMProgressType = (SELECT CASE WHEN A.IsStop = '1' AND B.MinorSeq = 1037006 THEN 1037007 --진행중단      
                                         WHEN B.MinorSeq = 1037009 THEN 1037009 -- 완료      
                                         WHEN A.IsStop = '1' THEN 1037005 -- 중단      
                                         ELSE B.MinorSeq END)      
      FROM #Temp_ASTransReqItemProg AS A       
           LEFT OUTER JOIN _TDASMinor AS B WITH(NOLOCK) ON B.MajorSeq      = 1037      
                                                       AND A.CompleteCHECK = B.Minorvalue   
    
    
    -- 진행수량  
    INSERT #TMP_PROGRESSTABLE       
    SELECT 1, '_TLGInOutDailyItem'      
  
    EXEC _SWCOMProgressTracking @CompanySeq, '_TLGASTransReqItem', '#Temp_ASTransReqItemProg', 'ReqSeq', 'ReqSerl', ''    
  
    INSERT INTO #InOutTracking      
    SELECT IDX_NO                                          ,      
           SUM(CASE IDInOut WHEN 1 THEN Qty     ELSE 0 END),      
           SUM(CASE IDInOut WHEN 1 THEN Amt     ELSE 0 END)  
      FROM #TCOMProgressTracking      
     GROUP BY IDX_No   
     
    SELECT ISNULL(A.ReqSeq, 0)                    AS ReqSeq                ,  
           ISNULL(A.ReqSerl, 0)                   AS ReqSerl               ,  
           ISNULL(D.BizUnitName, '')              AS BizUnitName           ,  
           ISNULL(B.BizUnit, 0)                   AS BizUnit               ,
           ISNULL(E.BizUnitName, '')              AS TransBizUnitName      ,  
           ISNULL(B.TransBizUnit, 0)              AS TransBizUnit          ,  
           ISNULL(B.ReqNo, '')                    AS ReqNo                 ,    
           ISNULL(H.EmpName, '')                  AS EmpName               ,  
           ISNULL(B.EmpSeq, 0)                    AS EmpSeq                ,  
           ISNULL(B.ReqDate, '')                  AS ReqDate               ,
           ISNULL(B.CompleteWishDate, '')         AS CompleteWishDate      ,    
           ISNULL(J.WHName, '')                   AS InWHName              ,  
           ISNULL(B.InWHSeq, 0)                   AS InWHSeq               ,
           ISNULL(M.MinorName,'')                 AS InOutReqDetailKindName,  
           ISNULL(B.InOutReqDetailKind, 0)        AS InOutReqDetailKind    ,  
           ISNULL(C.ItemName, '')                 AS ItemName              ,  
           ISNULL(C.ItemNo, '')                   AS ItemNo                ,  
           ISNULL(C.Spec, '')                     AS Spec                  ,  
           ISNULL(A.ItemSeq, 0)                   AS ItemSeq               ,  
           ISNULL(N.UnitName,'')                  AS UnitName              ,  
           ISNULL(A.UnitSeq, 0)                   AS UnitSeq               ,  
           ISNULL(A.Qty, 0)                       AS Qty                   ,  --요청수량  
           ISNULL(Y.InOutQty, 0)                  AS ProgressQty           ,  --진행수량  
           CASE WHEN ABS(ISNULL(A.Qty, 0)) <=  ABS(ISNULL(Y.InOutQty, 0)) THEN 0 ELSE ISNULL(A.Qty, 0) - ISNULL(Y.InOutQty, 0) END AS NotProgressQty,  --미진행수량  
           ISNULL(Z.MinorName, '')                AS SMProgressTypeName,
           ISNULL(L.UnitName,'')                  AS STDUnitName,  
           ISNULL(C.UnitSeq, 0)                   AS STDUnitSeq,  
           ISNULL(A.STDQty, 0)                    AS STDQty,  
           ISNULL(A.Remark, 0)                    AS Remark,
           ISNULL(B.InOutReqType, 0)              AS InOutReqType, 
           CASE ISNULL(B.InOutReqType, 0) WHEN 30 THEN '기타출고요청'  
                                          WHEN 40 THEN '기타입고요청'  
                                          WHEN 31 THEN '자재기타출고요청'  
                                          WHEN 41 THEN '자재기타입고요청'  
                                          WHEN 50 THEN '위탁출하요청'  
                                          WHEN 80 THEN '이동요청'
                                          WHEN 81 THEN '적송요청'  
                                          WHEN 82 THEN '자재이동요청'
                                          WHEN 83 THEN '자재적송요청'  
                                          WHEN 100 THEN '수탁품출고요청'  
                                          WHEN 110 THEN '수탁품입고요청' 
                                          ELSE '' END AS InOutReqTypeName            ,  
           'ReqSeq'                               AS ColumnName                      ,   
           A.LotNo                                AS LotNo                           ,  
           B.BizUnit                              AS BizUnit                         ,  -- 입고요청사업부문  
           ISNULL(X.Confirm,0)                    AS Confirm                         ,  
           ISNULL(X.ConfirmEmp,'')                AS ConfirmEmp                      ,  
           ISNULL(X.ConfirmDate,'')               AS ConfirmDate                     ,  
           ISNULL(X.ConfirmReason,'')             AS ConfirmReason                   ,  
           ISNULL(X.IsStop,'')                    AS IsStop                          ,  
           ISNULL(X.StopEmp,'')                   AS StopEmp                         ,  
           ISNULL(X.StopDate,'')                  AS StopDate                        ,  
           ISNULL(X.StopReason,'')                AS StopReason                      ,
           A.LastDateTime     AS LastDateTime                                        ,
           (SELECT UserName FROM _TCAUser WHERE CompanySeq = @CompanySeq AND UserSeq = A.LastUserSeq) AS LastUserName,
           ISNULL(B.IsAuto,'') AS IsAuto 
     FROM #Temp_ASTransReqItemProg     AS X 
          JOIN _TLGASTransReqItem        AS A WITH (NOLOCK) ON X.ReqSeq         = A.ReqSeq  
                                                           AND X.ReqSerl        = A.ReqSerl  
          JOIN _TLGASTransReq            AS B WITH (NOLOCK) ON A.CompanySeq     = B.CompanySeq  
                                                           AND A.ReqSeq         = B.ReqSeq   
          LEFT OUTER JOIN _TDAItem       AS C WITH (NOLOCK) ON A.CompanySeq     = C.CompanySeq  
                                                           AND A.ItemSeq        = C.ItemSeq
          LEFT OUTER JOIN _TDABizUnit    AS D WITH (NOLOCK) ON D.CompanySeq     = B.CompanySeq 
                                                           AND D.BizUnit        = B.BizUnit
          LEFT OUTER JOIN _TDABizUnit    AS E WITH (NOLOCK) ON E.CompanySeq     = B.CompanySeq
                                                           AND E.BizUnit        = B.TransBizUnit
          LEFT OUTER JOIN _TDAEmp        AS H WITH (NOLOCK) ON H.CompanySeq     = B.CompanySeq 
                                                           AND H.EmpSeq         = B.EmpSeq
          LEFT OUTER JOIN _TDAWH         AS J WITH (NOLOCK) ON J.CompanySeq     = B.CompanySeq 
                                                           AND J.WHSeq          = B.InWHSeq
          LEFT OUTER JOIN _TDAUMinor     AS M WITH (NOLOCK) ON M.CompanySeq     = B.CompanySeq 
                                                           AND M.MinorSeq       = B.InOutReqDetailKind
          LEFT OUTER JOIN _TDAUnit       AS N WITH (NOLOCK) ON N.CompanySeq     = A.CompanySeq 
                                                           AND N.UnitSeq        = A.UnitSeq
          LEFT OUTER JOIN _TDAUnit       AS L WITH (NOLOCK) ON L.CompanySeq     = C.CompanySeq 
                                                           AND L.UnitSeq        = C.UnitSeq
          LEFT OUTER JOIN #InOutTracking AS Y               ON X.IDX_No         = Y.IDX_No   
          LEFT OUTER JOIN _TDASMinor     AS Z WITH(NOLOCK)  ON Z.CompanySeq     = @CompanySeq  
                                                           AND X.SMProgressType = Z.MinorSeq  
    WHERE A.CompanySeq  = @CompanySeq  
      AND (@SMProgressType  = 0                OR 
          (X.SMProgressType = @SMProgressType) OR 
          (@SMProgressType  = 1037010         AND X.SMProgressType IN( SELECT MinorSeq 
                                                                         FROM _TDASMinorValue WITH(NOLOCK) 
                                                                        WHERE CompanySeq = @CompanySeq 
                                                                          AND MajorSeq   = 1037 
                                                                          AND Serl       = 1001 
                                                                          AND ValueText  = '1' ))) --20150608 이범확 추가
    ORDER BY B.BizUnit, B.ReqDate, B.ReqNo, A.ReqSerl  

RETURN      
