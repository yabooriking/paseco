IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterEmpInCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterEmpInCheck
GO

/********************************
 설    명 - 서비스센터인원등록:체크
 작 성 일 - 2020년 05월 08일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLServiceCenterEmpInCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @Count        INT          ,
            @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT  

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock1)
    BEGIN

        -------------------------------------------      
        -- 처리기사(서비스센터인원) 삭제 시, 해당 처리기사 입력된 서비스 접수, 처리 건이 있으면 삭제 불가
        /*
            참고화면
            서비스접수 - [서비스접수]
            서비스처리 - [서비스처리]
        */
        -------------------------------------------  

        -- 서비스접수내역이 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
                               @Status      OUTPUT     ,  
                               @Results     OUTPUT     ,  
                               1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq            ,  
                               24355, N'서비스접수정보', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스접수정보%' 
                               308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS ( SELECT TOP 1 1
                          FROM _TSLASServiceReceipt WITH(NOLOCK)
                         WHERE CompanySeq = @CompanySeq
                           AND ASEmpSeq = A.ASEmpSeq
                      )

        -- 서비스처리내역이 존재할 경우 삭제 불가 - 추후 테이블 생성되면 추가 
   --     EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
   --                            @Status      OUTPUT     ,  
   --                            @Results     OUTPUT     ,  
   --                            1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
   --                            @LanguageSeq            ,  
   --                            72883, N'서비스처리내역', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스처리내역%' 
   --                            308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

   --     UPDATE #BIZ_OUT_DataBlock1 
   --        SET Result      = @Results    , 
   --            MessageType = @MessageType,  
   --            Status      = @Status  
   --       FROM #BIZ_OUT_DataBlock1 AS A
		 --WHERE A.WorkingTag = 'D'
		 --  AND A.Status     = 0  
   --        AND EXISTS (SELECT 1 
   --                      FROM _TSLASServiceReceipt WITH(NOLOCK)
   --                     WHERE CompanySeq = @CompanySeq
   --                       AND ServiceCenterSeq = A.ServiceCenterSeq
   --                   )

	
		-------------------------------------------      
		-- 이름 + 사용자ID 중복체크
		-- 동명이인은 등록될 수 있으나, 하나의 계정(사용자ID)에 여러명의 인원이 등록될 수 없음
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               1088                , -- 동일한 @1이(가) 존재합니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1088)  
                               @LanguageSeq        ,  
                               6185, N'사용자ID'     -- SELECT * FROM _TCADictionary WHERE Word like '%사용자ID%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT S.UserSeq
                        FROM (
                               SELECT A.UserSeq
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A','U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.UserSeq
                                 FROM _TSLServiceCenterEmpIn AS A WITH(NOLOCK) 
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1
                                                    WHERE WorkingTag IN ('A','U')
												      AND ASEmpSeq   = A.ASEmpSeq
                                                 )
							 ) AS S
					   GROUP BY S.UserSeq 
					  HAVING COUNT(1) > 1
                    ) AS B
                 ON B.UserSeq = A.UserSeq 
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0 	    

		-------------------------------------------      
		-- 입사일이 퇴사일보다 큰 경우 체크 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT ,  
                               @Status      OUTPUT ,  
                               @Results     OUTPUT ,  
                               31                  , -- @1이(가) @2보다 커야 합니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 31)  
                               @LanguageSeq        ,  
                               1176, N'퇴사일'     , -- SELECT * FROM _TCADictionary WHERE Word like '%퇴사일%'
							   215, N'입사일'        -- SELECT * FROM _TCADictionary WHERE Word like '%입사일%'

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results, 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
         WHERE A.WorkingTag             IN ('A', 'U')
           AND A.Status                 = 0 
           AND A.RetireDate             <= A.EntDate
		   AND ISNULL(A.RetireDate, '') <> '' 

		-------------------------------------------      
		-- INSERT 번호부여(맨 마지막 처리) 
		-------------------------------------------         
		SELECT  @Count = COUNT(1) FROM #BIZ_OUT_DataBlock1 WHERE (WorkingTag = 'A' AND Status = 0)  

		IF(@Count > 0)  
		BEGIN  
            
			-- 키값생성코드부분 시작  
			EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLServiceCenterEmpIn', 'ASEmpSeq', @Count  
  
			-- Temp Table 에 생성된 키값 UPDATE  
			UPDATE #BIZ_OUT_DataBlock1  
			   SET ASEmpSeq = @Seq + DataSeq
			  FROM #BIZ_OUT_DataBlock1 AS A
			 WHERE A.WorkingTag = 'A' 
			   AND A.Status     = 0  

  
		END  

    END

RETURN
