IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterEmpInQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterEmpInQuery
GO

/********************************
 설    명 - 서비스센터인원등록:조회
 작 성 일 - 2020년 05월 08일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLServiceCenterEmpInQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    DECLARE @ServiceCenterSeq INT          ,
	        @ASEmpName        NVARCHAR(100)

    SELECT @ServiceCenterSeq = ISNULL(ServiceCenterSeq, 0),
	       @ASEmpName        = ISNULL(ASEmpName, '') 
	  FROM #BIZ_IN_DataBlock1

	SELECT A.ASEmpName			   ,
		   B.UserID 			   ,
		   A.UserSeq 			   ,
		   C.ServiceCenterName	   ,
		   A.ServiceCenterSeq 	   ,
		   B.PwdMailAdder AS Email ,
		   D.MinorName AS UMJdName , 
		   A.UMJdSeq 			   ,
		   A.EntDate			   ,
		   A.RetireDate			   ,
		   A.BirthDate			   ,
		   D.MinorName AS SMSexName,
		   A.SMSexSeq              ,
		   A.TelNo				   ,
		   A.Remark				   ,
		   A.ASEmpSeq              ,
           ISNULL(dbo._FWCOMDecrypt(A.Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq), '') AS Phone
	  FROM _TSLServiceCenterEmpIn            AS A WITH(NOLOCK)
	       LEFT OUTER JOIN _TCAUser          AS B WITH(NOLOCK) ON B.CompanySeq       = A.CompanySeq 
	                                                          AND B.UserSeq          = A.UserSeq 
           LEFT OUTER JOIN _TSLServiceCenter AS C WITH(NOLOCK) ON C.CompanySeq       = A.CompanySeq 
	                                                          AND C.ServiceCenterSeq = A.ServiceCenterSeq 
	       LEFT OUTER JOIN _TDAUMinor        AS D WITH(NOLOCK) ON D.CompanySeq       = A.CompanySeq 
	                                                          AND D.MinorSeq         = A.UMJdSeq 
	       LEFT OUTER JOIN _TDASMinor        AS E WITH(NOLOCK) ON E.CompanySeq       = A.CompanySeq 
	                                                          AND E.MinorSeq         = A.SMSexSeq 	 
	 WHERE A.CompanySeq       = @CompanySeq 
	   AND (@ServiceCenterSeq = 0  OR A.ServiceCenterSeq = @ServiceCenterSeq) 
	   AND  A.ASEmpName       LIKE @ASEmpName + '%'

RETURN

