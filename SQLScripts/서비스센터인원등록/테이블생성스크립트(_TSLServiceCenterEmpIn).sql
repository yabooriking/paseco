-- 테이블 생성 
CREATE TABLE _TSLServiceCenterEmpIn (
    CompanySeq       INT          ,
    ASEmpSeq         INT          ,
    ASEmpName        NVARCHAR(100),
    UserSeq          INT          ,
    ServiceCenterSeq INT          ,
    UMJdSeq          INT          ,
    EntDate          NCHAR(8)     ,
    RetireDate       NCHAR(8)     ,
    BirthDate        NCHAR(8)     ,
    SMSexSeq         INT          ,
    Phone            NVARCHAR(400),
    TelNo            NVARCHAR(400),
    Remark           NVARCHAR(400),
    PgmSeq           INT          ,
    LastUserSeq      INT          ,
    LastDateTime     DATETIME     )

CREATE CLUSTERED INDEX IDX_TSLServiceCenterEmpIn ON _TSLServiceCenterEmpIn (CompanySeq, ASEmpSeq)

--Log사용 설정
IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 521062)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLServiceCenterEmpIn', 1, 521062, '1', 1, GETDATE()END