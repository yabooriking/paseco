IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterEmpInSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterEmpInSave
GO

/********************************
 설    명 - 서비스센터인원등록:저장
 작 성 일 - 2020년 05월 08일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLServiceCenterEmpInSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    -- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq             ,      
                      @UserSeq                ,      
                      '_TSLServiceCenterEmpIn',     
                      '#BIZ_OUT_DataBlock1'   , -- 템프테이블명      
                      'ASEmpSeq'              , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq
                              
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
        
        -- DELETE 
        DELETE _TSLServiceCenterEmpIn    
          FROM _TSLServiceCenterEmpIn   AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ASEmpSeq = A.ASEmpSeq  
         WHERE A.CompanySeq = @CompanySeq
		   AND B.WorkingTag = 'D'   
		   AND B.Status     = 0  
           
        IF @@ERROR <> 0 RETURN      

		   
    END  
  
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  
      
       UPDATE _TSLServiceCenterEmpIn    
          SET ASEmpName        = ISNULL(B.ASEmpName, '')      ,	  
              UserSeq          = ISNULL(B.UserSeq, 0)  		  ,
              ServiceCenterSeq = ISNULL(B.ServiceCenterSeq, 0), 	
              UMJdSeq          = ISNULL(B.UMJdSeq, '0')		  ,
              EntDate          = ISNULL(B.EntDate, 0)		  ,
              RetireDate       = ISNULL(B.RetireDate, 0)	  ,
			  BirthDate        = ISNULL(B.BirthDate, 0)       ,
			  SMSexSeq         = ISNULL(B.SMSexSeq, 0) 	      ,
			  Phone            = ISNULL(dbo._FWCOMEncrypt(B.Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq), 0),
			  TelNo            = ISNULL(B.TelNo , 0)    	  ,
              Remark           = ISNULL(B.Remark, '')  		  ,
			  PgmSeq           = @PgmSeq                      , 
              LastUserSeq      = @UserSeq                     ,
              LastDateTime     = GETDATE()  
         FROM _TSLServiceCenterEmpIn   AS A 
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.ASEmpSeq = A.ASEmpSeq  
        WHERE A.CompanySeq = @CompanySeq   
		  AND B.Status     = 0  
          AND B.WorkingTag = 'U' 
  
        IF @@ERROR <> 0 RETURN   
         

    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLServiceCenterEmpIn(CompanySeq      , ASEmpSeq        , ASEmpName       , UserSeq	     , ServiceCenterSeq,
                                           UMJdSeq         , EntDate	     , RetireDate      , BirthDate       , SMSexSeq		   ,
                                           TelNo	       , Remark          , PgmSeq	       , LastUserSeq	 ,LastDateTime     ,
                                           Phone)   
        SELECT  @CompanySeq     , ASEmpSeq        , ASEmpName       , UserSeq	      ,ServiceCenterSeq,
                UMJdSeq         , EntDate         , RetireDate      , BirthDate       ,SMSexSeq		   ,
                TelNo           , Remark          , @PgmSeq         ,@UserSeq         ,GETDATE()       ,
                dbo._FWCOMEncrypt(Phone, N'_TSLServiceCenterEmpIn', 'Phone', @CompanySeq)
          FROM #BIZ_OUT_DataBlock1   
         WHERE WorkingTag = 'A' 
		   AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN    


    END

	-- INSERT, UPDATE 된 서비스센터인원의 이메일도 UPDATE 
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag IN ('A', 'U') AND Status = 0 )    
    BEGIN   

		UPDATE #BIZ_OUT_DataBlock1 
		   SET Email = B.PwdMailAdder
		  FROM #BIZ_OUT_DataBlock1 AS A
		       JOIN _TCAUser       AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
			                                        AND B.UserSeq    = A.UserSeq 
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0 


	END

RETURN