IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterQuery
GO

/********************************
 설    명 - 서비스센터등록:조회  
 작 성 일 - 2020년 04월 20일
 작 성 자 - 유태우
 수정내역 - 
*********************************/

-- SP파라미터들
CREATE PROC dbo._SWSLServiceCenterQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    -- 사용할 변수를 선언한다.
    DECLARE @ServiceCenterName  NVARCHAR(200), -- 서비스센터
            @CustSeq            INT            -- 거래처 내부코드

    -- XML의 DataBlock1으로부터 값을 가져와 변수에 저장한다.  
    SELECT @ServiceCenterName = ISNULL(ServiceCenterName,  ''),
           @CustSeq           = ISNULL(CustSeq,  0)
      FROM #BIZ_IN_DataBlock1

    -- 시트에 값을 출력하는 부분
    SELECT A.ServiceCenterName           ,
          B.CustName                     ,
          A.IsDirect                     ,
          C.WHName AS ServiceCenterWHName,
          D.WHName AS ReturnWHName       ,
          A.Remark                       ,
          A.ServiceCenterSeq             ,
          A.CustSeq                      ,
          A.ServiceCenterWHSeq           ,
          A.ReturnWHSeq                  ,
          A.EmpSeq                       ,
          E.EmpName
      FROM _TSLServiceCenter        AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDACust AS B WITH(NOLOCK) ON B.Companyseq         = A.CompanySeq
                                                     AND B.CustSeq            = A.CustSeq
           LEFT OUTER JOIN _TDAWH   AS C WITH(NOLOCK) ON C.CompanySeq         = A.CompanySeq
                                                     AND C.WHSeq              = A.ServiceCenterWHSeq
           LEFT OUTER JOIN _TDAWH   AS D WITH(NOLOCK) ON D.CompanySeq         = A.CompanySeq
                                                     AND D.WHSeq              = A.ReturnWHSeq
           LEFT OUTER JOIN _TDAEmp  AS E WITH(NOLOCK) ON E.CompanySeq         = A.CompanySeq
                                                     AND E.EmpSeq             = A.EmpSeq
      WHERE A.CompanySeq        = @CompanySeq
        AND A.ServiceCenterName LIKE @ServiceCenterName + '%'
        AND (@CustSeq = 0 OR A.CustSeq = @CustSeq)

RETURN
