IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterCheck
GO

/***********************************************************************************************
설    명 - 서비스센터등록:체크
작 성 일 - 2020년 04월 20일
작 성 자 - 유태우
수정내역 - 
***********************************************************************************************/

-- SP파라미터들
CREATE PROCEDURE _SWSLServiceCenterCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0
  
AS  

    -- 사용할 변수 선언
    DECLARE @Count       INT          ,      
            @MessageType INT          ,      
            @Status      INT          ,      
            @Results     NVARCHAR(250),  
            @Seq         INT   

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock1)
    BEGIN



        -------------------------------------------      
        -- 서비스센터 삭제 시, 해당 서비스센터의 처리기사, 관할지역이 입력되거나, 해당 서비스센터가 입력된 서비스 접수 및 처리 건이 있으면 삭제 불가
        /*
            참고화면
            처리기사 - [서비스센터인원등록]
            관할지역 - [서비스관할지역등록]
            서비스접수 - [서비스접수]
            서비스처리 - [서비스처리]
        */
        -------------------------------------------  

        -- 처리기사가 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                               @Status      OUTPUT,  
                               @Results     OUTPUT,  
                               1355               , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq       ,  
                               17907, N'처리기사' , -- SELECT * FROM _TCADictionary WHERE Word like '%처리기사%' 
                               308, N'삭제'         -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS ( SELECT TOP 1 1  
                          FROM _TSLServiceCenterEmpIn WITH(NOLOCK)
                         WHERE CompanySeq = @CompanySeq
                           AND ServiceCenterSeq = A.ServiceCenterSeq
                      )

        -- 관할지역이 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                               @Status      OUTPUT,  
                               @Results     OUTPUT,  
                               1355               , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq       ,  
                               72892, N'관할지역' , -- SELECT * FROM _TCADictionary WHERE Word like '%관할지역%' 
                               308, N'삭제'         -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS ( SELECT TOP 1 1  
                          FROM _TSLASArea WITH(NOLOCK)
                         WHERE CompanySeq = @CompanySeq
                           AND ServiceCenterSeq = A.ServiceCenterSeq
                      )

        -- 서비스접수내역이 존재할 경우 삭제 불가 
        EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
                               @Status      OUTPUT     ,  
                               @Results     OUTPUT     ,  
                               1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
                               @LanguageSeq            ,  
                               24355, N'서비스접수정보', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스접수정보%' 
                               308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
		 WHERE A.WorkingTag = 'D'
		   AND A.Status     = 0  
           AND EXISTS ( SELECT TOP 1 1 
                          FROM _TSLASServiceReceipt WITH(NOLOCK)
                         WHERE CompanySeq = @CompanySeq
                           AND ServiceCenterSeq = A.ServiceCenterSeq
                      )

        -- 서비스처리내역이 존재할 경우 삭제 불가 - 추후 테이블 생성되면 추가 
   --     EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
   --                            @Status      OUTPUT     ,  
   --                            @Results     OUTPUT     ,  
   --                            1355                    , -- @1이(가) 존재합니다. @2 후 진행하십시오.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1355)  
   --                            @LanguageSeq            ,  
   --                            72883, N'서비스처리내역', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스처리내역%' 
   --                            308, N'삭제'              -- SELECT * FROM _TCADictionary WHERE Word like '%삭제%' 

   --     UPDATE #BIZ_OUT_DataBlock1 
   --        SET Result      = @Results    , 
   --            MessageType = @MessageType,  
   --            Status      = @Status  
   --       FROM #BIZ_OUT_DataBlock1 AS A
		 --WHERE A.WorkingTag = 'D'
		 --  AND A.Status     = 0  
   --        AND EXISTS (SELECT 1 
   --                      FROM _TSLASServiceReceipt WITH(NOLOCK)
   --                     WHERE CompanySeq = @CompanySeq
   --                       AND ServiceCenterSeq = A.ServiceCenterSeq
   --                   )

        -------------------------------------------      
        --서비스센터 중복체크
        -------------------------------------------  
        EXEC dbo._SWCOMMessage @MessageType OUTPUT   ,  
                               @Status      OUTPUT   ,  
                               @Results     OUTPUT   ,  
                               6                     , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq          ,  
                               25191, N'서비스센터명', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스센터%'  
                               355, N'데이터'          -- SELECT * FROM _TCADictionary WHERE Word like '%데이터%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT S.ServiceCenterName
                        FROM ( SELECT A.ServiceCenterName
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A','U')
                                  AND A.Status = 0
                                UNION ALL
                               SELECT A.ServiceCenterName
                                 FROM _TSLServiceCenter AS A
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1 
                                                    WHERE WorkingTag        IN ('U','D')
                                                      AND Status            = 0
                                                      AND ServiceCenterSeq  = A.ServiceCenterSeq
                                                 )
                             ) AS S
                       GROUP BY S.ServiceCenterName
                      HAVING COUNT(1) > 1
                    ) AS B
                 ON B.ServiceCenterName = A.ServiceCenterName
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0  



        -------------------------------------------      
        --거래처 중복체크
        -------------------------------------------   

        EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                               @Status      OUTPUT,  
                               @Results     OUTPUT,  
                               6                  ,    -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq       ,  
                               6, N'거래처'       ,    -- SELECT * FROM _TCADictionary WHERE Word like '%거래처%'  
                               355, N'데이터'          -- SELECT * FROM _TCADictionary WHERE Word like '%데이터%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT S.CustSeq
                        FROM ( SELECT A.CustSeq
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A','U')
                                  AND A.Status = 0
                                UNION ALL
                               SELECT  A.CustSeq
                                 FROM _TSLServiceCenter AS A
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1 
                                                    WHERE WorkingTag        IN ('U','D')
                                                      AND Status            = 0
                                                      AND ServiceCenterSeq  = A.ServiceCenterSeq
                                                 )
                             ) AS S
                       GROUP BY S.CustSeq
                      HAVING COUNT(1) > 1
                    ) AS B
                 ON  A.CustSeq = B.CustSeq



        -------------------------------------------      
        --서비스센터창고 중복체크
        -------------------------------------------   

        EXEC dbo._SWCOMMessage @MessageType OUTPUT     ,  
                               @Status      OUTPUT     ,  
                               @Results     OUTPUT     ,  
                               6                       , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq            ,  
                               72701, N'서비스센터창고', -- SELECT * FROM _TCADictionary WHERE Word like '%서비스센터창고%'  
                               355, N'데이터'            -- SELECT * FROM _TCADictionary WHERE Word like '%데이터%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT S.ServiceCenterWHSeq
                        FROM ( SELECT  A.ServiceCenterWHSeq
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A','U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.ServiceCenterWHSeq
                                 FROM _TSLServiceCenter AS A
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1 
                                                    WHERE WorkingTag        IN ('U','D')
                                                      AND Status            = 0
                                                      AND ServiceCenterSeq  = A.ServiceCenterSeq
                                                 )
                             ) AS S
                       GROUP BY S.ServiceCenterWHSeq
                      HAVING COUNT(1) > 1
                    ) AS B
                 ON  A.ServiceCenterWHSeq = B.ServiceCenterWHSeq

        -------------------------------------------      
        --담당자 중복체크 
        -------------------------------------------   

        EXEC dbo._SWCOMMessage @MessageType OUTPUT   ,  
                               @Status      OUTPUT   ,  
                               @Results     OUTPUT   ,  
                               6                     , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq          ,  
                               360, N'담당자', -- SELECT * FROM _TCADictionary WHERE Word like '%담당자%'  
                               355, N'데이터'          -- SELECT * FROM _TCADictionary WHERE Word like '%데이터%'  

        UPDATE #BIZ_OUT_DataBlock1 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock1 AS A
               JOIN ( SELECT S.EmpSeq
                        FROM ( SELECT A.EmpSeq
                                 FROM #BIZ_OUT_DataBlock1 AS A
                                WHERE A.WorkingTag IN ('A','U')
                                  AND A.Status     = 0
                                  AND A.EmpSeq     <> 0
                                UNION ALL
                               SELECT A.EmpSeq
                                 FROM _TSLServiceCenter AS A
                                WHERE A.CompanySeq = @CompanySeq
                                  AND A.EmpSeq     <> 0
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock1 
                                                    WHERE WorkingTag        IN ('U','D')
                                                      AND Status            = 0
                                                      AND EmpSeq            <> 0
                                                      AND ServiceCenterSeq  = A.ServiceCenterSeq
                                                      
                                                 )
                             ) AS S
                       GROUP BY S.EmpSeq
                      HAVING COUNT(1) > 1
                    ) AS B
                 ON  A.EmpSeq = B.EmpSeq
        WHERE A.EmpSeq <> 0 

  END

    -------------------------------------------      
    -- INSERT 번호부여(맨 마지막 처리) 
    -------------------------------------------         

    SELECT  @Count = COUNT(1) FROM #BIZ_OUT_DataBlock1 WHERE (WorkingTag = 'A' AND Status = 0)  

    IF(@Count > 0)  
    BEGIN  
            
        -- 키값생성코드부분 시작  
        EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLServiceCenter', 'ServiceCenterSeq', @Count  
  
        -- Temp Table 에 생성된 키값 UPDATE  
        UPDATE #BIZ_OUT_DataBlock1  
           SET ServiceCenterSeq = @Seq + A.DataSeq
          FROM #BIZ_OUT_DataBlock1 AS A 
         WHERE A.WorkingTag = 'A' 
           AND A.Status = 0  

    
    END  

RETURN
