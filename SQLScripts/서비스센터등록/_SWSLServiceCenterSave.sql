IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLServiceCenterSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLServiceCenterSave
GO

/******************************    
설  명 - 서비스센터등록:저장
작성일 - 2020년 04월 20일    
작성자 - 유태우    
수정일 - 
*******************************/   
  
CREATE PROCEDURE _SWSLServiceCenterSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0  

AS    

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq          ,      
                      @UserSeq             ,      
                      '_TSLServiceCenter'  , -- 원테이블명 _TPRBasPulog      
                      '#BIZ_OUT_DataBlock1', -- 템프테이블명      
                      'ServiceCenterSeq'   , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq
            
    --DELETE
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
  
        -- 신청내역 삭제  
        DELETE _TSLServiceCenter    
          FROM _TSLServiceCenter        AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ServiceCenterSeq = A.ServiceCenterSeq  
         WHERE A.CompanySeq = @CompanySeq  
           AND B.WorkingTag = 'D' 
           AND B.Status     = 0  
        
        IF @@ERROR <> 0 RETURN  
      
               
    END  
  
    --UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  
  
       UPDATE _TSLServiceCenter    
          SET ServiceCenterName  = ISNULL(B.ServiceCenterName, ''),
              CustSeq            = ISNULL(B.CustSeq, 0)           ,
              EmpSeq             = ISNULL(B.EmpSeq , 0)           ,
              IsDirect           = ISNULL(B.IsDirect, '0')        ,
              ServiceCenterWHSeq = ISNULL(B.ServiceCenterWHSeq, 0),
              ReturnWHSeq        = ISNULL(B.ReturnWHSeq , 0)      ,
              Remark             = ISNULL(B.Remark, '')           ,
              LastUserSeq        = @UserSeq                       ,
              LastDateTime       = GETDATE()  
         FROM _TSLServiceCenter        AS A    
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.ServiceCenterSeq = A.ServiceCenterSeq  
        WHERE A.CompanySeq = @CompanySeq  
          AND B.WorkingTag = 'U' 
          AND B.Status     = 0  

  
        IF @@ERROR <> 0 RETURN  
       
  
    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLServiceCenter(CompanySeq        , ServiceCenterSeq  , ServiceCenterName , CustSeq           , EmpSeq            ,
                                      IsDirect          , ServiceCenterWHSeq, ReturnWHSeq       , Remark            , PgmSeq            ,
                                      LastUserSeq       , LastDateTime)   
        SELECT  @CompanySeq       , ServiceCenterSeq  , ServiceCenterName , CustSeq           , EmpSeq            ,
                IsDirect          , ServiceCenterWHSeq, ReturnWHSeq       , Remark            , @PgmSeq           ,
                @UserSeq          ,  GETDATE()  
          FROM #BIZ_OUT_DataBlock1   
         WHERE WorkingTag = 'A' 
           AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN  
   
  
    END  

RETURN

