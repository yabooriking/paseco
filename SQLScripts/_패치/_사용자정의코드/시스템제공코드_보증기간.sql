INSERT INTO _TDASMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorValue,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100041001,100041,N'보증',N'',0,N'',29296,27485,'2020-05-19 08:59:41.513',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDASMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100041001)
INSERT INTO _TDASMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorValue,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100041002,100041,N'보증 외',N'',0,N'',72889,27485,'2020-05-20 09:28:05.860',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDASMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100041002)