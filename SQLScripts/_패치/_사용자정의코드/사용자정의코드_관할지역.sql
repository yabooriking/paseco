
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116001,100116,N'서울북부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116001)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116002,100116,N'서울남부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116002)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116003,100116,N'서울서부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116003)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116004,100116,N'서울동부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116004)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116005,100116,N'경기북부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116005)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116006,100116,N'경기남부',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116006)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116007,100116,N'인천',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116007)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116008,100116,N'충북',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116008)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116009,100116,N'충남',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116009)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116010,100116,N'전북',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116010)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116011,100116,N'전남',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116011)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116012,100116,N'경북',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116012)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100116013,100116,N'경남',0,N'',0,27485,'2020-05-14 16:55:03.013',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100116013)
