INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115001,100115,N'직접입력',0,N'삭제X',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115001)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115002,100115,N'naver.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115002)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115003,100115,N'gmail.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115003)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115004,100115,N'hanmail.net',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115004)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115005,100115,N'daum.net',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115005)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115006,100115,N'nate.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115006)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115007,100115,N'korea.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115007)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115008,100115,N'paran.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115008)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115009,100115,N'lycos.co.kr',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115009)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115010,100115,N'hotmail.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115010)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100115011,100115,N'msn.com',0,N'',0,27485,'2020-05-14 16:54:13.930',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100115011)
