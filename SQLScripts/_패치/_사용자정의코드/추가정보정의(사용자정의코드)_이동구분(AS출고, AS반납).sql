GetINsertScriptTableNULL '', '_TCOMUserDefine'

SELECT 'INSERT INTO _TCOMUserDefine(CompanySeq,TableName,DefineUnitSeq,TitleSerl,Title,IsEss,CodeHelpConst,CodeHelpParams,SMInputType,MaskAndCaption,QrySort,IsFix,DataFieldID,IsCodeHelpTitle,LastUserSeq,LastDateTime,DecLen,WordSeq)' + CHAR(13) + 'SELECT '  + CONVERT(VARCHAR(30), ISNULL(CompanySeq, 0)) + ','   + 'N''' + ISNULL(RTRIM(REPLACE(TableName, '''', '''''')), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(DefineUnitSeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(TitleSerl, 0)) + ','   + CASE WHEN Title IS NULL THEN 'NULL, ' ELSE 'N''' + REPLACE(Title, '''', '''''') + ''',' END   + 'N''' + ISNULL(RTRIM(REPLACE(IsEss, '''', '''''')), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(CodeHelpConst, 0)) + ','   + CASE WHEN CodeHelpParams IS NULL THEN 'NULL, ' ELSE 'N''' + REPLACE(CodeHelpParams, '''', '''''') + ''',' END   + CONVERT(VARCHAR(30), ISNULL(SMInputType, 0)) + ','   + 'N''' + ISNULL(RTRIM(REPLACE(MaskAndCaption, '''', '''''')), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(QrySort, 0)) + ','   + 'N''' + ISNULL(RTRIM(REPLACE(IsFix, '''', '''''')), '') + ''','   + 'N''' + ISNULL(RTRIM(REPLACE(DataFieldID, '''', '''''')), '') + ''','   + 'N''' + ISNULL(RTRIM(REPLACE(IsCodeHelpTitle, '''', '''''')), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(LastUserSeq, 0)) + ','   + '''' + CONVERT(VARCHAR(30), ISNULL(LastDateTime, ''), 121) + ''','   + CASE WHEN DecLen IS NULL THEN 'NULL, ' ELSE CONVERT(VARCHAR(30), DecLen) + ',' END   + CASE WHEN WordSeq IS NULL THEN 'NULL ' ELSE CONVERT(VARCHAR(30), WordSeq) END   FROM _TCOMUserDefine
WHERE TableName = '_TDAUMajor' AND DefineUnitSeq = 8012

INSERT INTO _TCOMUserDefine(CompanySeq,TableName,DefineUnitSeq,TitleSerl,Title,IsEss,CodeHelpConst,CodeHelpParams,SMInputType,MaskAndCaption,QrySort,IsFix,DataFieldID,IsCodeHelpTitle,LastUserSeq,LastDateTime,DecLen,WordSeq) 
SELECT @CompanySeq ,N'_TDAUMajor',8012,1001,N'이동',N'0',0,N'',1027006,N'',1,N'1',N'',N'0',31,'2013-05-29 17:35:01.940',NULL, 36241
INSERT INTO _TCOMUserDefine(CompanySeq,TableName,DefineUnitSeq,TitleSerl,Title,IsEss,CodeHelpConst,CodeHelpParams,SMInputType,MaskAndCaption,QrySort,IsFix,DataFieldID,IsCodeHelpTitle,LastUserSeq,LastDateTime,DecLen,WordSeq) 
SELECT 1,N'_TDAUMajor',8012,1002,N'적송',N'0',0,N'',1027006,N'',2,N'1',N'',N'0',31,'2013-05-29 17:35:01.940',NULL, 53922





INSERT INTO _TCOMUserDefine(CompanySeq,TableName,DefineUnitSeq,TitleSerl,Title,IsEss,CodeHelpConst,CodeHelpParams,SMInputType,MaskAndCaption,QrySort,IsFix,DataFieldID,IsCodeHelpTitle,LastUserSeq,LastDateTime,DecLen,WordSeq) 
SELECT A.CompanySeq,N'_TDAUMajor',8012,10000001,N'AS출고 ',N'0',0,N'',1027006,N'',3,N'0',N'',N'0',27485,'2020-05-15 10:45:10.000',0,23590
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TCOMUserDefine WITH(NOLOCK)
                     WHERE CompanySeq    = A.CompanySeq 
                       AND TableName     = '_TDAUMajor'
                       AND DefineUnitSeq = 8012
                       AND TitleSerl     = 10000001
                  )

INSERT INTO _TCOMUserDefine(CompanySeq,TableName,DefineUnitSeq,TitleSerl,Title,IsEss,CodeHelpConst,CodeHelpParams,SMInputType,MaskAndCaption,QrySort,IsFix,DataFieldID,IsCodeHelpTitle,LastUserSeq,LastDateTime,DecLen,WordSeq) 
SELECT 1,N'_TDAUMajor',8012,10000002,N'AS반납',N'0',0,N'',1027006,N'',4,N'0',N'',N'0',27485,'2020-05-15 10:45:10.000',0,0
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TCOMUserDefine WITH(NOLOCK)
                     WHERE CompanySeq    = A.CompanySeq 
                       AND TableName     = '_TDAUMajor'
                       AND DefineUnitSeq = 8012
                       AND TitleSerl     = 10000002
                  )


