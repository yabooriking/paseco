INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100114001,100114,N'A',0,N'',0,27485,'2020-05-14 16:53:41.567',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100114001)

INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100114002,100114,N'B',0,N'',0,27485,'2020-05-14 16:53:41.567',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100114002)
INSERT INTO _TDAUMinor(CompanySeq,MinorSeq,MajorSeq,MinorName,MinorSort,Remark,WordSeq,LastUserSeq,LastDateTime,IsUse) 
SELECT A.CompanySeq,100114003,100114,N'C',0,N'',0,27485,'2020-05-14 16:53:41.567',N'1'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TDAUMinor WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND MinorSeq   = 100114003)