INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,199,N'_TSLFPShoppingMallEnv',N'AuthKey',1,'2020-05-22 18:18:30.813'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,200,N'_TSLFPShoppingMallOrder',N'OrdTelNo',1,'2020-05-22 18:18:30.813'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,201,N'_TSLFPShoppingMallOrder',N'OrdPhone',1,'2020-05-22 18:18:30.813'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,202,N'_TSLFPShoppingMallOrder',N'RecvTelNo',1,'2020-05-22 18:18:30.813'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,203,N'_TSLFPShoppingMallOrder',N'RecvPhone',1,'2020-05-22 18:18:30.813'


---- 아래의 Query 참고 
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime)
SELECT A.CompanySeq, 199,N'_TSLFPShoppingMallEnv',N'AuthKey',1,'2020-05-22 18:18:30.813'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 199)
 UNION 
SELECT A.CompanySeq,200,N'_TSLFPShoppingMallOrder',N'OrdTelNo',1,'2020-05-22 18:18:30.813'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 200)
 UNION 
SELECT A.CompanySeq,201,N'_TSLFPShoppingMallOrder',N'OrdPhone',1,'2020-05-22 18:18:30.813'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 201)
 UNION 
SELECT A.CompanySeq,202,N'_TSLFPShoppingMallOrder',N'RecvTelNo',1,'2020-05-22 18:18:30.813'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 202)
 UNION 
SELECT A.CompanySeq,203,N'_TSLFPShoppingMallOrder',N'RecvPhone',1,'2020-05-22 18:18:30.813'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 203)
