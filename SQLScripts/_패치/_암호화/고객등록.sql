INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,196,N'_TSLASClient',N'Phone',1,'2020-05-12 15:01:58.953'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,197,N'_TSLASClient',N'TelNo',1,'2020-05-12 15:01:58.953'
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime) SELECT 1,198,N'_TSLServiceCenterEmpIn',N'Phone',1,'2020-05-13 11:32:31.480'

---- 아래의 Query 참고 
INSERT INTO _TWCOMEncryptInfo(CompanySeq,Seq,TableName,ColumnName,LastUserSeq,LastDateTime)
SELECT A.CompanySeq,196,N'_TSLASClient',N'Phone',1,'2020-05-12 15:01:58.953'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 196)
 UNION 
SELECT A.CompanySeq,197,N'_TSLASClient',N'TelNo',1,'2020-05-12 15:01:58.953'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 197)
 UNION 
SELECT A.CompanySeq,198,N'_TSLServiceCenterEmpIn',N'Phone',1,'2020-05-13 11:32:31.480'
  FROM _TCACompany AS A WITH(NOLOCK)
 WHERE NOT EXISTS ( SELECT 1
                      FROM _TWCOMEncryptInfo WITH(NOLOCK)
                     WHERE CompanySeq = A.CompanySeq 
                       AND Seq        = 198)
