﻿--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
-------------------------------------------------------------------------------------------------------------
--서비스재고조회
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionaryCommon WHERE WordSeq = 65687) 
BEGIN 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 65687, N'서비스재고조회', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 65687, N'Service Inventory Inquiry', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 65687, N'サービス在庫照会', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 65687, N'服务中心库存查询', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 65687, N'服务中心库存查询', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 65687, N'서비스재고조회', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 65687, N'서비스재고조회', NULL, N'서비스재고조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 65687, N'서비스재고조회', NULL, N'서비스재고조회', 0, GETDATE(), 0 
END 

--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
--AS자재출고품목조회
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionaryCommon WHERE WordSeq = 65688) 
BEGIN 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 65688, N'AS자재출고품목조회', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 65688, N'AS Material GI Item Inquiry', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 65688, N'AS資材出庫品目照会', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 65688, N'AS材料出库品目查询', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 65688, N'AS材料出庫品目查詢', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 65688, N'AS자재출고품목조회', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 65688, N'AS자재출고품목조회', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 65688, N'AS자재출고품목조회', NULL, N'AS자재출고품목조회', 0, GETDATE(), 0 
END 


--------------------------------------------------------------------------------------------------------------
--운영사전 (프로그램명/이벤트명/코드도움명/프로세스명) _TCADictionaryCommon
--------------------------------------------------------------------------------------------------------------
--서비스센터인원등록
IF NOT EXISTS(SELECT TOP 1 1 FROM _TCADictionaryCommon WHERE WordSeq = 65689) 
BEGIN 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 1, 65689, N'서비스센터인원등록', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 2, 65689, N'Service Center Personnel Reg.', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 3, 65689, N'서비스센터인원등록', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 4, 65689, N'服务中心人员登记', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 5, 65689, N'服務中心人員登記', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 6, 65689, N'서비스센터인원등록', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 7, 65689, N'서비스센터인원등록', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
  INSERT INTO _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) SELECT 8, 65689, N'서비스센터인원등록', NULL, N'서비스센터인원등록', 0, GETDATE(), 0 
END 

