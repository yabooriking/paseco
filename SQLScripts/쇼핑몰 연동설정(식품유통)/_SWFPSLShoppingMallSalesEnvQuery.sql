IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallSalesEnvQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallSalesEnvQuery
GO

/********************************
 설    명 - 쇼핑몰 연동설정:조회
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallSalesEnvQuery
  
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS    

	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen
    
    SELECT B.MinorName AS ShopItemColName    ,
           C.MinorName AS ERPItemColName     ,
           D.MinorName AS InOutDetailKindName,
           E.MinorName AS UMOrderKindName    ,
           A.CompanyID                       ,
           A.ShopItemColSeq                  ,
           A.ERPItemColSeq                   ,
           A.InOutDetailKind                 ,
           A.UMOrderKind                     , 
           dbo._FWCOMDecrypt(A.AuthKey, '_TSLFPShoppingMallEnv', 'AuthKey', @CompanySeq) AS AuthKey
      FROM _TSLFPShoppingMallEnv AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDASMinor AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq
                                                       AND B.MinorSeq   = A.ShopItemColSeq
           LEFT OUTER JOIN _TDASMinor AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq
                                                       AND C.MinorSeq   = A.ERPItemColSeq
           LEFT OUTER JOIN _TDAUMinor AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq           
                                                       AND D.MinorSeq   = A.UMOrderKind
           LEFT OUTER JOIN _TDAUMinor AS E WITH(NOLOCK) ON E.CompanySeq = A.CompanySeq           
                                                       AND E.MinorSeq   = A.InOutDetailKind
     WHERE A.CompanySeq = @CompanySeq
    
RETURN