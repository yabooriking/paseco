IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallSave
GO

/********************************
 설    명 - 쇼핑몰 연동설정:쇼핑몰저장
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS    

    -- 로그테이블남기기(마지막파라메터는반드시한줄로보내기)
    EXEC _SCOMXmlLog  @CompanySeq          ,
                      @UserSeq             ,
                      '_TSLFPShoppingMall' , -- 원테이블명
                      '#BIZ_OUT_DataBlock2', -- 템프테이블명
                      'MallSeq' ,            -- 키가여러개일경우는, 로연결한다.
                      @PgmSeq,
                      ''                     -- TempTable과 Table의 명칭이 다를경우 

    -- DELETE
    IF EXISTS (SELECT  1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'D' AND Status = 0)
    BEGIN

        DELETE _TSLFPShoppingMall
          FROM _TSLFPShoppingMall       AS A WITH(NOLOCK)
               JOIN #BIZ_OUT_DataBlock2 AS B ON A.MallSeq = B.MallSeq
         WHERE A.CompanySeq  = @CompanySeq
           AND B.WorkingTag = 'D'
           AND B.Status     = 0

        IF @@ERROR <> 0  RETURN


    END

    -- UPDATE
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'U' AND Status = 0)
    BEGIN

        UPDATE _TSLFPShoppingMall
           SET MallName     = ISNULL(B.MallName, ''),
               MallID       = ISNULL(B.MallID, '')  ,
               CustSeq      = ISNULL(B.CustSeq, 0)  ,
               WHSeq        = ISNULL(B.WHSeq, 0)    ,
               ERPWHSeq     = ISNULL(B.ERPWHSeq, 0) ,
               LastUserSeq  = @UserSeq              ,
               LastDateTime = GETDATE()
          FROM _TSLFPShoppingMall       AS A WITH(NOLOCK)
               JOIN #BIZ_OUT_DataBlock2 AS B ON A.MallSeq = B.MallSeq
         WHERE A.CompanySeq = @CompanySeq
           AND B.WorkingTag = 'U'
           AND B.Status = 0

        IF @@ERROR <> 0  RETURN


    END

    -- INSERT
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A' AND Status = 0)
    BEGIN

        INSERT INTO _TSLFPShoppingMall(CompanySeq , MallSeq    , MallName   , MallID     , CustSeq    ,
                                       WHSeq      , ERPWHSeq   , LastUserSeq, LastDateTime)
        SELECT @CompanySeq           , A.MallSeq             , ISNULL(A.MallName, ''), ISNULL(A.MallID, ''), ISNULL(A.CustSeq,0),
               ISNULL(A.WHSeq, 0)    , ISNULL(A.ERPWHSeq, 0) , @UserSeq              , GETDATE()
          FROM #BIZ_OUT_DataBlock2 AS A
         WHERE A.WorkingTag = 'A'
           AND A.Status = 0

        IF @@ERROR <> 0 RETURN


    END

RETURN
