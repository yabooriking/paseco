IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallQuery
GO

/********************************
 ��    �� - ���θ� ��������:���θ���ȸ
 �� �� �� - 2020�� 05�� 22��
 �� �� �� - ����ȣ
 �������� - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallQuery  
    @ServiceSeq     INT     = 0,
    @WorkingTag     NVARCHAR(10)= '',
    @CompanySeq     INT     = 1,
    @LanguageSeq    INT     = 1,
    @UserSeq        INT     = 0,
    @PgmSeq         INT     = 0,
    @IsTransaction  BIT     = 0   
AS    

    SELECT A.MallName           ,
           A.MallID             ,
           B.CustName           ,
           C.WHName             ,
           D.WHName AS ERPWHName,
           A.CustSeq            ,
           A.WHSeq              ,
           A.ERPWHSeq           ,
           A.MallSeq     
      FROM _TSLFPShoppingMall AS A WITH(NOLOCK)
           JOIN _TDACust      AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq
                                               AND B.CustSeq    = A.CustSeq
           JOIN _TDAWH        AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq
                                               AND C.WHSeq      = A.WHSeq
           JOIN _TDAWH        AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq
                                               AND D.WHSeq      = A.ERPWHSeq
     WHERE A.CompanySeq = @CompanySeq 
     ORDER BY REPLACE(A.MallID,' ','')
    
RETURN