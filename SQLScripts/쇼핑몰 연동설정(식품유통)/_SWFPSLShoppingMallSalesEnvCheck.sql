IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallSalesEnvCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallSalesEnvCheck
GO

/********************************
 설    명 - 쇼핑몰 연동설정:체크
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallSalesEnvCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

RETURN
