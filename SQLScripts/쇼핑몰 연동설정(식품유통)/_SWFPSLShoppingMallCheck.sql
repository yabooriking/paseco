IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallCheck
GO

/********************************
 설    명 - 쇼핑몰 연동설정:쇼핑몰체크
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @Count        INT          ,
            @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT  

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock2)
    BEGIN

		-------------------------------------------      
		-- 쇼핑몰ID - 쇼핑몰명 중복 체크 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT  ,  
                               @Status      OUTPUT  ,  
                               @Results     OUTPUT  ,  
                               6                    , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq         ,  
                               10000073, N'쇼핑몰ID', -- SELECT * FROM _TCADictionary WHERE Word like '%쇼핑몰ID%'   
							   10000074, N'쇼핑몰'    -- SELECT * FROM _TCADictionary WHERE Word like '%쇼핑몰%' 

        UPDATE #BIZ_OUT_DataBlock2 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock2 AS A
               JOIN ( SELECT  S.MallID, S.MallName
                        FROM (
                               SELECT A.MallID, A.MallName
                                 FROM #BIZ_OUT_DataBlock2 AS A
                                WHERE A.WorkingTag IN ('A', 'U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.MallID, A.MallName
                                 FROM _TSLFPShoppingMall AS A WITH(NOLOCK)
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock2
                                                    WHERE WorkingTag IN ('A', 'U')
                                                      AND Status	 = 0
                                                      AND MallSeq   = A.MallSeq 
                                                 )
					         ) AS S
				       GROUP BY S.MallID, S.MallName
				      HAVING COUNT(1) > 1
                    ) AS B
                 ON B.MallID    = A.MallID
				AND B.MallName = A.MallName
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0  

        -- 출고매출 거래처 등록 불가 체크
        EXEC dbo._SCOMMessage @MessageType OUTPUT,
                              @Status      OUTPUT,
                              @Results     OUTPUT,
                              1342               , -- @1 @2 @3 은(는) 저장할 수 없습니다. (SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1342)
                              @LanguageSeq       ,
                              23566, N'출고매출' , -- SELECT * FROM _TCADictionary WHERE WordSeq = 23566
                              24260, N'설정'     , -- SELECT * FROM _TCADictionary WHERE WordSeq = 24260
                              6    , N'거래처'      -- SELECT * FROM _TCADictionary WHERE WordSeq = 6    

        UPDATE #BIZ_OUT_DataBlock2
           SET Result      = @Results    ,
               MessageType = @MessageType,
               Status      = @Status
          FROM #BIZ_OUT_DataBlock2           AS A
               JOIN _TDACustSalesReceiptCond AS B WITH(NOLOCK) ON B.CompanySeq = @CompanySeq
                                                              AND B.CustSeq    = A.CustSeq
         WHERE A.WorkingTag   IN ('A', 'U')
           AND A.Status       = 0
           AND B.SMSalesPoint <> 8017002  
           
                   
    END

    -------------------------------------------      
    -- INSERT 번호부여(맨 마지막 처리) 
    -------------------------------------------  
    SELECT @Count = COUNT(1) FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A'  --@Count값수정(AND Status = 0 제외) )

    IF @Count > 0
    BEGIN

        EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLFPShoppingMall', 'MallSeq', @Count

        UPDATE #BIZ_OUT_DataBlock2
           SET MallSeq   = @Seq + DataSeq
          FROM #BIZ_OUT_DataBlock2 AS A
         WHERE A.WorkingTag = 'A'
           AND A.Status = 0


    END

RETURN
