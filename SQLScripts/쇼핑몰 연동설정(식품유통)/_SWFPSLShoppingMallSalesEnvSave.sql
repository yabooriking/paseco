IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWFPSLShoppingMallSalesEnvSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWFPSLShoppingMallSalesEnvSave
GO

/********************************
 설    명 - 쇼핑몰 연동설정:저장
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWFPSLShoppingMallSalesEnvSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0   
AS    

  
	-- _FWCOMDecrypt, _FWCOMEncrypt Fuction을 사용하기 위해 아래 SP 실행
	EXEC _SYMMETRICKeyOpen
          
    -- 로그테이블남기기(마지막파라메터는반드시한줄로보내기)
    EXEC _SCOMXmlLog  @CompanySeq          ,
                      @UserSeq             ,
                      '_TSLFPShoppingMallEnv',   -- 원테이블명
                      '#BIZ_OUT_DataBlock1',   -- 템프테이블명
                      'CompanyID'          ,   -- 키가여러개일경우는, 로연결한다.
                      @PgmSeq              ,
                      ''                       -- TempTable과 Table의 명칭이 다를경우 
                      
    -- 컨틀로은 있으면 무조건 업데이트
    UPDATE _TSLFPShoppingMallEnv
       SET CompanyID       = ISNULL(B.CompanyID, '')     ,
           ShopItemColSeq  = ISNULL(B.ShopItemColSeq, 0) ,
           ERPItemColSeq   = ISNULL(B.ERPItemColSeq, 0)  ,
           InOutDetailKind = ISNULL(B.InOutDetailKind, 0),
           UMOrderKind     = ISNULL(B.UMOrderKind, 0)    ,
           LastUserSeq     = @UserSeq                    ,
           LastDateTime    = GETDATE()                   ,
           AuthKey         = dbo._FWCOMEncrypt(B.AuthKey, '_TSLFPShoppingMallEnv', 'AuthKey', @CompanySeq)
      FROM _TSLFPShoppingMallEnv    AS A WITH(NOLOCK)
           JOIN #BIZ_OUT_DataBlock1 AS B ON(1 = 1)
     WHERE A.CompanySeq = @CompanySeq

    IF @@ERROR <> 0  RETURN
        
    -- 데이터 없으면 INSERT
    INSERT INTO _TSLFPShoppingMallEnv(CompanySeq    , ShopItemColSeq, ERPItemColSeq , InOutDetailKind , UMOrderKind     ,
                                      CompanyID     , LastUserSeq   , LastDateTime  , AuthKey)
    SELECT @CompanySeq   , ShopItemColSeq, ERPItemColSeq , InOutDetailKind , UMOrderKind     ,
           CompanyID     , @UserSeq      , GETDATE()     , dbo._FWCOMEncrypt(AuthKey, '_TSLFPShoppingMallEnv', 'AuthKey', @CompanySeq)
      FROM #BIZ_OUT_DataBlock1 AS A
     WHERE A.Status = 0
       AND NOT EXISTS( SELECT 1 
                         FROM _TSLFPShoppingMallEnv 
                        WHERE CompanySeq = @CompanySeq
                     )

    IF @@ERROR <> 0 RETURN

RETURN
