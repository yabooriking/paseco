IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASClientConsultInfoQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASClientConsultInfoQuery
GO

/********************************
 설    명 - 고객상담정보:조회
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASClientConsultInfoQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    
    DECLARE @ClientSeq        INT,
            @ClientConsultSeq INT 

    SELECT @ClientSeq        = ISNULL(ClientSeq, 0),
	       @ClientConsultSeq = ISNULL(ClientConsultSeq, 0) 
	  FROM #BIZ_IN_DataBlock1

	SELECT B.ClientName                 ,
           C.MinorName AS ItemClassSName,
           A.ItemClassSSeq              ,               
           D.MinorName AS UMTendencyName,
           A.UMTendency                 ,
           E.UserName AS RegUserName    ,
           A.RegUserSeq                 ,
           A.Content                    ,
           A.ClientConsultSeq           ,
           A.LastDateTime               ,
           --CONVERT(NCHAR(8), A.LastDateTime, 112) AS LastDateTime
           F.UserName AS LastUserName   

	  FROM _TSLASClientConsultInfo      AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TSLASClient AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq 
                                                         AND B.ClientSeq  = A.ClientSeq 
           LEFT OUTER JOIN _TDAUMinor   AS C WITH(NOLOCK) ON C.CompanySeq = A.CompanySeq 
                                                         AND C.MinorSeq   = A.ItemClassSSeq 
           LEFT OUTER JOIN _TDAUMinor   AS D WITH(NOLOCK) ON D.CompanySeq = A.CompanySeq 
                                                         AND D.MinorSeq   = A.UMTendency 
           LEFT OUTER JOIN _TCAUser     AS E WITH(NOLOCK) ON E.CompanySeq = A.CompanySeq 
                                                         AND E.UserSeq    = A.RegUserSeq
           LEFT OUTER JOIN _TCAUser     AS F WITH(NOLOCK) ON F.CompanySeq = A.CompanySeq 
                                                         AND F.UserSeq    = A.RegUserSeq
     WHERE A.CompanySeq = @CompanySeq
       AND (A.ClientConsultSeq = @ClientConsultSeq OR (@ClientConsultSeq = 0 AND A.ClientSeq = @ClientSeq))

RETURN

