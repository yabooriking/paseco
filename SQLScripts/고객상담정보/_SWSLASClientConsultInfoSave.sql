IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASClientConsultInfoSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASClientConsultInfoSave
GO

/********************************
 설    명 - 고객상담정보:저장
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASClientConsultInfoSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    DECLARE @UserName NVARCHAR(100) 


    SELECT @UserName = UserName
      FROM _TCAUser AS A WITH(NOLOCK)
     WHERE CompanySeq = @CompanySeq
       AND UserSeq    = @UserSeq

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq              ,      
                      @UserSeq                 ,      
                      '_TSLASClientConsultInfo',     
                      '#BIZ_OUT_DataBlock1'    , -- 템프테이블명      
                      'ClientConsultSeq'       , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq
                              
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
        
        -- DELETE 
        DELETE _TSLASClientConsultInfo    
          FROM _TSLASClientConsultInfo   AS A    
               JOIN #BIZ_OUT_DataBlock1 AS B ON B.ClientConsultSeq = A.ClientConsultSeq  
         WHERE A.CompanySeq = @CompanySeq
		   AND B.WorkingTag = 'D'   
		   AND B.Status     = 0  
           
        IF @@ERROR <> 0 RETURN      

		   
    END    
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  
      
       UPDATE _TSLASClientConsultInfo    
          SET UMTendency    = ISNULL(B.UMTendency, 0)   , 	  
              ItemClassSSeq = ISNULL(B.ItemClassSSeq, 0), 
              Content       = ISNULL(B.Content, 0)      , 
              LastUserSeq        =@UserSeq             ,
              LastDateTime       =GETDATE()
         FROM _TSLASClientConsultInfo   AS A 
              JOIN #BIZ_OUT_DataBlock1 AS B ON B.ClientConsultSeq = A.ClientConsultSeq  
        WHERE A.CompanySeq = @CompanySeq   
		  AND B.Status     = 0  
          AND B.WorkingTag = 'U' 
  
        IF @@ERROR <> 0 RETURN   
         

    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    
  
        INSERT INTO _TSLASClientConsultInfo(CompanySeq      , ClientConsultSeq, ClientSeq       , UMTendency      , ItemClassSSeq   ,
                                            Content         , RegUserSeq      , RegDateTime     , LastUserSeq     , LastDateTime)

             SELECT @CompanySeq     , ClientConsultSeq, ClientSeq       , UMTendency      , ItemClassSSeq   ,
                    Content         , @UserSeq        , GETDATE()       , @UserSeq        , GETDATE() 

              FROM #BIZ_OUT_DataBlock1   
             WHERE WorkingTag = 'A' 
		       AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN    


    END

    UPDATE #BIZ_OUT_DataBlock1
       SET RegDateTime  = CONVERT(NVARCHAR(100), B.RegDateTime,20),
           LastDateTime = CONVERT(NVARCHAR(100), B.LastDateTime,20),
           LastUserName = @UserName
      FROM #BIZ_OUT_DataBlock1          AS A 
           JOIN _TSLASClientConsultInfo AS B ON B.CompanySeq       = @CompanySeq
                                            AND B.ClientConsultSeq = A.ClientConsultSeq

     WHERE A.WorkingTag IN ('A', 'U')
       AND A.Status     = 0

RETURN

