CREATE TABLE _TSLASClientConsultInfo (
    CompanySeq       INT          ,
    ClientConsultSeq INT          ,
    ClientSeq        INT          ,
    UMTendency       INT          ,
    ItemClassSSeq    INT          ,
    Content          NVARCHAR(400),
    RegUserSeq       INT          ,
    RegDateTime      DATETIME     ,
    LastUserSeq      INT          ,
    LastDateTime     DATETIME     )

CREATE CLUSTERED INDEX IDX_TSLASClientConsultInfo ON _TSLASClientConsultInfo (CompanySeq, ClientConsultSeq)
IF NOT EXISTS (SELECT 1 FROM _TCOMTableLogInfo WHERE TableSeq = 521079)BEGIN	INSERT _TCOMTableLogInfo (TableName, CompanySeq, TableSeq, UseLog, LastUserSeq, LastDateTime)	SELECT '_TSLASClientConsultInfo', 1, 521079, '1', 1, GETDATE()END