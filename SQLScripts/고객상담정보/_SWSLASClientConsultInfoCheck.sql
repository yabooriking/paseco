IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASClientConsultInfoCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASClientConsultInfoCheck
GO

/********************************
 설    명 - 고객상담정보:체크
 작 성 일 - 2020년 05월 19일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASClientConsultInfoCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT          


    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock1)
    BEGIN

	-------------------------------------------      
    -- 존재하지 않는 상담정보를 삭제할 때 메시지 표시 
    -------------------------------------------  
    EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                           @Status      OUTPUT,  
                           @Results     OUTPUT,  
                           1365               , -- @1이(가) 존재하지 않습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1365)  
                           @LanguageSeq       ,  
                           72882, N'상담정보'   -- SELECT * FROM _TCADictionary WHERE Word like '%상담정보%' 


     UPDATE #BIZ_OUT_DataBlock1 
        SET Result      = @Results    , 
            MessageType = @MessageType,  
            Status      = @Status  
       FROM #BIZ_OUT_DataBlock1 AS A
	  WHERE A.WorkingTag = 'D'
	    AND A.Status     = 0  
        AND NOT EXISTS ( SELECT TOP 1 1 
                           FROM _TSLASClientConsultInfo WITH(NOLOCK)
                          WHERE CompanySeq       = @CompanySeq
                            AND ClientConsultSeq = A.ClientConsultSeq 
                       ) 


		-------------------------------------------      
		-- INSERT 번호부여(맨 마지막 처리) 
		-------------------------------------------         

		IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock1 WHERE WorkingTag = 'A' AND Status = 0)  
		BEGIN  

			-- 키값생성코드부분 시작  
			EXEC @Seq = dbo._SWCOMCreateSeq @CompanySeq, '_TSLASClientConsultInfo', 'ClientConsultSeq', 1 
  
			-- Temp Table 에 생성된 키값 UPDATE  
			UPDATE #BIZ_OUT_DataBlock1  
			   SET ClientConsultSeq = @Seq + 1
			  FROM #BIZ_OUT_DataBlock1 AS A
			 WHERE A.WorkingTag = 'A' 
			   AND A.Status     = 0  

  
		END  


    END

RETURN
