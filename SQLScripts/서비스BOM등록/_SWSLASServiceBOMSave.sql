IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceBOMSave') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceBOMSave
GO

/********************************
 설    명 - 서비스BOM등록:저장
 작 성 일 - 2020년 05월 20일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceBOMSave
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

    -- 로그테이블 남기기(마지막 파라메터는 반드시 한줄로 보내기)      
    EXEC _SCOMXMLLog  @CompanySeq                ,      
                      @UserSeq                   ,      
                      '_TSLASServiceBOM',        
                      '#BIZ_OUT_DataBlock2'      , -- 템프테이블명      
                      'ItemSeq, MatItemSeq'      , -- 키가 여러개일 경우는 , 로 연결한다.      
                      @PgmSeq                    ,
                      'ItemSeqOld, MatItemSeqOld'  -- 템프테이블의 키 칼럼명
                      
                         
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'D' AND Status = 0)    
    BEGIN    
        
        -- DELETE 
        DELETE _TSLASServiceBOM    
          FROM _TSLASServiceBOM         AS A    
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.ItemSeqOld    = A.ItemSeq
                                            AND B.MatItemSeqOld = A.MatItemSeq
         WHERE A.CompanySeq = @CompanySeq
		   AND B.WorkingTag = 'D'   
		   AND B.Status     = 0  
           
        IF @@ERROR <> 0 RETURN      

		   
    END    
    -- UPDATE      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'U' AND Status = 0)    
    BEGIN  
      
       UPDATE _TSLASServiceBOM    
          SET MatUnitSeq    = ISNULL(B.MatUnitSeq, 0), 	  
              Qty           = ISNULL(B.Qty, 0)       , 
              Remark        = ISNULL(B.Remark, 0)    , 
              LastUserSeq   =@UserSeq                ,
              LastDateTime  =GETDATE()
         FROM _TSLASServiceBOM          AS A 
               JOIN #BIZ_OUT_DataBlock2 AS B ON B.ItemSeqOld    = A.ItemSeq
                                            AND B.MatItemSeqOld = A.MatItemSeq
        WHERE A.CompanySeq = @CompanySeq   
		  AND B.Status     = 0  
          AND B.WorkingTag = 'U' 
  
        IF @@ERROR <> 0 RETURN   
         

    END   
  
    -- INSERT      
    IF EXISTS (SELECT 1 FROM #BIZ_OUT_DataBlock2 WHERE WorkingTag = 'A' AND Status = 0)    
    BEGIN    

        INSERT INTO _TSLASServiceBOM(CompanySeq  , ItemSeq     , MatItemSeq  , Qty         , MatUnitSeq ,
                                     Remark      , PgmSeq      , LastUserSeq , LastDateTime)

        SELECT @CompanySeq , ItemSeq    , MatItemSeq , Qty        , MatUnitSeq ,
               Remark     , @PgmSeq     , @UserSeq   , GETDATE() 
              FROM #BIZ_OUT_DataBlock2   
             WHERE WorkingTag = 'A' 
		       AND Status     = 0 
    
        IF @@ERROR <> 0 RETURN    


    END

    UPDATE #BIZ_OUT_DataBlock2
       SET ItemSeqOld    = ItemSeq,
           MatItemSeqOld = MatItemSeq 
      FROM #BIZ_OUT_DataBlock2 AS A 
     WHERE A.WorkingTag IN ('A', 'U')
       AND A.Status     = 0

RETURN
