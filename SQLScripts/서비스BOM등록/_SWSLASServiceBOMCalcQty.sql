IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceBOMCalcQty') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceBOMCalcQty
GO

/********************************
 설    명 - 서비스BOM등록:소요량계산
 작 성 일 - 2020년 05월 21일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceBOMCalcQty
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS

     -- 20200521 김재호 : [서비스BOM등록] 화면에서 서비스처리에 들어가는 하위자재의 소요량을 확인해야하므로 환경설정값 BOM의 중복 하위자재 합산여부와 관계없이 하위자재를 합산하여 표시함. 
     CREATE TABLE #GroupedBOM (
        Seq        INT IDENTITY(1,1),
        ItemSeq    INT              ,
        MatItemSeq INT              ,
        MatUnitSeq INT              ,
        Qty        DECIMAL(19,5) 
     )

    INSERT INTO #GroupedBOM (ItemSeq, MatItemSeq, MatUnitseq, Qty)
    SELECT ItemSeq   , 
           MatItemSeq,
           MatUnitSeq,
           SUM(CASE WHEN A.NeedQtyDenominator = 0 THEN 0
                   ELSE A.NeedQtyNumerator / A.NeedQtyDenominator END
               )
      FROM #BIZ_OUT_DataBlock2 AS A 
     GROUP BY ItemSeq, MatItemSeq, MatUnitSeq 
     ORDER BY MAX(ItemSerl)


     UPDATE #BIZ_OUT_DataBlock2
        SET AssetName       =  ISNULL(C.AssetName, ''),              
            ItemName        =  ISNULL(B.ItemName, '') ,                    
            ItemNo          =  ISNULL(B.ItemNo, '')   ,                    
            Spec            =  ISNULL(B.Spec, '')     ,                    
            UnitName        =  ISNULL(D.UnitName, '') ,                    
            MatAssetName    =  ISNULL(F.AssetName, ''),
            MatItemName     =  ISNULL(E.ItemName, '') ,
            MatItemNo       =  ISNULL(E.ItemNo, '')   ,
            MatSpec         =  ISNULL(E.Spec, '')     ,
            MatUnitName     =  ISNULL(G.UnitName, '') ,
            Qty             =  ISNULL(A1.Qty, 0)      ,   
           -- Remark          =  ISNULL(A.Remark, '') ,              
            ItemSeq         =  ISNULL(A.ItemSeq, 0)   ,              
            MatItemSeq      =  ISNULL(A.MatItemSeq, 0),              
            MatUnitSeq      =  ISNULL(A.MatUnitSeq, 0)                 
       FROM #BIZ_OUT_DataBlock2 AS A 
            JOIN #GroupedBOM               AS A1             ON A1.ItemSeq    = A.ItemSeq 
                                                           AND A1.MatItemSeq = A.MatItemSeq 
            LEFT OUTER JOIN _TDAItem      AS B WITH(NOLOCK) ON B.CompanySeq  = @CompanySeq
                                                           AND B.ItemSeq     = A.ItemSeq 
            LEFT OUTER JOIN _TDAItemAsset AS C WITH(NOLOCK) ON C.CompanySeq  = B.CompanySeq
                                                           AND C.AssetSeq    = B.AssetSeq
            LEFT OUTER JOIN _TDAUnit      AS D WITH(NOLOCK) ON D.CompanySeq  = B.CompanySeq
                                                           AND D.UnitSeq     = B.UnitSeq
            LEFT OUTER JOIN _TDAItem      AS E WITH(NOLOCK) ON E.CompanySeq  = @CompanySeq
                                                           AND E.ItemSeq     = A.MatItemSeq 
            LEFT OUTER JOIN _TDAItemAsset AS F WITH(NOLOCK) ON F.CompanySeq  = B.CompanySeq
                                                           AND F.AssetSeq    = B.AssetSeq
            LEFT OUTER JOIN _TDAUnit      AS G WITH(NOLOCK) ON G.CompanySeq  = B.CompanySeq
                                                           AND G.UnitSeq     = B.UnitSeq


RETURN