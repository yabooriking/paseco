IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceBOMCheck') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceBOMCheck
GO

/********************************
 설    명 - 서비스BOM등록:체크
 작 성 일 - 2020년 05월 20일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceBOMCheck
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS 

    ---- 사용할 변수를 선언한다.
    DECLARE @Count        INT          ,
            @MessageType  INT          ,      
            @Status       INT          ,      
            @Results      NVARCHAR(250),  
            @Seq          INT  

    IF EXISTS(SELECT 1 FROM #BIZ_OUT_DataBlock2)
    BEGIN

	-------------------------------------------      
    -- 존재하지 않는 자재를 삭제할 때 메시지 표시 
    -------------------------------------------  
    EXEC dbo._SWCOMMessage @MessageType OUTPUT,  
                           @Status      OUTPUT,  
                           @Results     OUTPUT,  
                           1365               , -- @1이(가) 존재하지 않습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 1365)  
                           @LanguageSeq       ,  
                           22367, N'BOM품목'    -- SELECT * FROM _TCADictionary WHERE Word like '%BOM품목%' and languageseq = 1 


     UPDATE #BIZ_OUT_DataBlock2 
        SET Result      = @Results    , 
            MessageType = @MessageType,  
            Status      = @Status  
       FROM #BIZ_OUT_DataBlock2 AS A
	  WHERE A.WorkingTag IN ('U', 'D')
	    AND A.Status     = 0  
        AND NOT EXISTS ( SELECT TOP 1 1 
                           FROM _TSLASServiceBOM WITH(NOLOCK)
                          WHERE CompanySeq = @CompanySeq
                            AND ItemSeq    = A.ItemSeqOld 
                            AND MatItemSeq = A.MatItemSeqOld
                       ) 

		-------------------------------------------      
		-- 제품 - 자재 중복체크 
		-------------------------------------------  

        EXEC dbo._SWCOMMessage @MessageType OUTPUT  ,  
                               @Status      OUTPUT  ,  
                               @Results     OUTPUT  ,  
                               6                    , -- 중복된 @1 @2가(이) 입력되었습니다.(SELECT * FROM _TCAMessageLanguage WHERE MessageSeq = 6)  
                               @LanguageSeq         ,  
                               2031, N'제품'        , -- SELECT * FROM _TCADictionary WHERE Word like '%제품%'   
							   1968, N'자재'          -- SELECT * FROM _TCADictionary WHERE Word like '%자재%' 

        UPDATE #BIZ_OUT_DataBlock2 
           SET Result      = @Results    , 
               MessageType = @MessageType,  
               Status      = @Status  
          FROM #BIZ_OUT_DataBlock2 AS A
               JOIN ( SELECT  S.ItemSeq, S.MatItemSeq
                        FROM (
                               SELECT A.ItemSeq, A.MatItemSeq
                                 FROM #BIZ_OUT_DataBlock2 AS A
                                WHERE A.WorkingTag IN ('A', 'U')
                                  AND A.Status     = 0
                                UNION ALL
                               SELECT A.ItemSeq, A.MatItemSeq
                                 FROM _TSLASServiceBOM AS A WITH(NOLOCK)
                                WHERE A.CompanySeq = @CompanySeq
                                  AND NOT EXISTS ( SELECT 1
                                                     FROM #BIZ_OUT_DataBlock2
                                                    WHERE WorkingTag IN ('A', 'U')
                                                      AND Status	    = 0
                                                      AND ItemSeqOld    = A.ItemSeq
                                                      AND MatItemSeqOld = A.MatItemSeq
                                                 )
					         ) AS S
				       GROUP BY S.ItemSeq, S.MatItemSeq
				      HAVING COUNT(1) > 1
                    ) AS B
                 ON B.ItemSeq    = A.ItemSeq
				AND B.MatItemSeq = A.MatItemSeq
		 WHERE A.WorkingTag IN ('A', 'U')
		   AND A.Status     = 0  

    END

RETURN
