IF EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID('_SWSLASServiceBOMQuery') AND sysstat & 0xf = 4)
  DROP PROCEDURE dbo._SWSLASServiceBOMQuery
GO

/********************************
 설    명 - 서비스BOM등록:조회
 작 성 일 - 2020년 05월 14일
 작 성 자 - 김재호
 수정내역 - 
*********************************/

CREATE PROC dbo._SWSLASServiceBOMQuery
    @ServiceSeq     INT          = 0 ,
    @WorkingTag     NVARCHAR(10) = '',
    @CompanySeq     INT          = 1 ,
    @LanguageSeq    INT          = 1 ,
    @UserSeq        INT          = 0 ,
    @PgmSeq         INT          = 0 ,
    @IsTransaction  BIT          = 0

AS
    DECLARE @ItemName  NVARCHAR(200), 
            @ItemNo    NVARCHAR(100),
            @ItemNameM NVARCHAR(200),
            @ItemNoM   NVARCHAR(100)

    SELECT @ItemName  = ISNULL(ItemName,'') ,
           @ItemNo    = ISNULL(ItemNo,'')   ,
           @ItemNameM = ISNULL(ItemNameM,''),
           @ItemNoM   = ISNULL(ItemNoM,'')  
      FROM #BIZ_IN_DataBlock1 

	SELECT C.AssetName                ,
           B.ItemName                 ,
           B.ItemNo                   ,
           B.Spec                     ,
           D.UnitName                 ,
           F.AssetName AS MatAssetName,
           E.ItemName AS MatItemName  ,
           E.ItemNo AS MatItemNo      ,
           E.Spec AS MatSpec          ,
           G.UnitName AS MatUnitName  ,
           A.Qty                      ,
           A.Remark                   ,
           A.ItemSeq                  ,
           A.MatItemSeq               ,
           A.MatUnitSeq               ,
           A.ItemSeq AS ItemSeqOld    ,
           A.MatItemSeq AS MatItemSeqOld
      FROM _TSLASServiceBOM              AS A WITH(NOLOCK)
           LEFT OUTER JOIN _TDAItem      AS B WITH(NOLOCK) ON B.CompanySeq = A.CompanySeq
                                                          AND B.ItemSeq    = A.ItemSeq 
           LEFT OUTER JOIN _TDAItemAsset AS C WITH(NOLOCK) ON C.CompanySeq = B.CompanySeq
                                                          AND C.AssetSeq   = B.AssetSeq
           LEFT OUTER JOIN _TDAUnit      AS D WITH(NOLOCK) ON D.CompanySeq = B.CompanySeq
                                                          AND D.UnitSeq    = B.UnitSeq
           LEFT OUTER JOIN _TDAItem      AS E WITH(NOLOCK) ON E.CompanySeq = A.CompanySeq
                                                          AND E.ItemSeq    = A.MatItemSeq 
           LEFT OUTER JOIN _TDAItemAsset AS F WITH(NOLOCK) ON F.CompanySeq = B.CompanySeq
                                                          AND F.AssetSeq   = B.AssetSeq
           LEFT OUTER JOIN _TDAUnit      AS G WITH(NOLOCK) ON G.CompanySeq = B.CompanySeq
                                                          AND G.UnitSeq    = B.UnitSeq
     WHERE A.CompanySeq = @CompanySeq
       AND B.ItemName LIKE  @ItemName + '%' 
       AND B.ItemNo   LIKE  @ItemNo + '%' 
       AND E.ItemName LIKE  @ItemNameM + '%' 
       AND E.ItemName LIKE  @ItemNoM + '%' 

RETURN
