select * from _TCOMConfirmDef where ConfirmSeq = 1000163
select * from _TCOMConfirmTable where ConfirmTableSeq = 1003688
select * from _TCOMConfirmPgm where ConfirmSeq = 1000163
select * from _TCOMConfirmDefinition where ConfirmSeq = 1000163


select * from _TCOMConfirmDef where confirmname LIKE '계약관리%'
select * from _TCOMConfirmTable where confirmtablename = 'DTI_TSLContractMng'

GetInsertScriptTableNull '','_TCOMConfirmDef'
SELECT 'INSERT INTO _TCOMConfirmDef(CompanySeq,ConfirmSeq,ConfirmName,ConfirmID,ConfirmTableSeq,AutoConfirmSPName,IsNotUsed,LastUserSeq,LastDateTime,ISDisableCheck)' + CHAR(13) + 'SELECT '  + CONVERT(VARCHAR(30), ISNULL(CompanySeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(ConfirmSeq, 0)) + ','   + 'N''' + ISNULL(RTRIM(ConfirmName), '') + ''','   + 'N''' + ISNULL(RTRIM(ConfirmID), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(ConfirmTableSeq, 0)) + ','   + 'N''' + ISNULL(RTRIM(AutoConfirmSPName), '') + ''','   + 'N''' + ISNULL(RTRIM(IsNotUsed), '') + ''','   + CASE WHEN LastUserSeq IS NULL THEN 'NULL, ' ELSE CONVERT(VARCHAR(30), LastUserSeq) + ',' END   + CASE WHEN LastDateTime IS NULL THEN 'NULL, ' ELSE '''' + CONVERT(VARCHAR(30), LastDateTime, 121) + ''',' END   + CASE WHEN ISDisableCheck IS NULL THEN 'NULL ' ELSE 'N''' + ISDisableCheck + '''' END   FROM _TCOMConfirmDef
where ConfirmSeq = 1000163
union all
SELECT 'INSERT INTO _TCOMConfirmPgm(CompanySeq,ConfirmSeq,ConfirmSerl,PGMSeq,LastUserSeq,LastDateTime)' + CHAR(13) + 'SELECT '  + CONVERT(VARCHAR(30), ISNULL(CompanySeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(ConfirmSeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(ConfirmSerl, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(PGMSeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(LastUserSeq, 0)) + ','   + ''''  + CONVERT(VARCHAR(30), ISNULL(LastDateTime, ''), 121) + ''''   FROM _TCOMConfirmPgm
where ConfirmSeq = 1000163
union all
SELECT 'INSERT INTO _TCOMConfirmTable(CompanySeq,ConfirmTableSeq,ConfirmTableName,ConfirmTableDESC,ProgressTableName,ProgressCheckLevel,LastUserSeq,LastDateTime,ConfirmCheckLevel)' + CHAR(13) + 'SELECT '  + CONVERT(VARCHAR(30), ISNULL(CompanySeq, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(ConfirmTableSeq, 0)) + ','   + 'N''' + ISNULL(RTRIM(ConfirmTableName), '') + ''','   + 'N''' + ISNULL(RTRIM(ConfirmTableDESC), '') + ''','   + 'N''' + ISNULL(RTRIM(ProgressTableName), '') + ''','   + CONVERT(VARCHAR(30), ISNULL(ProgressCheckLevel, 0)) + ','   + CONVERT(VARCHAR(30), ISNULL(LastUserSeq, 0)) + ','   + '''' + CONVERT(VARCHAR(30), ISNULL(LastDateTime, ''), 121) + ''','   + CASE WHEN ConfirmCheckLevel IS NULL THEN 'NULL ' ELSE CONVERT(VARCHAR(30), ConfirmCheckLevel) END   FROM _TCOMConfirmTable
where ConfirmTableSeq = 1003688



-- 확정관리
INSERT INTO _TCOMConfirmDef(CompanySeq,ConfirmSeq,ConfirmName,ConfirmID,ConfirmTableSeq,AutoConfirmSPName,IsNotUsed,LastUserSeq,LastDateTime,ISDisableCheck) 
SELECT CompanySeq,1000160,N'사원별업무비중_DTI',N'EmpWorkRatioConfirm',1003686,N'',N'0',14175,'2014-03-05 20:32:48.623',N'1'
  FROM _TCACompany a
 WHERE NOT EXISTS (SELECT 1 FROM _TCOMConfirmDef WHERE CompanySeq = a.CompanySeq AND ConfirmSeq = 1000160)
 
-- 확정관리프로그램
INSERT INTO _TCOMConfirmPgm(CompanySeq,ConfirmSeq,ConfirmSerl,PGMSeq,LastUserSeq,LastDateTime) 
SELECT CompanySeq,1000160,1000001,1016731,14175,'2014-03-05 20:32:59.620'
  FROM _TCACompany a
 WHERE NOT EXISTS (SELECT 1 FROM _TCOMConfirmPgm WHERE CompanySeq = a.CompanySeq AND ConfirmSeq = 1000160)

-- 확정테이블등록
INSERT INTO _TCOMConfirmTable(CompanySeq,ConfirmTableSeq,ConfirmTableName,ConfirmTableDESC,ProgressTableName,ProgressCheckLevel,LastUserSeq,LastDateTime,ConfirmCheckLevel) 
SELECT CompanySeq,1003686,N'DTI_TESMDEmpWorkRatio',N'사원별업무비중_DTI',N'',0,14175,'2014-03-05 20:30:28.747',2
  FROM _TCACompany a
 WHERE NOT EXISTS (SELECT 1 FROM _TCOMConfirmTable WHERE CompanySeq = a.CompanySeq AND ConfirmTableSeq = 1003686)






INSERT INTO _TCOMConfirmDef(CompanySeq,ConfirmSeq,ConfirmName,ConfirmID,ConfirmTableSeq,AutoConfirmSPName,IsNotUsed,LastUserSeq,LastDateTime,ISDisableCheck) 
SELECT a.companySeq,1000163,N'계약관리현황',N'DTI_TSLContractMng',1003688,N'',N'0',50322,'2014-03-14 09:40:53.507',N'0' 
  from _TCACompany as a 
 where NOT EXISTS (SELECT 1 FROM _TCOMConfirmDef WHERE CompanySeq = a.CompanySeq AND ConfirmSeq = 1000163) 
 
INSERT INTO _TCOMConfirmPgm(CompanySeq,ConfirmSeq,ConfirmSerl,PGMSeq,LastUserSeq,LastDateTime) 
SELECT a.companySeq,1000163,1000001,1013761,50322,'2014-03-14 09:41:04.880'
  from _TCACompany as a 
 where NOT EXISTS (SELECT 1 FROM _TCOMConfirmPgm WHERE CompanySeq = a.CompanySeq AND ConfirmSeq = 1000163 and ConfirmSerl = 1000001) 

INSERT INTO _TCOMConfirmPgm(CompanySeq,ConfirmSeq,ConfirmSerl,PGMSeq,LastUserSeq,LastDateTime) 
SELECT a.companySeq,1000163,1000002,1013760,50322,'2014-03-14 10:20:42.100'
  from _TCACompany as a 
 where NOT EXISTS (SELECT 1 FROM _TCOMConfirmPgm WHERE CompanySeq = a.CompanySeq AND ConfirmSeq = 1000163 and ConfirmSerl = 1000001) 

INSERT INTO _TCOMConfirmTable(CompanySeq,ConfirmTableSeq,ConfirmTableName,ConfirmTableDESC,ProgressTableName,ProgressCheckLevel,LastUserSeq,LastDateTime,ConfirmCheckLevel) 
SELECT a.companySeq,1003688,N'DTI_TSLContractMng',N'계약관리현황_DTI',N'',0,50322,'2014-03-14 09:21:51.177',1
  from _TCACompany as a 
 WHERE NOT EXISTS (SELECT 1 FROM _TCOMConfirmTable WHERE CompanySeq = a.CompanySeq AND ConfirmTableSeq = 1003688)