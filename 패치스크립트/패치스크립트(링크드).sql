-- 운영
IF NOT EXISTS (SELECT 1 FROM _TCALinkPgmGroup WHERE LinkGroupSeq = 1000299)
BEGIN
    Insert Into _TCALinkPgmGroup(LinkGroupSeq,LinkGroupName,LinkGroupID,IsCompanyAll,LastUserSeq,LastDateTime,CompanySeq) 
    select 1000299,N'BOM비교조회_costel',N'BOM_costel',N'0',1,GETDATE(),0
END
IF NOT EXISTS (SELECT 1 FROM _TCALinkCreateID WHERE LinkGroupSeq = 1000299)
BEGIN
    Insert Into _TCALinkCreateID(LinkCreateSeq,LinkCreateID,LinkCreateName,LinkGroupSeq,CompanySeq,LastUserSeq,LastDateTime) 
    select 1000436,N'BOMCmp_costel',N'비교조회_costel',1000299,0,1,GETDATE()
END
IF NOT EXISTS (SELECT 1 FROM _TCALinkOrgPgm WHERE LinkGroupSeq = 1000299)
BEGIN
    Insert Into _TCALinkOrgPgm(LinkGroupSeq,OrgPgmSeq,LastUserSeq,LastDateTime,CompanySeq) 
    select 1000299,1015285,1,GETDATE(),0
END
IF NOT EXISTS (SELECT 1 FROM _TCALinkOrgPgm WHERE LinkGroupSeq = 1000299)
BEGIN
    Insert Into _TCALinkCreatePgm(LinkGroupSeq,OrgPgmSeq,LinkCreateSeq,PgmSeq,LastUserSeq,LastDateTime) 
    select 1000299,1015285,1000436,1016310,1,GETDATE()
END

IF NOT EXISTS (SELECT 1 FROM _TCALinkControlsAddProperty WHERE PgmSeq = 1016310)
BEGIN
    Insert Into _TCALinkControlsAddProperty(CompanySeq,PgmSeq,ControlName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq) 
    select 0,1016310,N'txtBOMRev',183001,0,N'차수',N'',0
    Insert Into _TCALinkControlsAddProperty(CompanySeq,PgmSeq,ControlName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq) 
    select 0,1016310,N'txtBOMRev',183002,0,N'NOQ;',N'',0
    Insert Into _TCALinkControlsAddProperty(CompanySeq,PgmSeq,ControlName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq)
    select 0,1016310,N'txtBOMRev',183006,2,N'',N'',0
END
IF NOT EXISTS (SELECT 1 FROM _TCALinkSheetsAddProperty WHERE PgmSeq = 1016310)
BEGIN
    Insert Into _TCALinkSheetsAddProperty(CompanySeq,PgmSeq,ControlName,DataFieldName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq) 
    select 0,1016310,N'SS1',N'BOMRev',183001,0,N'차수',N'',0
    Insert Into _TCALinkSheetsAddProperty(CompanySeq,PgmSeq,ControlName,DataFieldName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq) 
    select 0,1016310,N'SS1',N'BOMRev',183002,0,N';',N'',0
    Insert Into _TCALinkSheetsAddProperty(CompanySeq,PgmSeq,ControlName,DataFieldName,ControlPropertyType,ControlValue,ControlText,Remark,WordSeq) 
    select 0,1016310,N'SS1',N'BOMRev',183006,18,N'',N'',0
END