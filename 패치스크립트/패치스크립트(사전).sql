if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56065) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56065, N'사택료 항목정의 정보', null, N'사택료 항목정의 정보', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56065, N'사택료 항목정의 정보', null, N'사택료 항목정의 정보', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56065, N'사택료 항목정의 정보', null, N'사택료 항목정의 정보', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56065, N'사택료 항목정의 정보', null, N'사택료 항목정의 정보', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56065, N'사택료 항목정의 정보', null, N'사택료 항목정의 정보', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56066) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56066, N'사택료항목별계산', null, N'사택료항목별계산', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56066, N'사택료항목별계산', null, N'사택료항목별계산', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56066, N'사택료항목별계산', null, N'사택료항목별계산', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56066, N'사택료항목별계산', null, N'사택료항목별계산', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56066, N'사택료항목별계산', null, N'사택료항목별계산', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56067) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56067, N'사택료등록', null, N'사택료등록', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56067, N'사택료등록', null, N'사택료등록', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56067, N'사택료등록', null, N'사택료등록', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56067, N'사택료등록', null, N'사택료등록', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56067, N'사택료등록', null, N'사택료등록', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56068) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56068, N'사택료 급상여반영', null, N'사택료 급상여반영', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56068, N'사택료 급상여반영', null, N'사택료 급상여반영', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56068, N'사택료 급상여반영', null, N'사택료 급상여반영', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56068, N'사택료 급상여반영', null, N'사택료 급상여반영', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56068, N'사택료 급상여반영', null, N'사택료 급상여반영', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56069) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56069, N'소급조건별조회', null, N'소급조건별조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56069, N'소급조건별조회', null, N'소급조건별조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56069, N'소급조건별조회', null, N'소급조건별조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56069, N'소급조건별조회', null, N'소급조건별조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56069, N'소급조건별조회', null, N'소급조건별조회', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56070) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56070, N'소급지급공제차액내역조회', null, N'소급지급공제차액내역조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56070, N'소급지급공제차액내역조회', null, N'소급지급공제차액내역조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56070, N'소급지급공제차액내역조회', null, N'소급지급공제차액내역조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56070, N'소급지급공제차액내역조회', null, N'소급지급공제차액내역조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56070, N'소급지급공제차액내역조회', null, N'소급지급공제차액내역조회', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56071) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56071, N'소급지급공제차액개인상세조회', null, N'소급지급공제차액개인상세조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56071, N'소급지급공제차액개인상세조회', null, N'소급지급공제차액개인상세조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56071, N'소급지급공제차액개인상세조회', null, N'소급지급공제차액개인상세조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56071, N'소급지급공제차액개인상세조회', null, N'소급지급공제차액개인상세조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56071, N'소급지급공제차액개인상세조회', null, N'소급지급공제차액개인상세조회', 0, getdate(), 0 

end 




if not exists(select top 1 1 from _TCADictionary where WordSeq = 58472) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58472, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58472, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58472, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58472, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58472, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58473) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58473, N'사택료항목', null, N'사택료항목', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58473, N'사택료항목', null, N'사택료항목', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58473, N'사택료항목', null, N'사택료항목', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58473, N'사택료항목', null, N'사택료항목', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58473, N'사택료항목', null, N'사택료항목', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58474) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58474, N'입력대상생성', null, N'입력대상생성', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58474, N'입력대상생성', null, N'입력대상생성', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58474, N'입력대상생성', null, N'입력대상생성', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58474, N'입력대상생성', null, N'입력대상생성', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58474, N'입력대상생성', null, N'입력대상생성', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58475) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58475, N'사택료불러오기', null, N'사택료불러오기', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58475, N'사택료불러오기', null, N'사택료불러오기', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58475, N'사택료불러오기', null, N'사택료불러오기', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58475, N'사택료불러오기', null, N'사택료불러오기', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58475, N'사택료불러오기', null, N'사택료불러오기', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58476) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58476, N'사택료계산년월', null, N'사택료계산년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58476, N'사택료계산년월', null, N'사택료계산년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58476, N'사택료계산년월', null, N'사택료계산년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58476, N'사택료계산년월', null, N'사택료계산년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58476, N'사택료계산년월', null, N'사택료계산년월', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58477) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58477, N'급상여 반영 정보', null, N'급상여 반영 정보', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58477, N'급상여 반영 정보', null, N'급상여 반영 정보', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58477, N'급상여 반영 정보', null, N'급상여 반영 정보', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58477, N'급상여 반영 정보', null, N'급상여 반영 정보', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58477, N'급상여 반영 정보', null, N'급상여 반영 정보', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58478) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58478, N'급상여반영 여부', null, N'급상여반영 여부', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58478, N'급상여반영 여부', null, N'급상여반영 여부', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58478, N'급상여반영 여부', null, N'급상여반영 여부', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58478, N'급상여반영 여부', null, N'급상여반영 여부', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58478, N'급상여반영 여부', null, N'급상여반영 여부', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58479) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58479, N'사택료 반영', null, N'사택료 반영', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58479, N'사택료 반영', null, N'사택료 반영', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58479, N'사택료 반영', null, N'사택료 반영', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58479, N'사택료 반영', null, N'사택료 반영', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58479, N'사택료 반영', null, N'사택료 반영', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58480) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58480, N'사택료 반영취소', null, N'사택료 반영취소', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58480, N'사택료 반영취소', null, N'사택료 반영취소', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58480, N'사택료 반영취소', null, N'사택료 반영취소', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58480, N'사택료 반영취소', null, N'사택료 반영취소', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58480, N'사택료 반영취소', null, N'사택료 반영취소', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58481) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58481, N'남여구분', null, N'남여구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58481, N'남여구분', null, N'남여구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58481, N'남여구분', null, N'남여구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58481, N'남여구분', null, N'남여구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58481, N'남여구분', null, N'남여구분', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58482) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58482, N'노조가입구분', null, N'노조가입구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58482, N'노조가입구분', null, N'노조가입구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58482, N'노조가입구분', null, N'노조가입구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58482, N'노조가입구분', null, N'노조가입구분', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58482, N'노조가입구분', null, N'노조가입구분', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58483) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58483, N'사택내부순번', null, N'사택내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58483, N'사택내부순번', null, N'사택내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58483, N'사택내부순번', null, N'사택내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58483, N'사택내부순번', null, N'사택내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58483, N'사택내부순번', null, N'사택내부순번', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58484) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58484, N'임시사용', null, N'임시사용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58484, N'임시사용', null, N'임시사용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58484, N'임시사용', null, N'임시사용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58484, N'임시사용', null, N'임시사용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58484, N'임시사용', null, N'임시사용', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58485) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58485, N'전월검침량', null, N'전월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58485, N'전월검침량', null, N'전월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58485, N'전월검침량', null, N'전월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58485, N'전월검침량', null, N'전월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58485, N'전월검침량', null, N'전월검침량', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58486) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58486, N'당월검침량', null, N'당월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58486, N'당월검침량', null, N'당월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58486, N'당월검침량', null, N'당월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58486, N'당월검침량', null, N'당월검침량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58486, N'당월검침량', null, N'당월검침량', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58487) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58487, N'당월사용량', null, N'당월사용량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58487, N'당월사용량', null, N'당월사용량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58487, N'당월사용량', null, N'당월사용량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58487, N'당월사용량', null, N'당월사용량', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58487, N'당월사용량', null, N'당월사용량', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58488) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58488, N'전용면적(평)', null, N'전용면적(평)', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58488, N'전용면적(평)', null, N'전용면적(평)', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58488, N'전용면적(평)', null, N'전용면적(평)', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58488, N'전용면적(평)', null, N'전용면적(평)', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58488, N'전용면적(평)', null, N'전용면적(평)', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58489) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58489, N'상하수도료', null, N'상하수도료', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58489, N'상하수도료', null, N'상하수도료', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58489, N'상하수도료', null, N'상하수도료', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58489, N'상하수도료', null, N'상하수도료', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58489, N'상하수도료', null, N'상하수도료', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58490) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58490, N'공제급여내부코드', null, N'공제급여내부코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58490, N'공제급여내부코드', null, N'공제급여내부코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58490, N'공제급여내부코드', null, N'공제급여내부코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58490, N'공제급여내부코드', null, N'공제급여내부코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58490, N'공제급여내부코드', null, N'공제급여내부코드', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58491) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58491, N'소급년월', null, N'소급년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58491, N'소급년월', null, N'소급년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58491, N'소급년월', null, N'소급년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58491, N'소급년월', null, N'소급년월', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58491, N'소급년월', null, N'소급년월', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58492) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58492, N'사택료항목내부순번', null, N'사택료항목내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58492, N'사택료항목내부순번', null, N'사택료항목내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58492, N'사택료항목내부순번', null, N'사택료항목내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58492, N'사택료항목내부순번', null, N'사택료항목내부순번', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58492, N'사택료항목내부순번', null, N'사택료항목내부순번', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 17081) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 17081, N'适用方式代码', null, N'적용방식코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 17081, N'適用方式代碼 ', null, N'적용방식코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 17081, N'적용방식코드', null, N'적용방식코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 6, 17081, N'적용방식코드', null, N'적용방식코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 17081, N'適用方式コード', null, N'적용방식코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 17081, N'Application Method Code', null, N'적용방식코드', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58494) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58494, N'공실적용', null, N'공실적용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58494, N'공실적용', null, N'공실적용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58494, N'공실적용', null, N'공실적용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58494, N'공실적용', null, N'공실적용', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58494, N'공실적용', null, N'공실적용', 0, getdate(), 0 

end 
if not exists(select top 1 1 from _TCADictionary where WordSeq = 58495) 
begin 
    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 58495, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 58495, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 58495, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 58495, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

    insert into _TCADictionary(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 58495, N'항목명코드', null, N'항목명코드', 0, getdate(), 0 

end 

if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56072) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 
end 


if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56072) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56072, N'사택료조회', null, N'사택료조회', 0, getdate(), 0 
end 

if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56073) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56073, N'사택료 별도관리내역 출력', null, N'사택료 별도관리내역 출력', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56073, N'사택료 별도관리내역 출력', null, N'사택료 별도관리내역 출력', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56073, N'사택료 별도관리내역 출력', null, N'사택료 별도관리내역 출력', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56073, N'사택료 별도관리내역 출력', null, N'사택료 별도관리내역 출력', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56073, N'사택료 별도관리내역 출력', null, N'사택료 별도관리내역 출력', 0, getdate(), 0 
end 

if not exists(select top 1 1 from _TCADictionaryCommon where WordSeq = 56074) 
begin 
    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 1, 56074, N'사택료 급상여반영 출력(급여반영)', null, N'사택료 급상여반영 출력(급여반영)', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 2, 56074, N'사택료 급상여반영 출력(급여반영)', null, N'사택료 급상여반영 출력(급여반영)', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 3, 56074, N'사택료 급상여반영 출력(급여반영)', null, N'사택료 급상여반영 출력(급여반영)', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 4, 56074, N'사택료 급상여반영 출력(급여반영)', null, N'사택료 급상여반영 출력(급여반영)', 0, getdate(), 0 

    insert into _TCADictionaryCommon(LanguageSeq,WordSeq,Word,WordSite,Description,LastUserSeq,LastDateTime,CompanySeq) select 5, 56074, N'사택료 급상여반영 출력(급여반영)', null, N'사택료 급상여반영 출력(급여반영)', 0, getdate(), 0 
end 
