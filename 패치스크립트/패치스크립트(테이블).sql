if object_id('DTI_TCOMEnvAdmin') is null

begin 
--drop table DTI_TCOMEnvAdmin
CREATE TABLE DTI_TCOMEnvAdmin
(
    CompanySeq		INT 	 NOT NULL, 
    AdminSeq		INT 	 NOT NULL, 
    PgmSeq		INT 	 NULL, 
    EmpSeq		INT 	 NULL, 
    DeptSeq		INT 	 NULL, 
    TargetDeptSeq		INT 	 NULL, 
    LastUserSeq		INT 	 NULL, 
    LastDateTime		DATETIME 	 NULL
)
create unique clustered index idx_DTI_TCOMEnvAdmin on DTI_TCOMEnvAdmin(CompanySeq,AdminSeq) 
end 


if object_id('DTI_TCOMEnvAdminLog') is null
begin 
--drop table DTI_TCOMEnvAdminLog
CREATE TABLE DTI_TCOMEnvAdminLog
(
    LogSeq		INT IDENTITY(1,1) NOT NULL, 
    LogUserSeq		INT NOT NULL, 
    LogDateTime		DATETIME NOT NULL, 
    LogType		NCHAR(1) NOT NULL, 
    LogPgmSeq		INT NULL, 
    CompanySeq		INT 	 NOT NULL, 
    AdminSeq		INT 	 NOT NULL, 
    PgmSeq		INT 	 NULL, 
    EmpSeq		INT 	 NULL, 
    DeptSeq		INT 	 NULL, 
    TargetDeptSeq		INT 	 NULL, 
    LastUserSeq		INT 	 NULL, 
    LastDateTime		DATETIME 	 NULL
)
end