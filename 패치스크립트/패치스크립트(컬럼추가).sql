if not exists (select 1 from syscolumns where id = object_id('DTI_TSLContractMng') and name = 'ContractName')
begin
    ALTER TABLE DTI_TSLContractMng ADD ContractName NVARCHAR(100) 
    ALTER TABLE DTI_TSLContractMngLog ADD ContractName NVARCHAR(100) 
end

if not exists (select 1 from syscolumns where id = object_id('DTI_TSLContractMng') and name = 'IsOrderAutoCfm')
begin
    ALTER TABLE DTI_TSLContractMng ADD IsOrderAutoCfm NCHAR(1)
    ALTER TABLE DTI_TSLContractMngLog ADD IsOrderAutoCfm NCHAR(1)
end




if not exists (select 1 from syscolumns where id = object_id('_TEQYearRepairMngCHE') and name = 'UMKeepKind')
begin
alter table _TEQYearRepairMngCHE add UMKeepKind int null
alter table _TEQYearRepairMngCHELog add UMKeepKind int null
end 

if not exists (select 1 from syscolumns where id = object_id('_TEQYearRepairRltManHourCHE') and name = 'EmpSeq')
begin
alter table _TEQYearRepairRltManHourCHE add EmpSeq int null 
alter table _TEQYearRepairRltManHourCHELog add EmpSeq int null 
end 

if not exists (select 1 from syscolumns where id = object_id('_TEQYearRepairRltManHourCHE') and name = 'DivSeq')
begin
alter table _TEQYearRepairRltManHourCHE add DivSeq int null 
alter table _TEQYearRepairRltManHourCHELog add DivSeq int null 
end 